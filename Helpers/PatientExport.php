<?php

namespace Modules\Analytics\Helpers;

use Illuminate\Support\Facades\DB;
use App\Entities\Facility\InvoiceItem;

use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Facades\Excel;

class PatientExport implements FromCollection, WithHeadings
{
    function collection()
    {

        $builder1 = InvoiceItem::select(
            // DB::raw("number AS invoice_number"),
            DB::raw("date_closed"),
            DB::raw("invoices.number as invoice_number"),
            DB::raw("patients.first_name"),
            DB::raw("patients.last_name"),
            DB::raw("clinics.name"),
            DB::raw("invoice_items.name AS service_name"),
            DB::raw("invoice_items.total_amount"),
            DB::raw("invoice_items.amount_paid"),
            DB::raw("invoice_items.balance"),
            DB::raw("schemes.name as payment_mode")
            )
            ->join('invoices', 'invoices.id', 'invoice_items.invoice_id')
            ->join('payment_mode_visits', 'payment_mode_visits.visit_id', 'invoices.visit_id')
            ->join('schemes', 'schemes.id', 'payment_mode_visits.scheme_id')
            ->join('visits', 'visits.id', 'invoices.visit_id')
            ->join('patients', 'patients.id', 'visits.patient_id')
            ->join('clinics', 'clinics.id', 'visits.clinic_id')
            ->whereNotNull('date_closed');

        $builder = InvoiceItem::select(
            // DB::raw("number AS invoice_number"),
            DB::raw("date_closed"),
            DB::raw("invoices.number as invoice_number"),
            DB::raw("patients.first_name"),
            DB::raw("patients.last_name"),
            DB::raw("clinics.name"),
            DB::raw("invoice_items.name AS service_name"),
            DB::raw("invoice_items.total_amount"),
            DB::raw("invoice_items.amount_paid"),
            DB::raw("invoice_items.balance"),
            DB::raw("payment_modes.name as payment_mode")
            
            )
            ->join('invoices', 'invoices.id', 'invoice_items.invoice_id')
            ->join('payment_mode_visits', 'payment_mode_visits.visit_id', 'invoices.visit_id')
            ->join('payment_modes', 'payment_modes.id', 'payment_mode_visits.payment_mode_id')
            ->join('visits', 'visits.id', 'invoices.visit_id')
            ->join('patients', 'patients.id', 'visits.patient_id')
            ->join('clinics', 'clinics.id', 'visits.clinic_id')
            ->where('invoice_items.amount_paid', '>', 0)
            ->whereNotNull('date_closed') 
            ->union($builder1);
            
        $invoices = $builder->get();

        return $invoices;

    }

    public function headings(): array
    {
        return ["INVOICE DATE", "INVOICE NUMBER", "FIRST_NAME", "LAST_NAME", "CLINIC", "SERVICE", "TOTAL AMOUNT", "AMOUNT_PAID", "BALANCE", "PAYMENT_MODE"];
    }
}


?>