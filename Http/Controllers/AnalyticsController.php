<?php

namespace Modules\Analytics\Http\Controllers;

use App\Entities\Facility\Clinic;
use App\Entities\Facility\Visit;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Exception;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Support\Facades\DB;
use Illuminate\View\View;

class AnalyticsController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Application|Factory|View
     */
    public function index()
    {
        $pageTitle = "Analytics";
        $clinics = Clinic::active()->orderBy('name', 'ASC')->get();
        return view('analytics::index', compact(["pageTitle", "clinics"]));
    }

    public function visits_rest()
    {
        $start_date = trim(request("start_date", null));

        $end_date = trim(request("end_date", null));

        try {

            if (empty($start_date)) {

                $start_date = Carbon::now()->subMonths(1)->startOfDay();

            } else {

                $start_date = Carbon::parse($start_date)->startOfDay();

            }
        } catch (Exception $e) {

            $start_date = Carbon::now()->subMonths(1)->startOfDay();

        }

        try {

            if (empty($end_date)) {

                $end_date = Carbon::now()->endOfDay();

            } else {
                $end_date = Carbon::parse($end_date)->endOfDay();
            }

        } catch (Exception $e) {

            $end_date = Carbon::now()->endOfDay();

        }

        //$period = CarbonPeriod::create($start_date, $end_date);

        $dateTimeFormat = config('constants.php_date_format');

        $data = [
            "labels" => [],
            "datasets" => [
                [
                    'label' => 'Visits',
                    'backgroundColor' => '#00a65a',
                    'borderColor' => '#00a65a',
                    'pointRadius' => 5,
                    //'pointHoverRadius' => 15,
                    'showLine' => true,
                    'steppedLine' => false,
                    'fill' => false,
                    'data' => [

                    ]
                ]
            ],
        ];


        $builder = Visit::select(DB::raw('DATE(created_at) as date'), DB::raw('count(*) as visit_count'))
            ->whereBetween('created_at', [$start_date, $end_date])
            ->groupBy(DB::raw('DATE(created_at)'));

        $clinics = request('clinics', []);

        $is_emergency = (int)request('is_emergency', 0);

        if (!empty($clinics) && is_array($clinics)) {

            $builder->whereHas('clinic', function ($query) use ($clinics) {
                $query->select([
                    DB::raw("DISTINCT *"),
                    DB::raw("count(*) OVER() AS filtered")
                ]);
                $query->whereIn('uuid', $clinics);
            });

        }

        if ($is_emergency === 1) {

            $builder->where('is_emergency', $is_emergency);
        }

        $visits = $builder->get()->map(function ($visit) {
            $visit->{"date"} = Carbon::parse($visit->date);
            return $visit;
        })->sortBy(function ($obj, $key) {
            return $obj->date;
        });

        foreach ($visits as $visit) {

            $data['labels'][] = Carbon::parse($visit->date)->format($dateTimeFormat);
            $data['datasets'][0]["data"][] = $visit->visit_count;

        }

        return $data;

    }

    public function payment_modes_rest()
    {

        $start_date = trim(request("start_date", null));

        $end_date = trim(request("end_date", null));

        try {

            if (empty($start_date)) {

                $start_date = Carbon::now()->subMonths(1)->startOfDay();

            } else {

                $start_date = Carbon::parse($start_date)->startOfDay();

            }
        } catch (Exception $e) {

            $start_date = Carbon::now()->subMonths(1)->startOfDay();

        }

        try {

            if (empty($end_date)) {

                $end_date = Carbon::now()->endOfDay();

            } else {
                $end_date = Carbon::parse($end_date)->endOfDay();
            }

        } catch (Exception $e) {

            $end_date = Carbon::now()->endOfDay();

        }

        $builder = DB::table('invoice_items')
            ->select([
                'schemes.name',
                'amount_paid',
            ])
            ->whereBetween('invoice_items.updated_at', [$start_date, $end_date])
            ->whereNotNull('scheme_id')
            ->join('invoices', 'invoices.id', '=', 'invoice_items.invoice_id')
            ->join('schemes', 'schemes.id', '=', 'invoice_items.scheme_id')
            ->orderBy('schemes.name');

        $sql = $builder->toSql();

        //return $sql;

        $builder = DB::table(DB::raw("($sql) AS A"))
            ->mergeBindings($builder)
            ->select([
                'name',
                DB::raw('SUM(amount_paid) AS amount_paid')
            ])
            ->where('amount_paid', '>', 0)->groupBy('name');

        $results = $builder->get();

        $builder = DB::table('invoice_items')
            ->select([
                'payment_modes.name',
                'amount_paid',
            ])
            ->whereNotNull('payment_mode_id')
            ->join('payment_modes', 'payment_modes.id', '=', 'invoice_items.payment_mode_id');

        $sql = $builder->toSql();

        $builder = DB::table(DB::raw("($sql) AS A"))
            ->select([
                'name',
                DB::raw('SUM(amount_paid) AS amount_paid')
            ])->groupBy('name');

        $results = $builder->get()->merge($results)->sortByDesc('amount_paid');

        $data = [
            'labels' => [],
            'datasets' => [
                [
                    'label' => 'Payment Modes',
                    'data' => [],
                    'backgroundColor' => []
                ]
            ],
        ];

        foreach ($results as $result) {

            $randColor = $this->rand_color();

            $data['labels'][] = $result->name;
            $data['datasets'][0]['data'][] = $result->amount_paid;

            while (in_array($randColor, $data['datasets'][0]['backgroundColor'])) {

                $randColor = $this->rand_color();

            }

            $data['datasets'][0]['backgroundColor'][] = $randColor;
        }

        return $data;
    }

    public function sales_rest()
    {

        $start_date = trim(request("start_date", null));

        $end_date = trim(request("end_date", null));

        try {

            if (empty($start_date)) {

                $start_date = Carbon::now()->subMonths(1)->startOfDay();

            } else {

                $start_date = Carbon::parse($start_date)->startOfDay();

            }
        } catch (Exception $e) {

            $start_date = Carbon::now()->subMonths(1)->startOfDay();

        }

        try {

            if (empty($end_date)) {

                $end_date = Carbon::now()->endOfDay();

            } else {
                $end_date = Carbon::parse($end_date)->endOfDay();
            }

        } catch (Exception $e) {

            $end_date = Carbon::now()->endOfDay();

        }

        $builder = DB::table('invoice_items')
            ->whereBetween('invoice_items.updated_at', [$start_date, $end_date])
            ->orderBy('type');

        $sql = $builder->toSql();

        //return $builder->get();

        $builder = DB::table(DB::raw("($sql) AS A"))
            ->mergeBindings($builder)
            ->select([
                'type',
                DB::raw('SUM(amount_paid) AS amount_paid')
            ])
            ->where('amount_paid', '>', 0)
            ->orderByDesc('type')
            ->groupBy('type');

        $results = $builder->get();

        $data = [
            'labels' => [],
            'datasets' => [
                [
                    'label' => 'Sales',
                    'data' => [],
                    'backgroundColor' => []
                ]
            ],
        ];

        foreach ($results as $result) {

            $randColor = $this->rand_color();

            if ($result->type === config('constants.payment_item_type.service')) {

                $type = 'Services';

            } else if ($result->type === config('constants.payment_item_type.product')) {

                $type = 'Products';

            } else {

                $type = 'Lab';

            }

            $data['labels'][] = $type;
            $data['datasets'][0]['data'][] = $result->amount_paid;

            while (in_array($randColor, $data['datasets'][0]['backgroundColor'])) {

                $randColor = $this->rand_color();

            }

            $data['datasets'][0]['backgroundColor'][] = $randColor;
        }

        return $data;
    }

    private function rand_color()
    {
        return '#' . str_pad(dechex(mt_rand(0, 0xFFFFFF)), 6, '0', STR_PAD_LEFT);
    }

    public function moh_705b()
    {
        $pageTitle = "MOH 705 B Report";

        $clinics = Clinic::active()->orderBy('name', 'ASC')->get();

        $start_date = trim(request("start_date", null));

        $end_date = trim(request("end_date", null));

        try {

            if (empty($start_date)) {

                $start_date = Carbon::now()->subMonths(1)->startOfDay();

            } else {

                $start_date = Carbon::parse($start_date)->startOfDay();

            }
        } catch (Exception $e) {

            $start_date = Carbon::now()->subMonths(1)->startOfDay();

        }

        try {

            if (empty($end_date)) {

                $end_date = Carbon::now()->endOfDay();

            } else {
                $end_date = Carbon::parse($end_date)->endOfDay();
            }

        } catch (Exception $e) {

            $end_date = Carbon::now()->endOfDay();

        }

        $re = DB::table('diagnoses')
            ->select(
                [
                    DB::raw("DISTINCT diagnoses.uuid, diagnoses.created_at"),
                    'diseases.name AS disease_name',
                ]
            )
            ->whereBetween('diagnoses.created_at', [$start_date, $end_date])
            ->join('diseases', 'diseases.id', '=', 'diagnoses.disease_id')
            ->join('visits', 'visits.id', '=', 'diagnoses.visit_id')
            ->join('clinics', 'clinics.id', '=', 'visits.clinic_id')
            ->join('patients', 'patients.id', '=', 'visits.patient_id')
            ->whereDate('patients.dob', '<', Carbon::now()->subYears(5));

        $cholera = $re
            ->where('diseases.name', 'LIKE', '%Cholera%')
            ->get()
            ->groupBy(function($date) {
                return (int)Carbon::parse($date->created_at)->format('d'); // grouping by days
            })->map->count();

        $Dysentery = $re
            ->where('diseases.name', 'LIKE', '%Dysentery%')
            ->get()
            ->groupBy(function($date) {
                return (int)Carbon::parse($date->created_at)->format('d'); // grouping by days
            })->map->count();
        $Tuberculosis = $re
            ->where('diseases.name', 'LIKE', '%Tuberculosis%')
            ->get()
            ->groupBy(function($date) {
                return (int)Carbon::parse($date->created_at)->format('d'); // grouping by days
            })->map->count();
        $meningitis =$re
            ->where('diseases.name', 'LIKE', '%meningitis%')
            ->get()
            ->groupBy(function($date) {
                return (int)Carbon::parse($date->created_at)->format('d'); // grouping by days
            })->map->count();

        $Tetanus = $re
            ->where('diseases.name', 'LIKE', '%Tetanus%')
            ->get()
            ->groupBy(function($date) {
                return (int)Carbon::parse($date->created_at)->format('d'); // grouping by days
            })->map->count();

        $Measles = $re
            ->where('diseases.name', 'LIKE', '%Measles%')
            ->get()
            ->groupBy(function($date) {
                return (int)Carbon::parse($date->created_at)->format('d'); // grouping by days
            })->map->count();

        $Polio = $re
            ->where('diseases.name', 'LIKE', '%Polio%')
            ->get()
            ->groupBy(function($date) {
                return (int)Carbon::parse($date->created_at)->format('d'); // grouping by days
            })->map->count();

        $Chic = $re
            ->where('diseases.name', 'LIKE', '%Chicken pox%')
            ->get()
            ->groupBy(function($date) {
                return (int)Carbon::parse($date->created_at)->format('d'); // grouping by days
            })->map->count();

        $Hepatitis = $re
            ->where('diseases.name', 'LIKE', '%Hepatitis%')
            ->get()
            ->groupBy(function($date) {
                return (int)Carbon::parse($date->created_at)->format('d'); // grouping by days
            })->map->count();

        $Mumps = $re
            ->where('diseases.name', 'LIKE', '%Mumps%')
            ->get()
            ->groupBy(function($date) {
                return (int)Carbon::parse($date->created_at)->format('d'); // grouping by days
            })->map->count();

        $CMalaria = $re
            ->where('diseases.name', 'LIKE', '%Clinical Malaria%')
            ->get()
            ->groupBy(function($date) {
                return (int)Carbon::parse($date->created_at)->format('d'); // grouping by days
            })->map->count();

        $FMalaria = $re
            ->where('diseases.name', 'LIKE', '%Confirmed Malaria%')
            ->get()
            ->groupBy(function($date) {
                return (int)Carbon::parse($date->created_at)->format('d'); // grouping by days
            })->map->count();

        $Typhoid = $re
            ->where('diseases.name', 'LIKE', '%Typhoid%')
            ->get()
            ->groupBy(function($date) {
                return (int)Carbon::parse($date->created_at)->format('d'); // grouping by days
            })->map->count();

        $Bilharzia = $re
            ->where('diseases.name', 'LIKE', '%Bilharzia%')
            ->get()
            ->groupBy(function($date) {
                return (int)Carbon::parse($date->created_at)->format('d'); // grouping by days
            })->map->count();

        $Intestinal = $re
            ->where('diseases.name', 'LIKE', '%Intestinal Worms%')
            ->get()
            ->groupBy(function($date) {
                return (int)Carbon::parse($date->created_at)->format('d'); // grouping by days
            })->map->count();

        $Malnutrition = $re
            ->where('diseases.name', 'LIKE', '%Malnutrition%')
            ->get()
            ->groupBy(function($date) {
                return (int)Carbon::parse($date->created_at)->format('d'); // grouping by days
            })->map->count();

        $Anemia = $re
            ->where('diseases.name', 'LIKE', '%Anemia%')
            ->get()
            ->groupBy(function($date) {
                return (int)Carbon::parse($date->created_at)->format('d'); // grouping by days
            })->map->count();

        $eye = $re
            ->where('diseases.name', 'LIKE', '%Eye Infection%')
            ->get()
            ->groupBy(function($date) {
                return (int)Carbon::parse($date->created_at)->format('d'); // grouping by days
            })->map->count();

        $ear = $re
            ->where('diseases.name', 'LIKE', '%Ear Infection%')
            ->get()
            ->groupBy(function($date) {
                return (int)Carbon::parse($date->created_at)->format('d'); // grouping by days
            })->map->count();

        $Pneumonia = $re
            ->where('diseases.name', 'LIKE', '%Pneumonia%')
            ->get()
            ->groupBy(function($date) {
                return (int)Carbon::parse($date->created_at)->format('d'); // grouping by days
            })->map->count();

        $Hypertension = $re
            ->where('diseases.name', 'LIKE', '%Hypertension%')
            ->get()
            ->groupBy(function($date) {
                return (int)Carbon::parse($date->created_at)->format('d'); // grouping by days
            })->map->count();

        $Diabetes = $re
            ->where('diseases.name', 'LIKE', '%Diabetes%')
            ->get()
            ->groupBy(function($date) {
                return (int)Carbon::parse($date->created_at)->format('d'); // grouping by days
            })->map->count();

        $Epilepsy = $re
            ->where('diseases.name', 'LIKE', '%Epilepsy%')
            ->get()
            ->groupBy(function($date) {
                return (int)Carbon::parse($date->created_at)->format('d'); // grouping by days
            })->map->count();

        $Dracunculosis = $re
            ->where('diseases.name', 'LIKE', '%Dracunculiasis%')
            ->get()
            ->groupBy(function($date) {
                return (int)Carbon::parse($date->created_at)->format('d'); // grouping by days
            })->map->count();

        $yell = $re
            ->where('diseases.name', 'LIKE', '%Yellow Fever%')
            ->get()
            ->groupBy(function($date) {
                return (int)Carbon::parse($date->created_at)->format('d'); // grouping by days
            })->map->count();

        $Plague = $re
            ->where('diseases.name', 'LIKE', '%Plague%')
            ->get()
            ->groupBy(function($date) {
                return (int)Carbon::parse($date->created_at)->format('d'); // grouping by days
            })->map->count();

        $Brucellosis = $re
            ->where('diseases.name', 'LIKE', '%Brucellosis%')
            ->get()
            ->groupBy(function($date) {
                return (int)Carbon::parse($date->created_at)->format('d'); // grouping by days
            })->map->count();



        return view('analytics::moh-705b', compact(["pageTitle", "clinics"], "cholera", "Dysentery" , "FMalaria",
            "Tuberculosis", "meningitis", "Tetanus", "Polio", "Chic", "Measles", "Hepatitis", "Mumps", "CMalaria",
            "Typhoid", "Bilharzia", "Intestinal", "Malnutrition", "Anemia", "ear", "eye", "Pneumonia", "Hypertension",
            "Diabetes", "Epilepsy", "Dracunculosis", "yell", "Plague", "Brucellosis"));
    }


}
