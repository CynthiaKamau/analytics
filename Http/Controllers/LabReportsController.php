<?php

namespace Modules\Analytics\Http\Controllers;

use App\Entities\Facility\Clinic;
use App\Entities\Facility\InvoiceItem;
use App\Entities\Facility\Invoice;
use App\Entities\Facility\PaymentModeVisit;
use App\Entities\Facility\Scheme;
use Carbon\Carbon;
use Exception;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\DB;

use Maatwebsite\Excel\Facades\Excel;

use Modules\Analytics\Helpers\LabExport;
use Modules\laboratory\Entities\LabTest;
use Modules\laboratory\Entities\LabTestResult;
use Modules\laboratory\Entities\LabTestVisit;


class LabReportsController extends Controller
{
    public function lab_report()
    {
        $pageTitle = 'Laboratory Report';
        $clinics = Clinic::active()->orderBy('name', 'ASC')->get();
        $payment_modes = Scheme::active()->orderBy('name', 'ASC')->get();

        return view('analytics::lab-reports/lab-report', compact('pageTitle', 'clinics', 'payment_modes'));
    }

    public function lab_rest()
    {
        $start_date = request("start_date", '');

        $end_date = request("end_date", '');

        try {
            if (empty(trim($start_date))) {
                $start_date = Carbon::now()->subMonths(1)->startOfDay();
            } else {
                $start_date = Carbon::parse($start_date)->startOfDay();
            }
        } catch (Exception $e) {
            $start_date = Carbon::now()->subMonths(1)->startOfDay();
        }

        try {
            if (empty(trim($end_date))) {
                $end_date = Carbon::now()->endOfDay();
            } else {
                $end_date = Carbon::parse($end_date)->endOfDay();
            }
        } catch (Exception $e) {
            $end_date = Carbon::now()->endOfDay();
        }

        $dateTimeFormat = config('constants.php_date_format');

        $data = [
            "labels" => [],
            "datasets" => [
                [
                    'label' => 'Lab Tests',
                    'backgroundColor' => '#7cb5ec',
                    'borderColor' => '#00c0ef',
                    'data' => [

                    ]
                ]
            ],
        ];

        $builder = LabTestVisit::select(
            DB::raw("sum(invoice_items.amount_paid) as lab_count"),
            DB::raw('lab_tests.name as test_name')
        )
            ->join('lab_tests', 'lab_tests.id', 'lab_test_visits.lab_test_id')
            ->join('invoices', 'invoices.visit_id', 'lab_test_visits.visit_id')
            ->join('invoice_items', 'invoice_items.id', 'lab_test_visits.visit_id')
            ->groupBy('lab_tests.name')
            ->whereBetween('lab_test_visits.created_at', [$start_date, $end_date]);  

        $clinics = request('clinics', []);

        $is_emergency = (int)request('is_emergency', 0);

        $payment_modes = request('payment_modes', []);

        if (!empty($clinics) && is_array($clinics)) {
            $builder->whereHas('visit.clinic', function ($query) use ($clinics) {
                $query->select([
                    DB::raw("DISTINCT *"),
                    DB::raw("count(*) OVER() AS filtered")
                ]);
                $query->whereIn('uuid', $clinics);
            });
        }

        if (!empty($payment_modes) && is_array($payment_modes)) {

            $builder->whereHas('visit.paymentModes.scheme', function ($query) use ($payment_modes) {
                $query->select([
                    DB::raw("DISTINCT *"),
                    DB::raw("count(*) OVER() AS filtered")
                ]);
                $query->whereIn('uuid', $payment_modes);
            });
        }

        if ($is_emergency === 1) {

            $builder->whereHas('visit', function ($query) use ($is_emergency) {
                $query->where('is_emergency', $is_emergency);
            });

        }
            
        $labs = $builder->get()->map(function ($lab) {
            $lab->test_name;
            return $lab;
        })->sortBy(function ($obj, $key) {
            return $obj->test_name;
        });

        foreach ($labs as $lab) {
            $data['labels'][] = $lab->test_name;
            $data['datasets'][0]["data"][] = $lab->lab_count;
        }

        return $data;
    }

    public function lab_test_rest()
    {
        $start_date = request("start_date", '');

        $end_date = request("end_date", '');

        try {
            if (empty(trim($start_date))) {
                $start_date = Carbon::now()->subMonths(1)->startOfDay();
            } else {
                $start_date = Carbon::parse($start_date)->startOfDay();
            }
        } catch (Exception $e) {
            $start_date = Carbon::now()->subMonths(1)->startOfDay();
        }

        try {
            if (empty(trim($end_date))) {
                $end_date = Carbon::now()->endOfDay();
            } else {
                $end_date = Carbon::parse($end_date)->endOfDay();
            }
        } catch (Exception $e) {
            $end_date = Carbon::now()->endOfDay();
        }

        $dateTimeFormat = config('constants.php_date_format');

        $data = [
            "labels" => [],
            "datasets" => [
                [
                    'label' => 'Lab Tests',
                    'backgroundColor' => '#7cb5ec',
                    'borderColor' => '#00c0ef',
                    'data' => [

                    ]
                ]
            ],
        ];
               
        $builder = LabTestVisit::select(
            DB::raw("count(*) as lab_results_count"),
            DB::raw('lab_tests.name as test_name')
        )
            ->join('lab_tests', 'lab_tests.id', 'lab_test_visits.lab_test_id')
            ->groupBy('lab_tests.name')
            ->whereBetween('lab_test_visits.created_at', [$start_date, $end_date]);

        $clinics = request('clinics', []);

        $is_emergency = (int)request('is_emergency', 0);

        $payment_modes = Scheme::active()->orderBy('name', 'ASC')->get();

        if (!empty($clinics) && is_array($clinics)) {
            $builder->whereHas('visit.clinic', function ($query) use ($clinics) {
                $query->select([
                    DB::raw("DISTINCT *"),
                    DB::raw("count(*) OVER() AS filtered")
                ]);
                $query->whereIn('uuid', $clinics);
            });
        }

        if (!empty($payment_modes) && is_array($payment_modes)) {

            $builder->whereHas('visit.paymentModes.scheme', function ($query) use ($payment_modes) {
                $query->select([
                    DB::raw("DISTINCT *"),
                    DB::raw("count(*) OVER() AS filtered")
                ]);
                $query->whereIn('uuid', $payment_modes);
            });
        }

        if ($is_emergency === 1) {

            $builder->whereHas('visit', function ($query) use ($is_emergency) {
                $query->where('is_emergency', $is_emergency);
            });

        }

        $lebs = $builder->get()->map(function ($leb) {
            $leb->test_name;
            return $leb;
        })->sortBy(function ($obj, $key) {
            return $obj->test_name;
        });

        foreach ($lebs as $leb) {
            $data['labels'][] = $leb->test_name;
            $data['datasets'][0]["data"][] = $leb->lab_results_count;
        }

        return $data;
    }

    public function lab_payments_rest()
    {
        $start_date = request("start_date", '');

        $end_date = request("end_date", '');

        try {
            if (empty(trim($start_date))) {
                $start_date = Carbon::now()->subMonths(1)->startOfDay();
            } else {
                $start_date = Carbon::parse($start_date)->startOfDay();
            }
        } catch (Exception $e) {
            $start_date = Carbon::now()->subMonths(1)->startOfDay();
        }

        try {
            if (empty(trim($end_date))) {
                $end_date = Carbon::now()->endOfDay();
            } else {
                $end_date = Carbon::parse($end_date)->endOfDay();
            }
        } catch (Exception $e) {
            $end_date = Carbon::now()->endOfDay();
        }

        $dateTimeFormat = config('constants.php_date_format');

        $data = [
            "labels" => [],
            "datasets" => [
                [
                    'label' => 'Lab Tests',
                    'backgroundColor' => '#7cb5ec',
                    'borderColor' => '#00c0ef',
                    'data' => [

                    ]
                ]
            ],
        ];
        
        $builder = LabTestVisit::select(
            DB::raw("count(*) as payment_count"),
            DB::raw("COALESCE(payment_modes.name, schemes.name) as payment_name")
        )
        ->join('payment_mode_visits', 'payment_mode_visits.visit_id', 'lab_test_visits.visit_id')
        ->leftJoin('payment_modes', 'payment_modes.id', 'payment_mode_visits.payment_mode_id')
        ->leftJoin('schemes', 'schemes.id', 'payment_mode_visits.scheme_id')
        ->whereBetween('lab_test_visits.created_at', [$start_date, $end_date])
        ->where(static function ($query) {
            $query
                ->whereNotNull('payment_modes.name')
                ->orWhereNotNull('schemes.name');
        })
        ->groupBy(DB::raw('COALESCE(payment_modes.name, schemes.name)'));

        $clinics = request('clinics', []);

        $is_emergency = (int)request('is_emergency', 0);

        $payment_modes = request('payment_modes', []);

        if (!empty($clinics) && is_array($clinics)) {
            $builder->whereHas('visit.clinic', function ($query) use ($clinics) {
                $query->select([
                    DB::raw("DISTINCT *"),
                    DB::raw("count(*) OVER() AS filtered")
                ]);
                $query->whereIn('uuid', $clinics);
            });
        }

        if (!empty($payment_modes) && is_array($payment_modes)) {

            $builder->whereHas('visit.paymentModes.scheme', function ($query) use ($payment_modes) {
                $query->select([
                    DB::raw("DISTINCT *"),
                    DB::raw("count(*) OVER() AS filtered")
                ]);
                $query->whereIn('uuid', $payment_modes);
            });
        }

        if ($is_emergency === 1) {

            $builder->whereHas('visit', function ($query) use ($is_emergency) {
                $query->where('is_emergency', $is_emergency);
            });

        }

        $lubs = $builder->get()->map(function ($lub) {
            $lub->payment_name;
            return $lub;
        })->sortBy(function ($obj, $key) {
            return $obj->payment_name;
        });

        foreach ($lubs as $lub) {
            $data['labels'][] = $lub->payment_name;
            $data['datasets'][0]["data"][] = $lub->payment_count;
        }

        return $data;
    }

    public function lab_revenue_rest()
    {
        $start_date = request("start_date", '');

        $end_date = request("end_date", '');

        try {
            if (empty(trim($start_date))) {
                $start_date = Carbon::now()->subMonths(1)->startOfDay();
            } else {
                $start_date = Carbon::parse($start_date)->startOfDay();
            }
        } catch (Exception $e) {
            $start_date = Carbon::now()->subMonths(1)->startOfDay();
        }

        try {
            if (empty(trim($end_date))) {
                $end_date = Carbon::now()->endOfDay();
            } else {
                $end_date = Carbon::parse($end_date)->endOfDay();
            }
        } catch (Exception $e) {
            $end_date = Carbon::now()->endOfDay();
        }

        $dateTimeFormat = config('constants.php_date_format');

        $data = [
            "labels" => [],
            "datasets" => [
                [
                    'label' => 'Expected Amount',
                    'backgroundColor' => '#7cb5ec',
                    'borderColor' => '#00c0ef',
                    'data' => [],
                ],
                [
                    'label' => 'Paid Amount',
                    'backgroundColor' => 'rgb(255,127,80)',
                    'borderColor' => '#00c0ef',
                    'data' => [],
                ]
            ],
        ];

        $builder = LabTestVisit::select(
                DB::raw("sum(invoice_items.total_amount) as expected_amount"),
                DB::raw("COALESCE(schemes.name, payment_modes.name) as payment_name"),
                DB::raw("sum(invoice_items.amount_paid) as amount_paid")
            )->join('payment_mode_visits', 'payment_mode_visits.visit_id', 'lab_test_visits.visit_id')
            ->leftJoin('schemes', 'schemes.id', 'payment_mode_visits.scheme_id')
            ->leftJoin('payment_modes', 'payment_modes.id', 'payment_mode_visits.payment_mode_id')
            ->join('invoice_items', 'invoice_items.id', 'lab_test_visits.visit_id')
            ->where(static function ($query) {
                $query
                    ->whereNotNull('payment_modes.name')
                    ->orWhereNotNull('schemes.name');
            })
            ->whereBetween('lab_test_visits.created_at', [$start_date, $end_date])
            ->groupBy(DB::raw('schemes.name, payment_modes.name'));

        $clinics = request('clinics', []);

        $is_emergency = (int)request('is_emergency', 0);

        $payment_modes = request('payment_modes', []);

        if (!empty($clinics) && is_array($clinics)) {
            $builder->whereHas('visit.clinic', function ($query) use ($clinics) {
                $query->select([
                    DB::raw("DISTINCT *"),
                    DB::raw("count(*) OVER() AS filtered")
                ]);
                $query->whereIn('uuid', $clinics);
            });
        }

        if (!empty($payment_modes) && is_array($payment_modes)) {

            $builder->whereHas('visit.paymentModes.scheme', function ($query) use ($payment_modes) {
                $query->select([
                    DB::raw("DISTINCT *"),
                    DB::raw("count(*) OVER() AS filtered")
                ]);
                $query->whereIn('uuid', $payment_modes);
            });
        }

        if ($is_emergency === 1) {
            $builder->whereHas('visit', function ($query) use ($is_emergency) {
                $query->where('is_emergency', $is_emergency);
            });
        }

        $labs = $builder->get()->map(function ($lab) {
            $lab->payment_name;
            return $lab;
        })->sortBy(function ($obj, $key) {
            return $obj->payment_name;
        });

        foreach ($labs as $lab) {
            $data['labels'][] = $lab->payment_name;
            $data['datasets'][0]["data"][] = $lab->expected_amount;
            $data['datasets'][1]["data"][] = $lab->amount_paid;
        }

        return $data;

    }

    public function labtest_list_download()
    {
        $export = new LabExport();

        return Excel::download($export, 'labtests.xlsx');
    }


}


