<?php

namespace Modules\Analytics\Http\Controllers;

use App\Entities\Facility;
use App\Entities\Facility\Clinic;
use App\Entities\Facility\InvoiceItem;
use App\Entities\Facility\Invoice;
use App\Entities\Facility\Patient;
use App\Entities\Facility\PaymentModeVisit;
use App\Entities\Facility\PaymentMode;
use App\Entities\Facility\Scheme;
use Modules\laboratory\Entities\LabTestResult;

use Excel;
use App\Entities\Facility\Visit;

use Carbon\Carbon;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\DB;

use Modules\Consultation\Entities\Diagnosis;
use Modules\Analytics\Helpers\PatientExport;
use Modules\Laboratory\Entities\LabTestVisit;
use PhpParser\Node\Expr\Cast\Object_;

class ReportsController extends Controller
{

    public function index()
    {
        $disease_categories = DB::table('disease_categories')
            ->select(
                [
                    'disease_categories.name as name',
                    'disease_categories.id as id',
                ]
            )
            ->where('disease_categories.status', '=', '1')
            ->get();


        $builder2 = DB::table('schemes')
            ->select(
                [
                    DB::raw("DISTINCT schemes.name"),
                ]
            )
            ->where('schemes.status', '=', '1');

        $diagnoses = $builder2->get();

        $scheme = collect($diagnoses);

        $builder2 = DB::table('payment_modes')
            ->select(
                [
                    DB::raw("DISTINCT payment_modes.name"),
                ]
            )
            ->where('payment_modes.status', '=', '1');

        $diagnoses = $builder2->get();

        $payment = collect($diagnoses);

        $clinics = Clinic::active()->orderBy('name', 'ASC')->get();
        return view('analytics::visit-report', compact('scheme', 'payment', 'disease_categories', 'clinics'));
    }

    public function sales_reports()
    {
        $pageTitle = 'Sales Report';
        $clinics = Clinic::active()->orderBy('name', 'ASC')->get();
        $payment_modes = Scheme::active()->orderBy('name', 'ASC')->get();

        return view('analytics::general-reports/sales-report', compact('pageTitle', 'clinics', 'payment_modes'));
    }

    public function sales_rest()
    {
        $start = (int)request('start', '0');
        $length = (int)request('length', 100);
        $is_emergency = (int)request('is_emergency', 0);
        $startDate = request("start_date", null);
        $endDate = request("end_date", null);
        $clinics = request('clinics', []);
        $payment_modes = request('payment_modes', []);
        
        try {
            if (empty(trim($startDate))) {

                $startDate = Carbon::today()->subMonths(1)->startOfDay();
            } else {

                $startDate = Carbon::parse($startDate)->startOfDay();
            }
        } catch (Exception $e) {

            $startDate = Carbon::today()->subMonths(1)->startOfDay();
        }

        try {
            if (empty(trim($endDate))) {

                $endDate = Carbon::today()->endOfDay();
            } else {

                $endDate = Carbon::parse($endDate)->endOfDay();
            }
        } catch (Exception $e) {

            $endDate = Carbon::today()->endOfDay();
        }

        $builder = Invoice::select([

            DB::raw("TO_CHAR(invoices.created_at, 'YYYY-MM-DD') AS date"),
            DB::raw("invoice_items.amount_paid as sales_count"),
            DB::raw("COALESCE(payment_modes.name, schemes.name) as payment_name"),

        ])
            ->join('payment_mode_visits', 'payment_mode_visits.visit_id', 'invoices.visit_id')
            ->leftJoin('schemes', 'schemes.id', 'payment_mode_visits.scheme_id')
            ->leftJoin('payment_modes', 'payment_modes.id', 'payment_mode_visits.payment_mode_id')
            ->leftJoin('invoice_items', 'invoice_items.id', 'invoices.visit_id')
            ->where(static function ($query) {
                $query
                    ->whereNotNull('payment_modes.name')
                    ->orWhereNotNull('schemes.name');
            })
            ->groupBy(DB::raw("payment_modes.name, schemes.name, invoices.created_at, invoice_items.amount_paid"))
            ->whereBetween('invoices.created_at', [$startDate, $endDate]);

        if (!empty($clinics) && is_array($clinics)) {

            $builder->whereHas('visit.clinic', function ($query) use ($clinics) {
                $query->select([
                    DB::raw("DISTINCT *"),
                    DB::raw("count(*) OVER() AS filtered")
                ]);
                $query->whereIn('uuid', $clinics);
            });
        }

        if (!empty($payment_modes) && is_array($payment_modes)) {

            $builder->whereHas('visit.paymentModes.scheme', function ($query) use ($payment_modes) {
                $query->select([
                    DB::raw("DISTINCT *"),
                    DB::raw("count(*) OVER() AS filtered")
                ]);
                $query->whereIn('uuid', $payment_modes);
            });
        }

        if ($is_emergency === 1) {
            $builder->whereHas('visit', function ($query) use ($is_emergency) {
                $query->where('is_emergency', $is_emergency);
            });
        }

        $data = [
            "labels" => [],
            "datasets" => [
                [
                    'label' => 'Visits',
                    'backgroundColor' => '#00a65a',
                    'borderColor' => '#00a65a',
                    'pointRadius' => 5,
                    'showLine' => true,
                    'steppedLine' => false,
                    'fill' => false,
                    'data' => [

                    ]
                ]
            ],
        ];

        // $builder->orderBy('id', 'asc');

        // if ($length != -1) {

        //     $builder->offset($start);

        //     $builder->limit($length);

        // }

        $requests = $builder->get();

        $num = $requests->groupBy('date')->map(function ($row) {
            return $row->sum('sales_count');
        });

        foreach ($num as $key => $value) {

            $data['labels'][] = $key;
            $data['datasets'][0]["data"][] = $value;

        }

        return $data;

    }

    public function sales_dates()
    {
        $pageTitle = 'Sales Report Agaist Dates';
        $clinics = Clinic::active()->orderBy('name', 'ASC')->get();
        $schemes = Scheme::active()->orderBy('name', 'ASC')->get();
        $payment_modes = PaymentMode::active()->orderBy('name', 'ASC')->get();

        return view('analytics::general-reports/sales-dates', compact('pageTitle', 'clinics', 'schemes'));
    }

    public function sales_date_rest()
    {
        $start = (int)request('start', '0');
        $length = (int)request('length', 100);
        $is_emergency = (int)request('is_emergency', 0);
        $startDate = request("start_date", null);
        $endDate = request("end_date", null);
        $clinics = request('clinics', []);
        $schemes = request('schemes', []);

        try {
            if (empty(trim($startDate))) {

                $startDate = Carbon::today()->subMonths(1)->startOfDay();

            } else {

                $startDate = Carbon::parse($startDate)->startOfDay();

            }
        } catch (Exception $e) {

            $startDate = Carbon::today()->subMonths(1)->startOfDay();

        }

        try {
            if (empty(trim($endDate))) {

                $endDate = Carbon::today()->endOfDay();

            } else {

                $endDate = Carbon::parse($endDate)->endOfDay();

            }

        } catch (Exception $e) {

            $endDate = Carbon::today()->endOfDay();

        }

        $builder = Invoice::select([

            DB::raw("TO_CHAR(invoices.created_at, 'YYYY-MM-DD') AS date"),
            DB::raw("invoice_items.amount_paid"),
            DB::raw("COALESCE(payment_modes.name, schemes.name) as payment_name")

        ])
        ->join('payment_mode_visits', 'payment_mode_visits.visit_id', 'invoices.visit_id')
        ->leftJoin('schemes', 'schemes.id', 'payment_mode_visits.scheme_id')
        ->leftJoin('payment_modes', 'payment_modes.id', 'payment_mode_visits.payment_mode_id')
        ->leftJoin('invoice_items', 'invoice_items.id', 'invoices.visit_id')
        ->where(static function ($query) {
            $query
                ->whereNotNull('payment_modes.name')
                ->orWhereNotNull('schemes.name');
        })
        ->groupBy(DB::raw("payment_modes.name, schemes.name, invoices.created_at, invoice_items.amount_paid"))
        ->whereBetween('invoices.created_at', [$startDate, $endDate]);

        if (!empty($clinics) && is_array($clinics)) {

            $builder->whereHas('visit.clinic', function ($query) use ($clinics) {
                $query->select([
                    DB::raw("DISTINCT *"),
                    DB::raw("count(*) OVER() AS filtered")
                ]);
                $query->whereIn('uuid', $clinics);
            });

        }

        if (!empty($schemes) && is_array($schemes)) {

            $builder->whereHas('visit.paymentModes.scheme', function ($query) use ($schemes) {
                $query->select([
                    DB::raw("DISTINCT *"),
                    DB::raw("count(*) OVER() AS filtered")
                ]);
                $query->whereIn('uuid', $schemes);
            });

        }

        if ($is_emergency === 1) {
            $builder->whereHas('visit', function ($query) use ($is_emergency) {
                $query->where('is_emergency', $is_emergency);
            });
        }
       
        $requests = $builder->get()->groupBy('date');

        $num = $requests->map(function ($row) {

            $result = array();

            $ttl = $row->sum('amount_paid');

            $row = $row->toArray();

            $final_array = [];

            foreach($row as $arr){
            $final_array[$arr['payment_name']]['payment_name'] = $arr['payment_name'];
            $final_array[$arr['payment_name']]['amount_paid'] = (isset($final_array[$arr['payment_name']]['amount_paid']))? $final_array[$arr['payment_name']]['amount_paid']+$arr['amount_paid'] : $arr['amount_paid'];    
            }
            $final_array = array_values($final_array);

            return [$final_array, $ttl];

        });

        return $num;

    }

    public function corporate_sale()
    {
        $pageTitle = "Corporates Sales";
        $clinics = Clinic::active()->get();
        return view('analytics::general-reports/corporate_sales', compact('pageTitle', 'clinics'));
    }

    public function corporate_rest(Request $request, Facility $facility)
    {
        $start_date = request("start_date", null);

        $end_date = request("end_date", null);

        try {
            if (empty(trim($start_date))) {

                $start_date = Carbon::now()->subMonths(1)->startOfDay();

            } else {

                $start_date = Carbon::parse($start_date)->startOfDay();

            }
        } catch (Exception $e) {

            $start_date = Carbon::now()->subMonths(1)->startOfDay();

        }

        try {
            if (empty(trim($end_date))) {

                $end_date = Carbon::now()->endOfDay();

            } else {

                $end_date = Carbon::parse($end_date)->endOfDay();

            }

        } catch (Exception $e) {

            $end_date = Carbon::now()->endOfDay();

        }

        $builder = Invoice::select(

            DB::raw("TO_CHAR(invoices.created_at, 'YYYY-MM-DD') AS date"),
            DB::raw("invoice_items.amount_paid as amount_paid"),
            DB::raw("invoice_items.total_amount as bill"),
            DB::raw("invoice_items.balance as amount_balance"),
            DB::raw("COALESCE(payment_modes.name, schemes.name) as payment_name"),
            DB::raw("invoices.number as invoice_number"),
            DB::raw("patients.number as patient_number"),
            DB::raw("CONCAT(patients.first_name,' ',patients.middle_name,' ',patients.last_name)  AS full_name")

        )
        ->join('payment_mode_visits', 'payment_mode_visits.visit_id', 'invoices.visit_id')
        ->leftJoin('visits', 'visits.id', 'payment_mode_visits.visit_id')
        ->leftJoin('schemes', 'schemes.id', 'payment_mode_visits.scheme_id')
        ->leftJoin('payment_modes', 'payment_modes.id', 'payment_mode_visits.payment_mode_id')
        ->leftJoin('invoice_items', 'invoice_items.id', 'invoices.visit_id')
        ->leftJoin('patients', 'patients.id', 'visits.patient_id')
        ->orderBy('invoices.created_at', 'asc')
        ->whereBetween('invoices.created_at', [$start_date, $end_date]);

        $status = request('status', null);
        $search = request('search', null);
        $start = (int)request('start', '0');
        $length = (int)request('length', 100);
        $clinics = request('clinics', []);
        $is_emergency = (int)request('is_emergency', 0);
        


        switch ($status) {
            case 'active':
                $builder->whereNull('ended_at');
                break;
            case 'ended':
                $builder->whereNotNull('ended_at');
                break;
            default:
                break;
        }

        if (!empty($clinics) && is_array($clinics)) {

            $builder->whereHas('visit.clinic', function ($query) use ($clinics) {
                $query->select([
                    DB::raw("DISTINCT *"),
                    DB::raw("count(*) OVER() AS filtered")
                ]);
                $query->whereIn('uuid', $clinics);
            });
        }

        if (!empty($search) && !empty($search['value'])) {

            $builder->whereHas('visit.patient', function ($query) use ($search) {
                $query->search($search['value']);
            });

        }

        if ($is_emergency === 1) {

            $builder->where('is_emergency', $is_emergency);
        }

        if ($length != -1) {

            $builder->offset($start);

            $builder->limit($length);

        }

        $corporates = $builder->get();

        $recordsFiltered = 0;
        $total_amount = 0;
        $total_bill = 0;
        $total_balances = 0;

        $recordsTotal = Invoice::whereBetween('created_at', [$start_date, $end_date])->count();
        foreach($corporates as $row)
        {
            $sub_array = array();
            $total_amount = $total_amount + floatval($row['amount_paid']);
            $total_bill = $total_bill + floatval($row['bill']);
            $total_balances = $total_balances + floatval($row['amount_balance']);
        }

        $corporates = $corporates->map(function ($corporate) use (&$recordsFiltered) {

            $recordsFiltered = $corporate['filtered'];
        


            return [
                'date' => $corporate->date,
                'amount_paid' => $corporate->amount_paid,
                'bill' => $corporate->bill,
                'amount_balance' => $corporate->amount_balance,
                'patient_number' => $corporate->patient_number,
                'invoice_number' => $corporate->invoice_number,
                'payment_name' => $corporate->payment_name,
                'full_name' => $corporate->full_name,
            ];
        });

        return [
            'data' => $corporates,
            'draw' => request('draw', 0),
            'recordsTotal' => $recordsTotal,
            'recordsFiltered' => $recordsFiltered,
            'billTotal' => $total_bill,
            'total' => $total_amount,
            'balanceTotal' => $total_balances
        ];

    }

    public function cash_sale()
    {
        $pageTitle = "Cash Sales";
        $clinics = Clinic::active()->get();
        return view('analytics::general-reports/cash-sales', compact('pageTitle', 'clinics'));
    }

    public function cash_rest()
    {
        $start_date = request("start_date", null);

        $end_date = request("end_date", null);

        try {
            if (empty(trim($start_date))) {

                $start_date = Carbon::now()->subMonths(1)->startOfDay();

            } else {

                $start_date = Carbon::parse($start_date)->startOfDay();

            }
        } catch (Exception $e) {

            $start_date = Carbon::now()->subMonths(1)->startOfDay();

        }

        try {
            if (empty(trim($end_date))) {

                $end_date = Carbon::now()->endOfDay();

            } else {

                $end_date = Carbon::parse($end_date)->endOfDay();

            }

        } catch (Exception $e) {

            $end_date = Carbon::now()->endOfDay();

        }

        $builder = Invoice::select([

            DB::raw("TO_CHAR(invoices.created_at, 'YYYY-MM-DD') AS date"),
            DB::raw("invoice_items.amount_paid as amount_paid"),
            DB::raw("COALESCE(payment_modes.name, schemes.name) as payment_name"),
            DB::raw("CONCAT(patients.first_name,' ',patients.middle_name,' ',patients.last_name)  AS full_name"),

        ])
        ->join('payment_mode_visits', 'payment_mode_visits.visit_id', 'invoices.visit_id')
        ->leftJoin('visits', 'visits.id', 'payment_mode_visits.visit_id')
        ->leftJoin('schemes', 'schemes.id', 'payment_mode_visits.scheme_id')
        ->leftJoin('payment_modes', 'payment_modes.id', 'payment_mode_visits.payment_mode_id')
        ->leftJoin('invoice_items', 'invoice_items.id', 'invoices.visit_id')
        ->leftJoin('patients', 'patients.id', 'visits.patient_id')
        ->orderBy('invoices.created_at', 'asc')
        ->where('invoice_items.amount_paid', '>', 0)
        ->whereBetween('invoices.created_at', [$start_date, $end_date])
        ->where('payment_modes.name', '=', 'Cash');

        $status = request('status', null);
        $search = request('search', '');
        $start = (int)request('start', '0');
        $length = (int)request('length', 100);
        $clinics = request('clinics', []);
        $is_emergency = (int)request('is_emergency', 0);

        switch ($status) {
            case 'active':
                $builder->whereNull('ended_at');
                break;
            case 'ended':
                $builder->whereNotNull('ended_at');
                break;
            default:
                break;
        }

        if (!empty($clinics) && is_array($clinics)) {

            $builder->whereHas('visit.clinic', function ($query) use ($clinics) {
                $query->select([
                    DB::raw("DISTINCT *"),
                    DB::raw("count(*) OVER() AS filtered")
                ]);
                $query->whereIn('uuid', $clinics);
            });
        }

        if (!empty($search) && !empty($search['value'])) {

            $builder->whereHas('visit.patient', function ($query) use ($search) {
                $query->search($search['value']);
            });

        }

        if ($is_emergency === 1) {

            $builder->where('is_emergency', $is_emergency);
        }

        if ($length != -1) {

            $builder->offset($start);

            $builder->limit($length);

        }

        $visits = $builder->get();

        $recordsFiltered = 0;

        $recordsTotal = Invoice::whereBetween('created_at', [$start_date, $end_date])->count();
        $total_amount = 0;

        foreach($visits as $row)
        {
            $sub_array = array();
            $total_amount = $total_amount + floatval($row['amount_paid']);
        }

        $visits = $visits->map(function ($visit) use (&$recordsFiltered) {

            $recordsFiltered = $visit['filtered'];
        


            return [
                'date' => $visit->date,
                'amount_paid' => $visit->amount_paid,
                'full_name' => $visit->full_name,
            ];
        });

        return [
            'data' => $visits,
            'draw' => request('draw', 0),
            'recordsTotal' => $recordsTotal,
            'recordsFiltered' => $recordsFiltered,
            'total' => $total_amount
        ];

    }

    public function visit_scheme_rest()
    {
        $start_date = request("start_date", '');
        $end_date = request("end_date", '');

        try {
            if (empty(trim($start_date))) {
                $start_date = Carbon::now()->subMonths(1);
            } else {
                $start_date = Carbon::parse($start_date);
            }
        } catch (Exception $e) {
            $start_date = Carbon::now()->subMonths(1);
        }

        $start_date = $start_date->format('Y-m-d') . " 00:00:00";

        try {
            if (empty(trim($end_date))) {
                $end_date = Carbon::now();
            } else {
                $end_date = Carbon::parse($end_date);
            }
        } catch (Exception $e) {
            $end_date = Carbon::now();
        }

        $end_date = $end_date->format('Y-m-d') . " 23:59:59";


        $data = [
            "labels" => [],
            "datasets" => [
                [
                    'label' => 'Visits',
                    'backgroundColor' => '#7cb5ec',
                    'borderColor' => '#00c0ef',
                    'showLine' => true,
                    'steppedLine' => false,
                    'fill' => false,
                    'data' => []
                ]
            ],
        ];

        $scheme_diag = DB::table('payment_mode_visits')
            ->select([
                DB::raw("DISTINCT payment_mode_visits.uuid, payment_mode_visits.visit_id"),
                'schemes.name as payment_mode',
                'diagnoses.uuid as diagnoses_uuid',
                'payment_mode_visits.created_at'
            ])
            ->join('schemes', 'schemes.id', '=', 'payment_mode_visits.scheme_id')
            ->join('diagnoses', 'diagnoses.visit_id', '=', 'payment_mode_visits.visit_id')
            ->whereBetween('payment_mode_visits.created_at', [$start_date, $end_date])
            ->get();
        $re = DB::table('payment_mode_visits')
            ->select([
                DB::raw("DISTINCT payment_mode_visits.uuid, payment_mode_visits.visit_id"),
                'payment_modes.name as payment_mode',
                'diagnoses.uuid as diagnoses_uuid',
                'payment_mode_visits.created_at'
            ])
            ->join('payment_modes', 'payment_modes.id', '=', 'payment_mode_visits.payment_mode_id')
            ->join('diagnoses', 'diagnoses.visit_id', '=', 'payment_mode_visits.visit_id')
            ->whereBetween('payment_mode_visits.created_at', [$start_date, $end_date])
            ->get();

        foreach ($scheme_diag as $d) {
            $re[] = $d;
        }

        $re = $re->each(function ($item, $key) {
            $resultsPayment = DB::table('diagnoses')
                ->select(
                    [
                        DB::raw("DISTINCT diagnoses.uuid, diagnoses.created_at"),
                        'diagnoses.created_at AS diagnosis_date',
                        'diseases.name AS disease_name',
                        'visits.number AS visit_number',
                        'visits.id AS visit_id',
                        'diagnoses.disease_id AS disease_id',
                        'patients.first_name',
                        'patients.last_name',
                        'visits.is_emergency',
                        'clinics.name AS clinic_name',

                    ]
                )
                ->where('diagnoses.uuid', '=', $item->diagnoses_uuid)
                ->join('visits', 'visits.id', '=', 'diagnoses.visit_id')
                ->join('clinics', 'clinics.id', '=', 'visits.clinic_id')
                ->join('patients', 'patients.id', '=', 'visits.patient_id')
                ->join('diseases', 'diseases.id', '=', 'diagnoses.disease_id')
                ->get();

            $item->disease_name = $resultsPayment['0']->disease_name;
            $item->disease_id = $resultsPayment['0']->disease_id;

            $disease_categories = DB::table('disease_disease_category')
                ->select(
                    [
                        'disease_categories.name as category',
                        'diseases.name as disease',
                    ]
                )
                ->where('disease_disease_category.disease_id', '=', $item->disease_id)
                ->join('disease_categories', 'disease_categories.id', '=', 'disease_disease_category.disease_category_id')
                ->join('diseases', 'diseases.id', '=', 'disease_disease_category.disease_id')
                ->get();
            try {
                $item->category = $disease_categories['0']->category;
            } catch (Exception $e) {
                $item->category = 'other';
            }
        });

        $diagnosis_grouped = $re->groupBy('payment_mode');

        $diagnosis_count = $re->groupBy('payment_mode')->map->count();

        $builder2 = DB::table('schemes')
            ->select(
                [
                    DB::raw("DISTINCT schemes.name"),
                ]
            )
            ->where('status', '=', '1');

        $diagnoses = $builder2->get();

        $scheme = collect($diagnoses);

        $builder2 = DB::table('payment_modes')
            ->select(
                [
                    DB::raw("DISTINCT payment_modes.name"),
                ]
            )
            ->where('status', '=', '1');

        $diagnoses = $builder2->get();

        $payment = collect($diagnoses);

        $list = array();
        $keys = array();

        foreach ($scheme as $item) {
            $list[] = $item->name;
        }
        foreach ($payment as $item) {
            $list[] = $item->name;
        }
        foreach ($diagnosis_grouped as $key => $value) {
            $keys[] = $key;
        }

        $data['labels'] = $list;

        $cat = request('scheme', []);

        if (!empty($cat) && is_array($cat)) {
            $all = array();
            foreach ($cat as $item) {
                $t = array();
                foreach ($list as $value) {
                    $count = 0;
                    if (in_array($value, $keys))
                        foreach ($diagnosis_grouped[$value] as $b) {
                            if ($b->category == $item)
                                $count++;
                        }
                    $t[] = $count;
                }
                $all[] = $t;
            }
            for ($i = 0; $i < count($all[0]); $i++) {
                $count = 0;
                for ($j = 0; $j < count($all); $j++) {
                    $count = $count + $all[$j][$i];
                }
                $data['datasets'][0]["data"][] = $count;
            }
        } else
            foreach ($list as $visit) {
                try {
                    $data['datasets'][0]["data"][] = $diagnosis_count[$visit];
                } catch (Exception $e) {
                    $data['datasets'][0]["data"][] = 0;
                }
            }

        return [$data, $diagnosis_count];
    }

    public function all_visit_rest()
    {
        $start_date = request("start_date", '');
        $end_date = request("end_date", '');

        try {
            if (empty(trim($start_date))) {
                $start_date = Carbon::now()->subMonths(1);
            } else {
                $start_date = Carbon::parse($start_date);
            }
        } catch (Exception $e) {
            $start_date = Carbon::now()->subMonths(1);
        }

        $start_date = $start_date->format('Y-m-d') . " 00:00:00";

        try {
            if (empty(trim($end_date))) {
                $end_date = Carbon::now();
            } else {
                $end_date = Carbon::parse($end_date);
            }
        } catch (Exception $e) {
            $end_date = Carbon::now();
        }

        $end_date = $end_date->format('Y-m-d') . " 23:59:59";


        $data = [
            "labels" => [],
            "datasets" => [
                [
                    'label' => 'Visits',
                    'backgroundColor' => '#7cb5ec',
                    'borderColor' => '#00c0ef',
                    'showLine' => true,
                    'steppedLine' => false,
                    'fill' => false,
                    'data' => []
                ]
            ],
        ];
        $clinics = request('clinic', []);

        $scheme_diag = DB::table('payment_mode_visits')
            ->select([
                DB::raw("DISTINCT payment_mode_visits.uuid, payment_mode_visits.visit_id"),
                'schemes.name as payment_mode',
                'payment_mode_visits.created_at',
                'visits.clinic_id as clinic'
            ])
            ->join('schemes', 'schemes.id', '=', 'payment_mode_visits.scheme_id')
            ->join('visits', 'visits.id', '=', 'payment_mode_visits.visit_id')
            ->whereBetween('payment_mode_visits.created_at', [$start_date, $end_date])
            ->get();

        $re = DB::table('payment_mode_visits')
            ->select([
                DB::raw("DISTINCT payment_mode_visits.uuid, payment_mode_visits.visit_id"),
                'payment_modes.name as payment_mode',
                'payment_mode_visits.created_at',
                'visits.clinic_id as clinic'
            ])
            ->join('payment_modes', 'payment_modes.id', '=', 'payment_mode_visits.payment_mode_id')
            ->join('visits', 'visits.id', '=', 'payment_mode_visits.visit_id')
            ->whereBetween('payment_mode_visits.created_at', [$start_date, $end_date])
            ->get();

        foreach ($scheme_diag as $d) {
            $re[] = $d;
        }

        if (!empty($clinics) && is_array($clinics)) {
            $scheme_diag = DB::table('payment_mode_visits')
                ->select([
                    DB::raw("DISTINCT payment_mode_visits.uuid, payment_mode_visits.visit_id"),
                    'schemes.name as payment_mode',
                    'payment_mode_visits.created_at',
                    'visits.clinic_id as clinic'
                ])
                ->join('schemes', 'schemes.id', '=', 'payment_mode_visits.scheme_id')
                ->join('visits', 'visits.id', '=', 'payment_mode_visits.visit_id')
                ->whereBetween('payment_mode_visits.created_at', [$start_date, $end_date])
                ->whereIn('visits.clinic_id', $clinics)
                ->get();

            $re = DB::table('payment_mode_visits')
                ->select([
                    DB::raw("DISTINCT payment_mode_visits.uuid, payment_mode_visits.visit_id"),
                    'payment_modes.name as payment_mode',
                    'payment_mode_visits.created_at',
                    'visits.clinic_id as clinic'
                ])
                ->join('payment_modes', 'payment_modes.id', '=', 'payment_mode_visits.payment_mode_id')
                ->join('visits', 'visits.id', '=', 'payment_mode_visits.visit_id')
                ->whereBetween('payment_mode_visits.created_at', [$start_date, $end_date])
                ->whereIn('visits.clinic_id', $clinics)
                ->get();

            foreach ($scheme_diag as $d) {
                $re[] = $d;
            }

        }

        $diagnosis_count = $re->groupBy('payment_mode')->map->count();

        $builder2 = DB::table('schemes')
            ->select(
                [
                    DB::raw("DISTINCT schemes.name"),
                ]
            )
            ->where('status', '=', '1');

        $diagnoses = $builder2->get();

        $scheme = collect($diagnoses);

        $builder2 = DB::table('payment_modes')
            ->select(
                [
                    DB::raw("DISTINCT payment_modes.name"),
                ]
            )
            ->where('status', '=', '1');

        $diagnoses = $builder2->get();

        $payment = collect($diagnoses);

        $list = array();
        $keys = array();

        foreach ($scheme as $item) {
            $list[] = $item->name;
        }
        foreach ($payment as $item) {
            $list[] = $item->name;
        }

        $data['labels'] = $list;

        foreach ($list as $visit) {
            try {
                $data['datasets'][0]["data"][] = $diagnosis_count[$visit];
            } catch (Exception $e) {
                $data['datasets'][0]["data"][] = 0;
            }
        }

        return $data;
    }

    public function work_related_illness()
    {
        $start_date = request("start_date", '');
        $end_date = request("end_date", '');

        try {
            if (empty(trim($start_date))) {
                $start_date = Carbon::now()->subMonths(1);
            } else {
                $start_date = Carbon::parse($start_date);
            }
        } catch (Exception $e) {
            $start_date = Carbon::now()->subMonths(1);
        }

        $start_date = $start_date->format('Y-m-d') . " 00:00:00";

        try {
            if (empty(trim($end_date))) {
                $end_date = Carbon::now();
            } else {
                $end_date = Carbon::parse($end_date);
            }
        } catch (Exception $e) {
            $end_date = Carbon::now();
        }

        $end_date = $end_date->format('Y-m-d') . " 23:59:59";


        $scheme_diag = DB::table('payment_mode_visits')
            ->select([
                DB::raw("DISTINCT payment_mode_visits.uuid, payment_mode_visits.visit_id"),
                'schemes.name as payment_mode',
                'diagnoses.uuid as diagnoses_uuid',
                'payment_mode_visits.created_at'
            ])
            ->join('schemes', 'schemes.id', '=', 'payment_mode_visits.scheme_id')
            ->join('diagnoses', 'diagnoses.visit_id', '=', 'payment_mode_visits.visit_id')
            ->whereBetween('payment_mode_visits.created_at', [$start_date, $end_date])
            ->get();
        $re = DB::table('payment_mode_visits')
            ->select([
                DB::raw("DISTINCT payment_mode_visits.uuid, payment_mode_visits.visit_id"),
                'payment_modes.name as payment_mode',
                'diagnoses.uuid as diagnoses_uuid',
                'payment_mode_visits.created_at'
            ])
            ->join('payment_modes', 'payment_modes.id', '=', 'payment_mode_visits.payment_mode_id')
            ->join('diagnoses', 'diagnoses.visit_id', '=', 'payment_mode_visits.visit_id')
            ->whereBetween('payment_mode_visits.created_at', [$start_date, $end_date])
            ->get();

        foreach ($scheme_diag as $d) {
            $re[] = $d;
        }

        $disease_categories = DB::table('disease_categories')
            ->select(
                [
                    'disease_categories.name as name',
                    'disease_categories.id as id',
                ]
            )
            ->where('disease_categories.status', '=', '1')
            ->get();

        $re = $re->each(function ($item, $key) {
            $resultsPayment = DB::table('diagnoses')
                ->select(
                    [
                        DB::raw("DISTINCT diagnoses.uuid, diagnoses.created_at"),
                        'diagnoses.created_at AS diagnosis_date',
                        'diseases.name AS disease_name',
                        'diagnoses.disease_id AS disease_id',
                        'visits.number AS visit_number',
                        'visits.id AS visit_id',
                        'diagnoses.work_related AS is_work_related',
                        'clinics.name AS clinic_name',

                    ]
                )
                ->where('diagnoses.uuid', '=', $item->diagnoses_uuid)
                ->join('visits', 'visits.id', '=', 'diagnoses.visit_id')
                ->join('clinics', 'clinics.id', '=', 'visits.clinic_id')
                ->join('patients', 'patients.id', '=', 'visits.patient_id')
                ->join('diseases', 'diseases.id', '=', 'diagnoses.disease_id')
                ->get();

            $item->disease_name = $resultsPayment['0']->disease_name;
            $item->disease_id = $resultsPayment['0']->disease_id;
            $item->is_wr = $resultsPayment['0']->is_work_related;

            $disease_categories = DB::table('disease_disease_category')
                ->select(
                    [
                        'disease_categories.name as category',
                        'diseases.name as disease',
                    ]
                )
                ->where('disease_disease_category.disease_id', '=', $item->disease_id)
                ->join('disease_categories', 'disease_categories.id', '=', 'disease_disease_category.disease_category_id')
                ->join('diseases', 'diseases.id', '=', 'disease_disease_category.disease_id')
                ->get();
            try {
                $item->category = $disease_categories['0']->category;
            } catch (Exception $e) {
                $item->category = 'other';
            }
        });

        $di = $re->groupBy('payment_mode');
        $re = $re->groupBy('payment_mode');

        $builder2 = DB::table('schemes')
            ->select(
                [
                    DB::raw("DISTINCT schemes.name"),
                ]
            )
            ->where('schemes.status', '=', '1');

        $diagnoses = $builder2->get();

        $scheme = collect($diagnoses);

        $builder2 = DB::table('payment_modes')
            ->select(
                [
                    DB::raw("DISTINCT payment_modes.name"),
                ]
            )
            ->where('payment_modes.status', '=', '1');

        $diagnoses = $builder2->get();

        $payment = collect($diagnoses);

        $diagnosis_count2 = $di;

        foreach ($diagnosis_count2 as $val) {
            $a = 0;
            $b = 0;

            foreach ($val as $dat) {
                if ($dat->is_wr) {
                    $a = $a + 1;
                } else {
                    $b = $b + 1;
                }
            }
            $val[] = ['work_related' => $a, 'general_illness' => $b];
        }

        $list = array();
        $keys = array();

        foreach ($scheme as $item) {
            $list[] = $item->name;
        }
        foreach ($payment as $item) {
            $list[] = $item->name;
        }
        foreach ($re as $key => $value) {
            $keys[] = $key;
        }

        $cat = request('scheme', []);

        $pay = array();
        $keys = array();
        $inner = array();

        foreach ($scheme as $s)
            $pay[] = $s->name;
        foreach ($payment as $p)
            $pay[] = $p->name;
        foreach ($re as $key => $p)
            $keys[] = $key;

        if (!empty($cat) && is_array($cat)) {
            $inner[] = 'General Illness';
            foreach ($pay as $item) {
                foreach ($cat as $c) {
                    $a = 0;
                    if ($item == $c && in_array($c, $keys)) {
                        $a = json_decode(json_encode($diagnosis_count2[$item][count($diagnosis_count2[$item]) - 1]))->general_illness;
                    }
                    $inner[] = $a;
                }
            }
            $data = [$inner];
            $inner = array();
            $inner[] = 'Work related Illness';
            foreach ($pay as $item) {
                foreach ($cat as $c) {
                    $a = 0;
                    if ($item == $c && in_array($c, $keys)) {
                        $a = json_decode(json_encode($diagnosis_count2[$item][count($diagnosis_count2[$item]) - 1]))->work_related;
                    }
                    $inner[] = $a;
                }
            }
            $data[] = $inner;
            $inner = array();
            $inner[] = 'Totals';
            foreach ($pay as $item) {
                foreach ($cat as $c) {
                    $a = 0;
                    if ($item == $c && in_array($c, $keys)) {
                        $a = count($diagnosis_count2[$item]) - 1;
                    }
                    $inner[] = $a;
                }
            }
            $data[] = $inner;
        } else {
            $inner[] = 'General Illness';
            foreach ($pay as $item) {
                $a = 0;
                if (in_array($item, $keys)) {
                    $a = json_decode(json_encode($diagnosis_count2[$item][count($diagnosis_count2[$item]) - 1]))->general_illness;
                }
                $inner[] = $a;
            }

            $data = [$inner];
            $inner = array();
            $inner[] = 'Work related Illness';
            foreach ($pay as $item) {
                $a = 0;
                if (in_array($item, $keys)) {
                    $a = json_decode(json_encode($diagnosis_count2[$item][count($diagnosis_count2[$item]) - 1]))->work_related;
                }
                $inner[] = $a;
            }
            $data[] = $inner;
            $inner = array();
            $inner[] = 'Totals';
            foreach ($pay as $item) {
                $a = 0;
                if (in_array($item, $keys)) {
                    $a = count($diagnosis_count2[$item]) - 1;
                }
                $inner[] = $a;
            }
            $data[] = $inner;
        }

        return ['data' => $data];
    }

    public function ail_category()
    {
        $start_date = request("start_date", '');
        $end_date = request("end_date", '');

        try {
            if (empty(trim($start_date))) {
                $start_date = Carbon::now()->subMonths(1);
            } else {
                $start_date = Carbon::parse($start_date);
            }
        } catch (Exception $e) {
            $start_date = Carbon::now()->subMonths(1);
        }

        $start_date = $start_date->format('Y-m-d') . " 00:00:00";

        try {
            if (empty(trim($end_date))) {
                $end_date = Carbon::now();
            } else {
                $end_date = Carbon::parse($end_date);
            }
        } catch (Exception $e) {
            $end_date = Carbon::now();
        }

        $end_date = $end_date->format('Y-m-d') . " 23:59:59";


        $scheme_diag = DB::table('payment_mode_visits')
            ->select([
                DB::raw("DISTINCT payment_mode_visits.uuid, payment_mode_visits.visit_id"),
                'schemes.name as payment_mode',
                'diagnoses.uuid as diagnoses_uuid',
                'payment_mode_visits.created_at'
            ])
            ->join('schemes', 'schemes.id', '=', 'payment_mode_visits.scheme_id')
            ->join('diagnoses', 'diagnoses.visit_id', '=', 'payment_mode_visits.visit_id')
            ->whereBetween('payment_mode_visits.created_at', [$start_date, $end_date])
            ->get();
        $re = DB::table('payment_mode_visits')
            ->select([
                DB::raw("DISTINCT payment_mode_visits.uuid, payment_mode_visits.visit_id"),
                'payment_modes.name as payment_mode',
                'diagnoses.uuid as diagnoses_uuid',
                'payment_mode_visits.created_at'
            ])
            ->join('payment_modes', 'payment_modes.id', '=', 'payment_mode_visits.payment_mode_id')
            ->join('diagnoses', 'diagnoses.visit_id', '=', 'payment_mode_visits.visit_id')
            ->whereBetween('payment_mode_visits.created_at', [$start_date, $end_date])
            ->get();

        foreach ($scheme_diag as $d) {
            $re[] = $d;
        }

        $disease_categories = DB::table('disease_categories')
            ->select(
                [
                    'disease_categories.name as name',
                    'disease_categories.id as id',
                ]
            )
            ->where('disease_categories.status', '=', '1')
            ->get();

        $re = $re->each(function ($item, $key) {
            $resultsPayment = DB::table('diagnoses')
                ->select(
                    [
                        DB::raw("DISTINCT diagnoses.uuid, diagnoses.created_at"),
                        'diagnoses.created_at AS diagnosis_date',
                        'diseases.name AS disease_name',
                        'diagnoses.disease_id AS disease_id',
                        'visits.number AS visit_number',
                        'visits.id AS visit_id',
                        'diagnoses.work_related AS is_work_related',
                        'clinics.name AS clinic_name',

                    ]
                )
                ->where('diagnoses.uuid', '=', $item->diagnoses_uuid)
                ->join('visits', 'visits.id', '=', 'diagnoses.visit_id')
                ->join('clinics', 'clinics.id', '=', 'visits.clinic_id')
                ->join('patients', 'patients.id', '=', 'visits.patient_id')
                ->join('diseases', 'diseases.id', '=', 'diagnoses.disease_id')
                ->get();

            $item->disease_name = $resultsPayment['0']->disease_name;
            $item->disease_id = $resultsPayment['0']->disease_id;
            $item->is_wr = $resultsPayment['0']->is_work_related;

            $disease_categories = DB::table('disease_disease_category')
                ->select(
                    [
                        'disease_categories.name as category',
                        'diseases.name as disease',
                    ]
                )
                ->where('disease_disease_category.disease_id', '=', $item->disease_id)
                ->join('disease_categories', 'disease_categories.id', '=', 'disease_disease_category.disease_category_id')
                ->join('diseases', 'diseases.id', '=', 'disease_disease_category.disease_id')
                ->get();
            try {
                $item->category = $disease_categories['0']->category;
            } catch (Exception $e) {
                $item->category = 'other';
            }
        });

        $re = $re->groupBy('payment_mode');

        $builder2 = DB::table('schemes')
            ->select(
                [
                    DB::raw("DISTINCT schemes.name"),
                ]
            )
            ->where('schemes.status', '=', '1');

        $diagnoses = $builder2->get();

        $scheme = collect($diagnoses);

        $builder2 = DB::table('payment_modes')
            ->select(
                [
                    DB::raw("DISTINCT payment_modes.name"),
                ]
            )
            ->where('payment_modes.status', '=', '1');

        $diagnoses = $builder2->get();

        $payment = collect($diagnoses);

        $list = array();
        $keys = array();

        foreach ($scheme as $item) {
            $list[] = $item->name;
        }
        foreach ($payment as $item) {
            $list[] = $item->name;
        }
        foreach ($re as $key => $value) {
            $keys[] = $key;
        }

        $cat = request('scheme', []);

        $pay = array();
        $keys = array();
        $inner = array();

        foreach ($scheme as $s)
            $pay[] = $s->name;
        foreach ($payment as $p)
            $pay[] = $p->name;
        foreach ($re as $key => $p)
            $keys[] = $key;

        if (!empty($cat) && is_array($cat)) {
            foreach ($cat as $c) {
                $inner = array();
                $inner[] = $c;
                foreach ($pay as $d) {
                    $count = 0;
                    if (in_array($d, $keys)) {
                        foreach ($re[$d] as $item) {
                            if ($item->category == $c)
                                $count++;
                        }
                    }
                    $inner[] = $count;
                }
                $data[] = $inner;
            }

            $val[] = ['uresti' => $a, 'msklt' => $b, 'gi' => $c, 'derm' => $d, 'pud' => $e, 'uti' => $f, 'dental' => $g, 'ent' => $h, 'neur' => $i, 'obs' => $j, 'malaria' => $k];

            $val = json_encode($val);

            $val = json_encode($val);
        } else {
            foreach ($disease_categories as $category) {
                $inner = array();
                $inner[] = $category->name;
                foreach ($pay as $d) {
                    $count = 0;
                    if (in_array($d, $keys)) {
                        foreach ($re[$d] as $item) {
                            if ($item->category == $category->name)
                                $count++;
                        }
                    }
                    $inner[] = $count;
                }
                $data[] = $inner;
            }


            return view('analytics::visit-report', compact('clinic_count', 'diagnosis_count', 'scheme', 'payment'));

            $inner = array();
            $inner[] = "Other";
            foreach ($pay as $d) {
                $count = 0;
                if (in_array($d, $keys)) {
                    foreach ($re[$d] as $item) {
                        if ($item->category == 'other')
                            $count++;
                    }
                }
                $inner[] = $count;
            }
            $data[] = $inner;
        }

        return ['data' => $data];
    }

    public function wr_illness_rest()
    {
        $start_date = request("start_date", '');
        $end_date = request("end_date", '');

        try {
            if (empty(trim($start_date))) {
                $start_date = Carbon::now()->subMonths(1);
            } else {
                $start_date = Carbon::parse($start_date);
            }
        } catch (Exception $e) {
            $start_date = Carbon::now()->subMonths(1);
        }

        $start_date = $start_date->format('Y-m-d') . " 00:00:00";

        try {
            if (empty(trim($end_date))) {
                $end_date = Carbon::now();
            } else {
                $end_date = Carbon::parse($end_date);
            }
        } catch (Exception $e) {
            $end_date = Carbon::now();
        }

        $end_date = $end_date->format('Y-m-d') . " 23:59:59";


        $data = [
            "labels" => [],
            "datasets" => [
                [
                    'label' => 'Visits',
                    'backgroundColor' => '#7cb5ec',
                    'borderColor' => '#00c0ef',
                    'showLine' => true,
                    'steppedLine' => false,
                    'fill' => false,
                    'data' => []
                ]
            ],
        ];

        $scheme_diag = DB::table('payment_mode_visits')
            ->select([
                DB::raw("DISTINCT payment_mode_visits.uuid, payment_mode_visits.visit_id"),
                'schemes.name as payment_mode',
                'diagnoses.uuid as diagnoses_uuid',
                'payment_mode_visits.created_at'
            ])
            ->join('schemes', 'schemes.id', '=', 'payment_mode_visits.scheme_id')
            ->join('diagnoses', 'diagnoses.visit_id', '=', 'payment_mode_visits.visit_id')
            ->whereBetween('payment_mode_visits.created_at', [$start_date, $end_date])
            ->get();
        $re = DB::table('payment_mode_visits')
            ->select([
                DB::raw("DISTINCT payment_mode_visits.uuid, payment_mode_visits.visit_id"),
                'payment_modes.name as payment_mode',
                'diagnoses.uuid as diagnoses_uuid',
                'payment_mode_visits.created_at'
            ])
            ->join('payment_modes', 'payment_modes.id', '=', 'payment_mode_visits.payment_mode_id')
            ->join('diagnoses', 'diagnoses.visit_id', '=', 'payment_mode_visits.visit_id')
            ->whereBetween('payment_mode_visits.created_at', [$start_date, $end_date])
            ->get();
        foreach ($scheme_diag as $d) {
            $re[] = $d;
        }
        $re = $re->each(function ($item, $key) {
            $resultsPayment = DB::table('diagnoses')
                ->select(
                    [
                        DB::raw("DISTINCT diagnoses.uuid, diagnoses.created_at"),
                        'diagnoses.work_related AS is_work_related',
                    ]
                )
                ->where('diagnoses.uuid', '=', $item->diagnoses_uuid)
                ->get();
            if ($resultsPayment['0']->is_work_related)
                $item->is_wr = 'Work related Illness';
            else
                $item->is_wr = 'General Illness';
        });


        $diagnosis_count = $re->groupBy('is_wr')->map->count();
        $re = $re->groupBy('payment_mode');

        $builder2 = DB::table('schemes')
            ->select(
                [
                    DB::raw("DISTINCT schemes.name"),
                ]
            );

        $diagnoses = $builder2->get();

        $scheme = collect($diagnoses);

        $builder2 = DB::table('payment_modes')
            ->select(
                [
                    DB::raw("DISTINCT payment_modes.name"),
                ]
            );

        $diagnoses = $builder2->get();

        $payment = collect($diagnoses);

        $list = array();

        foreach ($scheme as $item) {
            $list[] = $item->name;
        }
        foreach ($payment as $item) {
            $list[] = $item->name;
        }

        $clinics = request('scheme', []);


        if (!empty($clinics) && is_array($clinics)) {
            foreach ($diagnosis_count as $key => $value) {
                $data['labels'][] = $key;
                $a = 0;
                foreach ($clinics as $s) {
                    try {
                        foreach ($re[$s] as $item)
                            if ($item->is_wr == $key)
                                $a = $a + 1;
                    } catch (Exception $e) {
                        $a = $a + 0;
                    }
                }
                $data['datasets'][0]["data"][] = $a;
            }
        } else {
            foreach ($diagnosis_count as $key => $value) {
                $data['labels'][] = $key;
                try {
                    $data['datasets'][0]["data"][] = $value;
                } catch (Exception $e) {
                    $data['datasets'][0]["data"][] = 0;
                }
            }
        }

        return $data;
    }

    public function patient_invoices(Request $request)
    {
        $start_date = request("start_date", '');
        $end_date = request("end_date", '');

        try {
            if (empty(trim($start_date))) {
                $start_date = Carbon::now()->subMonths(1);
            } else {
                $start_date = Carbon::parse($start_date);
            }
        } catch (Exception $e) {
            $start_date = Carbon::now()->subMonths(1);
        }

        $start_date = $start_date->format('Y-m-d') . " 00:00:00";

        try {
            if (empty(trim($end_date))) {
                $end_date = Carbon::now();
            } else {
                $end_date = Carbon::parse($end_date);
            }
        } catch (Exception $e) {
            $end_date = Carbon::now();
        }

        $end_date = $end_date->format('Y-m-d') . " 23:59:59";

        $builder1 = DB::table('invoice_items')
            ->select(
                'invoice_items.name as service_name',
                DB::raw('count(invoice_items.name) AS service_count'),
                DB::raw('DATE(invoices.date_closed) as date'),
                DB::raw('count(schemes.name) as scheme_count'),
                DB::raw('count(payment_modes.name) as mode_count'),
                DB::raw('SUM(invoice_items.amount_paid) as amount_paid')
            )
            ->join('invoices', 'invoices.id', 'invoice_items.invoice_id')
            ->join('visits', 'visits.id', 'invoices.visit_id')
            ->join('patients', 'patients.id', 'visits.patient_id')
            ->join('clinics', 'clinics.id', 'visits.clinic_id')
            ->join('payment_mode_visits', 'payment_mode_visits.visit_id', 'invoices.visit_id')
            ->leftJoin('payment_modes', 'payment_modes.id', 'payment_mode_visits.payment_mode_id')
            ->leftJoin('schemes', 'schemes.id', 'payment_mode_visits.scheme_id')
            ->whereBetween('invoices.date_closed', [$start_date, $end_date])
            ->where('invoices.date_closed', '!=',  null)
            ->groupBy('date', 'service_name')
            ->orderBy('date', 'asc');

        $payment_service_count = $builder1->get()->groupBy('date');

        $builder12 = DB::table('invoice_items')
            ->select(
                'payment_modes.name as payment_mode',
                DB::raw('DATE(invoices.date_closed) as date_mode'),
                DB::raw('count(payment_modes.name) as mode_count'),
                DB::raw('SUM(invoice_items.amount_paid) as amount_paid')
            )
            ->join('invoices', 'invoices.id', 'invoice_items.invoice_id')
            ->join('visits', 'visits.id', 'invoices.visit_id')
            ->join('patients', 'patients.id', 'visits.patient_id')
            ->join('clinics', 'clinics.id', 'visits.clinic_id')
            ->join('payment_mode_visits', 'payment_mode_visits.visit_id', 'invoices.visit_id')
            ->leftJoin('payment_modes', 'payment_modes.id', 'payment_mode_visits.payment_mode_id')
            ->whereBetween('invoices.date_closed', [$start_date, $end_date])
            ->where('payment_mode_visits.is_scheme', '=', false)
           // ->orHas('payment_modes.name')
            ->where('amount_paid', '>', 0)
            ->groupBy('date_mode', 'payment_mode')
            ->orderBy('date_mode', 'asc');

        $pay_mode_method = $builder12->groupBy('date_mode')->get();


        $builder13 = DB::table('invoice_items')
            ->select(
                'schemes.name as schemes_name',
                'invoice_items.name as service_name',
                DB::raw('payment_modes.name as payment_mode'),
                DB::raw('count(invoice_items.name) AS service_count'),
                DB::raw('DATE(invoices.date_closed) as date'),
                DB::raw('count(schemes.name) as scheme_count'),
                DB::raw('count(payment_modes.name) as mode_count'),
                DB::raw('SUM(invoice_items.amount_paid) as amount_paid')
            )
            ->join('invoices', 'invoices.id', 'invoice_items.invoice_id')
            ->join('visits', 'visits.id', 'invoices.visit_id')
            ->join('patients', 'patients.id', 'visits.patient_id')
            ->join('clinics', 'clinics.id', 'visits.clinic_id')
            ->join('payment_mode_visits', 'payment_mode_visits.visit_id', 'invoices.visit_id')
            ->leftJoin('payment_modes', 'payment_modes.id', 'payment_mode_visits.payment_mode_id')
            ->leftJoin('schemes', 'schemes.id', 'payment_mode_visits.scheme_id')
            ->whereBetween('invoices.date_closed', [$start_date, $end_date])
           // ->where('amount_paid', '>', 0)
            ->groupBy('date', 'schemes_name', 'payment_mode', 'service_name')
            ->orderBy('date', 'asc');

        $pay_scheme_method = $builder13->get()->groupBy('date');
      
        $builder2 = DB::table('schemes')
            ->select(
                [
                    DB::raw("DISTINCT schemes.name"),
                ]
            );

        $scheme = $builder2->get();

        $builder3 = DB::table('payment_modes')
            ->select(
                [
                    DB::raw("DISTINCT payment_modes.name"),
                ]
            );

        $payment = $builder3->get();

        $builder4 = DB::table('payment_items')
            ->select(
                [
                    DB::raw("DISTINCT payment_items.name"),
                ]
            );

        $billable = $builder4->get();

        $builder5 = DB::table('invoice_items')
            ->join('invoices', 'invoices.id', 'invoice_items.invoice_id')
            ->join('payment_modes', 'payment_modes.id', 'invoice_items.payment_mode_id')
            ->join('visits', 'visits.id', 'invoices.visit_id')
            ->leftJoin('payment_mode_visits', 'payment_mode_visits.id', 'payment_mode_visits.payment_mode_id')
            ->select('payment_modes.name as mode_name', 'payment_mode_visits.is_scheme as is_scheme', DB::raw('SUM(amount_paid) as total_sales'), DB::raw('COUNT(payment_modes.name) as mode_count_total'))
            ->whereBetween('invoices.date_closed', [$start_date, $end_date])
            ->where('invoices.date_closed', '!=',  null)
            ->where('payment_mode_visits.is_scheme', '=', false)
            ->groupBy('mode_name', 'is_scheme');

        $amount_total = $builder5->get()->groupBy('is_scheme');


        $builder6 = DB::table('invoice_items')
            ->join('invoices', 'invoices.id', 'invoice_items.invoice_id')
            ->join('schemes', 'schemes.id', 'invoice_items.scheme_id')
            ->join('visits', 'visits.id', 'invoices.visit_id')
            ->leftJoin('payment_mode_visits', 'payment_mode_visits.visit_id', 'payment_mode_visits.scheme_id', 'invoices.visit_id')
            ->select('schemes.name as scheme_names', 'payment_mode_visits.is_scheme as is_schemes', DB::raw('SUM(amount_paid) as total_scheme'), DB::raw('count(amount_paid) as sales_count'), DB::raw('COUNT(schemes.name) as scheme_count_total'))
            ->whereBetween('invoices.date_closed', [$start_date, $end_date])
            ->where('invoices.date_closed', '!=',  null)
            ->where('payment_mode_visits.is_scheme', '=', true)
            ->groupBy('scheme_names', 'is_schemes');

        $schemes_total = $builder6->get()->groupBy('is_schemes');


        $builder8 = DB::table('invoice_items')

            ->select(
                DB::raw('DATE(invoices.date_closed) as date_closed'),
                DB::raw('invoice_items.name AS service_name'),
                DB::raw('SUM(invoice_items.amount_paid) as amount_paid')
            )
            ->join('invoices', 'invoices.id', 'invoice_items.invoice_id')
            ->join('visits', 'visits.id', 'invoices.visit_id')
            ->join('patients', 'patients.id', 'visits.patient_id')
            ->join('clinics', 'clinics.id', 'visits.clinic_id')
            ->leftJoin('payment_mode_visits', 'payment_mode_visits.visit_id', 'invoices.visit_id')
            ->whereBetween('invoices.date_closed', [$start_date, $end_date])
            ->groupBy('invoices.date_closed', 'invoice_items.name');

        $billable_sum = $builder8->get();

        $builder9 = DB::table('invoice_items')
            ->join('invoices', 'invoices.id', 'invoice_items.invoice_id')
            ->join('payment_modes', 'payment_modes.id', 'invoice_items.payment_mode_id')
            ->join('visits', 'visits.id', 'invoices.visit_id')
            ->leftJoin('payment_mode_visits', 'payment_mode_visits.id', 'payment_mode_visits.payment_mode_id')
            ->select(DB::raw('DATE(invoices.date_closed) as mode_col_date'), DB::raw('SUM(amount_paid) as total_col_mode'), DB::raw('COUNT(payment_modes.name) as mode_count_total'))
            ->whereBetween('invoices.date_closed', [$start_date, $end_date])
            ->where('payment_mode_visits.is_scheme', '=', false)
            ->where('invoices.date_closed', '!=',  null)
            ->orderBy('mode_col_date', 'asc')
            ->groupBy('mode_col_date');

        $mode_col_total = $builder9->get()->groupBy('mode_col_date');

        $builder9 = DB::table('invoice_items')
            ->join('invoices', 'invoices.id', 'invoice_items.invoice_id')
            ->join('schemes', 'schemes.id', 'invoice_items.scheme_id')
            ->join('visits', 'visits.id', 'invoices.visit_id')
            ->leftJoin('payment_mode_visits', 'payment_mode_visits.id', 'payment_mode_visits.scheme_id')
            ->select(DB::raw('DATE(invoices.date_closed) as scheme_col_date'), DB::raw('SUM(amount_paid) as total_col_scheme'), DB::raw('COUNT(schemes.name) as scheme_count_total'))
            ->whereBetween('invoices.date_closed', [$start_date, $end_date])
            ->where('payment_mode_visits.is_scheme', '=', true)
            ->where('invoices.date_closed', '!=',  null)
            ->orderBy('scheme_col_date', 'asc')
            ->groupBy('scheme_col_date');

        $scheme_col_total = $builder9->get()->groupBy('scheme_col_date');

        $builder10 = DB::table('invoice_items')
            ->join('invoices', 'invoices.id', 'invoice_items.invoice_id')
            ->join('payment_items', 'payment_items.id', 'invoice_items.billable_item_id')
            ->join('visits', 'visits.id', 'invoices.visit_id')
            ->leftJoin('payment_mode_visits', 'payment_mode_visits.id', 'payment_mode_visits.scheme_id')
            ->select(DB::raw('DATE(invoices.date_closed) as date'), DB::raw('SUM(amount_paid) as total_col_bill'))
            ->whereBetween('invoices.date_closed', [$start_date, $end_date])
            ->orderBy('date', 'asc')
            ->groupBy('date');

        $billable_col_total = $builder10->get();

        $builder11 = DB::table('invoice_items')
            ->join('invoices', 'invoices.id', 'invoice_items.invoice_id')
            ->join('payment_items', 'payment_items.id', 'invoice_items.billable_item_id')
            ->join('visits', 'visits.id', 'invoices.visit_id')
            ->leftJoin('payment_mode_visits', 'payment_mode_visits.visit_id', 'payment_mode_visits.scheme_id', 'invoices.visit_id')
            ->select(
                'payment_items.name',
                DB::raw('SUM(amount_paid) as total_bill_paid'),
                DB::raw('count(amount_paid) as bill_count'),
                DB::raw('COUNT(payment_items.name) as bill_count_total')
            )
            ->whereBetween('invoices.date_closed', [$start_date, $end_date])
            ->groupBy('payment_items.name');

        $billable_total = $builder11->get();



        return view('analytics::invoice-report', compact('pay_mode_method', 'pay_scheme_method', 'billable_total', 'payment_service_count', 'scheme', 'payment', 'billable', 'amount_total', 'schemes_total', 'billable_sum', 'mode_col_total', 'scheme_col_total', 'billable_col_total'));
    }


    public function downloadExcel($type)
    {
        $data = Post::get()->toArray();
        return Excel::create('invoice-report', function ($excel) use ($data) {
            $excel->sheet('mySheet', function ($sheet) use ($data) {
                $sheet->fromArray($data);
            });
        })->download($type);
    }

    public function sales_corporate()
    {
        $builder = DB::table('invoice_items')
            ->select(
                [
                    DB::raw("DISTINCT invoice_items.uuid"),
                    'invoices.number AS invoice_number',
                    'invoices.date_closed',
                    'invoice_items.name AS service_name',
                    'invoice_items.total_amount',
                    'invoice_items.amount_paid',
                    'invoice_items.balance',

                ]
            )
            ->join('invoices', 'invoices.id', 'invoice_items.invoice_id')
            ->join('visits', 'visits.id', 'invoices.visit_id')
            ->join('patients', 'patients.id', 'visits.patient_id')
            ->join('clinics', 'clinics.id', 'visits.clinic_id')
            ->leftJoin('payment_mode_visits', 'payment_mode_visits.visit_id', 'invoices.visit_id')
            ->where('invoice_items.amount_paid', '>', 0)
            ->orderBy('invoices.date_closed', 'asc');

        $invoices = $builder->get();

        return view('analytics::sales-corporate', compact('invoices'));
    }

    public function clinic_index()
    {

        $pageTitle = "South Lake Analytics";

        return view('analytics::general-reports/clinic-report', compact("pageTitle"));
    }

    public function clinic_rest()
    {
        $start_date = request("start_date", '');
        $end_date = request("end_date", '');

        try {
            if (empty(trim($start_date))) {
                $start_date = Carbon::now()->subMonths(1)->startOfpayment_name();
            } else {
                $start_date = Carbon::parse($start_date)->startOfDay();
            }
        } catch (Exception $e) {
            $start_date = Carbon::now()->subMonths(1)->startOfDay();
        }

        $start_date = $start_date->format('Y-m-d') . " 00:00:00";

        try {
            if (empty(trim($end_date))) {
                $end_date = Carbon::now()->endOfDay();
            } else {
                $end_date = Carbon::parse($end_date)->endOfDay();
            }
        } catch (Exception $e) {
            $end_date = Carbon::now()->endOfDay();
        }

        $end_date = $end_date->format('Y-m-d') . " 23:59:59";

        $data = [
            "labels" => [],
            "datasets" => [
                [
                    'label' => 'Clincs',
                    'backgroundColor' => '#7cb5ec',
                    'borderColor' => '#00c0ef',
                    'data' => []
                ]
            ],
        ];

        $builder = Visit::select(
            DB::raw("count(*) AS clinic_count"),
            DB::raw("clinics.name as clinic_name")
        )->leftJoin('clinics', 'clinics.id', 'visits.clinic_id')
            ->groupBy('clinics.name')
            ->whereBetween('visits.created_at', [$start_date, $end_date]);

        $status = request('status', null);
        $search = request('search', '');
        $start = (int)request('start', '0');
        $length = (int)request('length', 100);
        $is_emergency = (int)request('is_emergency', 0);

        switch ($status) {
            case 'active':
                $builder->whereNull('ended_at');
                break;
            case 'ended':
                $builder->whereNotNull('ended_at');
                break;
            default:
                break;
        }

        if (!empty($search) && !empty($search['value'])) {

            $builder->whereHas('patient', function ($query) use ($search) {
                $query->search($search['value']);
            });
        }

        if ($is_emergency === 1) {

            $builder->where('is_emergency', $is_emergency);
        }

        if ($length != -1) {

            $builder->offset($start);

            $builder->limit($length);
        }

        $visits = $builder->get();

        $visits = $visits->map(function ($visit) {
            $visit->clinic_name;
            return $visit;
        })->sortBy(function ($obj, $key) {
            return $obj->clinic_name;
        });

        foreach ($visits as $visit) {
            $data['labels'][] = $visit->clinic_name;
            $data['datasets'][0]["data"][] = $visit->clinic_count;
        }

        return $data;
    }

    public function patient_index()
    {

        $pageTitle = "South Lake Patient List";
        $clinics = Clinic::active()->get();

        return view('analytics::raw-data/visit-list', compact(["pageTitle", "clinics"]));
    }

    public function patient_rest()
    {

        $search = request('search', null);
        $start = (int)request('start', '0');
        $length = (int)request('length', 100);
        $is_emergency = (int)request('is_emergency', 0);
        $startDate = request("start_date", null);
        $endDate = request("end_date", null);
        $clinics = request('clinics', []);

        try {
            if (empty(trim($startDate))) {

                $startDate = Carbon::today()->subMonths(1)->startOfDay();
            } else {

                $startDate = Carbon::parse($startDate)->startOfDay();
            }
        } catch (Exception $e) {

            $startDate = Carbon::today()->subMonths(1)->startOfDay();
        }

        try {
            if (empty(trim($endDate))) {

                $endDate = Carbon::today()->endOfDay();
            } else {

                $endDate = Carbon::parse($endDate)->endOfDay();
            }
        } catch (Exception $e) {

            $endDate = Carbon::today()->endOfDay();
        }

        $builder = Invoice::select([
            DB::raw("DISTINCT *")
        ])->whereBetween('created_at', [$startDate, $endDate])
            ->whereNotNull('date_closed');

        if (!empty($clinics) && is_array($clinics)) {

            $builder->whereHas('visit.clinic', function ($query) use ($clinics) {
                $query->select([
                    DB::raw("DISTINCT *"),
                    DB::raw("count(*) OVER() AS filtered")
                ]);
                $query->whereIn('uuid', $clinics);
            });
        }

        if ($is_emergency === 1) {
            $builder->whereHas('visit', function ($query) use ($is_emergency) {
                $query->where('is_emergency', $is_emergency);
            });
        }        

        if (!empty($search) && !empty($search['value'])) {

            $builder->whereHas('visit.patient', function ($query) use ($search) {
                $query->search($search['value']);
            });

        }
        
        $builder->orderBy('id', 'asc');

        if ($length != -1) {

            $builder->offset($start);

            $builder->limit($length);
        }

        $requests = $builder->get();

        $requests->load(['visit.patient', 'items']);

        $recordsFiltered = 0;

        $recordsTotal = Invoice::whereBetween('created_at', [$startDate, $endDate])->count();

        $requests = $requests->map(function ($request) use (&$recordsFiltered) {

            $recordsFiltered = $request['filtered'];

            $date = $request->date_closed->format('Y-m-d');

            $patientName = trim_space($request->visit->patient->first_name . ' ' . $request->visit->patient->last_name, " ");

            foreach ($request->items as $ev) {
                $service = $ev->name;
                $total_amount = $ev->total_amount;
                $amount_paid = $ev->amount_paid;
                $balance = $ev->balance;
            }

            return [
                'invoice_number' => $request->visit->number,
                'full_name' => $patientName,
                'clinic' => $request->visit->clinic === null ? '' : $request->visit->clinic->name,
                'is_emergency' => $request->visit->is_emergency,
                'invoice_number' => $request->visit->invoice->number,
                'service' => $service,
                'total_amount' => $total_amount,
                'amount_paid' => $amount_paid,
                'balance' => $balance,
                'date_closed' => $date

            ];
        });

        return [
            'draw' => request('draw', 0),
            'recordsTotal' => $recordsTotal,
            'recordsFiltered' => $recordsFiltered,
            'data' => $requests
        ];
    }

    public function patient_list_download()
    {
        $export = new PatientExport();

        return Excel::download($export, 'patients.xlsx');
    }

    public function moh_lab_report()
    { 
        $pageTitle = 'Laboratory Tests Data Summary Report';
        $clinics = Clinic::active()->orderBy('name', 'ASC')->get();

        return view('analytics::general-reports/lab-moh', compact('pageTitle', 'clinics'));
    }

    public function urine_rest()
    {
        $start = (int)request('start', '0');
        $length = (int)request('length', 100);
        $is_emergency = (int)request('is_emergency', 0);
        $startDate = request("start_date", null);
        $endDate = request("end_date", null);
        $clinics = request('clinics', []);

        try {
            if (empty(trim($startDate))) {

                $startDate = Carbon::today()->subMonths(1)->startOfDay();

            } else {

                $startDate = Carbon::parse($startDate)->startOfDay();

            }
        } catch (Exception $e) {

            $startDate = Carbon::today()->subMonths(1)->startOfDay();

        }

        try {
            if (empty(trim($endDate))) {

                $endDate = Carbon::today()->endOfDay();

            } else {

                $endDate = Carbon::parse($endDate)->endOfDay();

            }

        } catch (Exception $e) {

            $endDate = Carbon::today()->endOfDay();

        }

        $builder = LabTestResult::select([

            DB::raw("test AS blood_test"),
            DB::raw("result AS blood_result"), 
            DB::raw("COUNT(case when result = 'Positive' then 0 END) as blood_result_positive"),
            DB::raw("COUNT(case when result = 'Negative' then 0 END) as blood_result_negative"),
            DB::raw("COUNT(case when result = 'high' then 0 END) as blood_result_high"),
            DB::raw("COUNT(case when result = 'low' then 0 END) as blood_result_low"),
            DB::raw("COUNT(result) AS blood_result_count")
        ])
       
        ->where(static function ($query) {
            $query
                ->whereNotNull('test');
        })
        ->groupBy(DB::raw("test, result"))
        ->whereBetween('created_at', [$startDate, $endDate]);

        if (!empty($clinics) && is_array($clinics)) {

            $builder->whereHas('visit.clinic', function ($query) use ($clinics) {
                $query->select([
                    DB::raw("DISTINCT *"),
                    DB::raw("count(*) OVER() AS filtered")
                ]);
                $query->whereIn('uuid', $clinics);
            });

        }
       
        $requests = $builder->get()->groupBy('blood_test');

        $num = $requests->map(function ($row) {

            $result = array();

            $row = $row->toArray();

            $final_array = [];

            foreach($row as $arr){
            $final_array[$arr['blood_test']]['blood_test'] = $arr['blood_test'];
            $final_array[$arr['blood_test']]['blood_result'] = $arr['blood_result'];
            $final_array[$arr['blood_test']]['blood_result_positive'] = $arr['blood_result_positive'];
            $final_array[$arr['blood_test']]['blood_result_negative'] = $arr['blood_result_negative'];
            $final_array[$arr['blood_test']]['blood_result_high'] = $arr['blood_result_high'];
            $final_array[$arr['blood_test']]['blood_result_low'] = $arr['blood_result_low'];
            $final_array[$arr['blood_test']]['blood_result_count'] = (isset($final_array[$arr['blood_test']]['blood_result_count']))? $final_array[$arr['blood_test']]['blood_result_count']+$arr['blood_result_count'] : $arr['blood_result_count'];    
            }
            $final_array = array_values($final_array);

            return [$final_array];

        });

        return $num;
    }

}