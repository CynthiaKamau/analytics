<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::domain('{subdomain}.' . env('APP_DOMAIN', 'localhost.loc'))->prefix('analytics')
    ->group(function () {
        Route::get('/', 'AnalyticsController@index');


        Route::get('/dashboard/visits-rest', 'AnalyticsController@visits_rest')->name('analytics.dashboard.visits-rest');
        Route::get('/dashboard/payment-modes-rest', 'AnalyticsController@payment_modes_rest')->name('analytics.dashboard.payment-modes-rest');
        Route::get('/dashboard/sales-rest', 'AnalyticsController@sales_rest')->name('analytics.dashboard.sales-rest');

        Route::get('/sales-reports', 'ReportsController@sales_reports')->name('analytics.reports.sales');
        Route::get('/sales-rest', 'ReportsController@sales_rest')->name('analytics.reports.sales_rest');
        Route::get('/sales-dates', 'ReportsController@sales_dates')->name('analytics.reports.sales_dates');
        Route::post('/sales-date-rest', 'ReportsController@sales_date_rest')->name('analytics.reports.sales_date_rest');
        Route::get('downloadExcel/{type}', 'ReportsController@downloadExcel');//Not tested


        Route::get('/sales-invoices', 'ReportsController@patient_invoices')->name('analytics.reports.patient_invoices');
        Route::get('/sales-corporate', 'ReportsController@corporate_sale')->name('analytics.reports.corporate_sale');
        Route::post('/corporate-rest', 'ReportsController@corporate_rest')->name('analytics.reports.corporate-rest');
        Route::get('/cash-sale', 'ReportsController@cash_sale')->name('analytics.reports.cash_sale');
        Route::post('/cash-rest', 'ReportsController@cash_rest')->name('analytics.reports.cash_rest');


        Route::get('/visits-report', 'ReportsController@index')->name('analytics.reports.index');//Breaking

        Route::get('/visits-scheme-rest', 'ReportsController@visit_scheme_rest')->name('analytics.reports.visits_report');
        Route::get('/work-related-illness-rest', 'ReportsController@work_related_illness')->name('analytics.reports.work-related-illness');
        Route::get('/ail-category-rest', 'ReportsController@ail_category')->name('analytics.reports.ail-category-rest');
        Route::get('/wr-illness-rest', 'ReportsController@wr_illness_rest')->name('analytics.reports.work-related-illness-rest');
        Route::get('/all-visits-rest', 'ReportsController@all_visit_rest')->name('analytics.reports.all-visits-rest');

        Route::get('/patient-list', 'ReportsController@patient_index')->name('analytics.reports.list');
        Route::post('/patient-rest', 'ReportsController@patient_rest')->name('analytics.reports.patient.rest');
        Route::get('/patient-list-download', 'ReportsController@patient_list_download')->name('analytics.payments.download');
        Route::get('/clinic-rest', 'ReportsController@clinic_rest')->name('analytics.reports.clinic_rest');
        Route::get('/clinic-report', 'ReportsController@clinic_index')->name('analytics.reports.clinic_report');

        Route::get('/lab-report', 'LabReportsController@lab_report')->name('analytics.reports.lab_report');
        Route::get('/lab-rest', 'LabReportsController@lab_rest')->name('analytics.reports.lab_rest');
        Route::get('/lab-test-download', 'LabReportsController@labtest_list_download')->name('analytics.lab_tests_export');
        Route::get('/lab-payments-rest', 'LabReportsController@lab_payments_rest')->name('analytics.reports.lab_payments_rest');
        Route::get('/lab-test-rest', 'LabReportsController@lab_test_rest')->name('analytics.reports.lab_test_rest');
        Route::get('/lab-revenue-rest', 'LabReportsController@lab_revenue_rest')->name('analytics.reports.lab_revenue_rest');

        Route::get('/moh-lab-report', 'ReportsController@moh_lab_report')->name('analytics.reports.moh_lab_report');
        Route::post('/urine-rest', 'ReportsController@urine_rest')->name('analytics.reports.urine-rest');

        Route::get('/moh-705b', 'AnalyticsController@moh_705b')->name('analytics.reports.moh_705b');


    });
