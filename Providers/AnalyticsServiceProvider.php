<?php

namespace Modules\Analytics\Providers;

use Illuminate\Support\Facades\Config;
use Illuminate\Support\ServiceProvider;
use Illuminate\Database\Eloquent\Factory;

class AnalyticsServiceProvider extends ServiceProvider
{
    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = false;

    /**
     * Boot the application events.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerTranslations();
        $this->registerConfig();
        $this->registerViews();
        $this->registerFactories();
        $this->loadMigrationsFrom(__DIR__ . '/../Database/Migrations');
        $this->overrideMenu();
    }

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->app->register(RouteServiceProvider::class);
    }

    /**
     * Register config.
     *
     * @return void
     */
    protected function registerConfig()
    {
        $this->publishes([
            __DIR__.'/../Config/config.php' => config_path('analytics.php'),
        ], 'config');
        $this->mergeConfigFrom(
            __DIR__.'/../Config/config.php', 'analytics'
        );
    }

    /**
     * Register views.
     *
     * @return void
     */
    public function registerViews()
    {
        $viewPath = resource_path('views/modules/analytics');

        $sourcePath = __DIR__.'/../Resources/views';

        $this->publishes([
            $sourcePath => $viewPath
        ],'views');

        $this->loadViewsFrom(array_merge(array_map(function ($path) {
            return $path . '/modules/analytics';
        }, \Config::get('view.paths')), [$sourcePath]), 'analytics');
    }

    /**
     * Register translations.
     *
     * @return void
     */
    public function registerTranslations()
    {
        $langPath = resource_path('lang/modules/analytics');

        if (is_dir($langPath)) {
            $this->loadTranslationsFrom($langPath, 'analytics');
        } else {
            $this->loadTranslationsFrom(__DIR__ .'/../Resources/lang', 'analytics');
        }
    }

    /**
     * Register an additional directory of factories.
     * 
     * @return void
     */
    public function registerFactories()
    {
        if (! app()->environment('production')) {
            app(Factory::class)->load(__DIR__ . '/../Database/factories');
        }
    }

    private function overrideMenu()
    {
        $menu = [
            [
                'order' => 6,
                'type' => 'dropdown',
                'title' => 'Analytics',
                'icon' => 'fa fa-bar-chart',
                'sub_links' => [
                    [
                        'url' => '/analytics/',
                        'title' => 'Reports',
                        'type' => 'link',
                    ],
                    [
                        'url' => '/analytics/visits-report',
                        'title' => 'Visit Reports',
                        'type' => 'link',
                    ],
                    [
                        'url' => '/analytics/lab-report',
                        'title' => 'Lab Reports',
                        'type' => 'link',
                    ],
                    [
                        'url' => '/analytics/sales-reports',
                        'title' => 'Sales Report',
                        'type' => 'link',
                    ],
                    [
                        'url' => '/analytics/clinic-report',
                        'title' => 'Clinic Report',
                        'type' => 'link',
                    ],
                    [
                        'url' => '/analytics/sales-dates',
                        'title' => 'Invoice Lists',
                        'type' => 'link',
                    ],
                ]
            ]
        ];

        override_menu('analytics', $menu);
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return [];
    }
}
