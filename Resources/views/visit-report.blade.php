@extends('partials.facility.app')
@push('styles')
    <link rel="stylesheet" type="text/css" href="{{asset('libs/bootstrap-select/dist/css/bootstrap-select.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset("libs/bootstrap-daterangepicker/daterangepicker.css")}}"/>
    <style>
        canvas {
            -moz-user-select: none;
            -webkit-user-select: none;
            -ms-user-select: none;
        }
    </style>
@endpush
@push("content")

    <section class="content-header">
        <h1>
            Visit Report
        </h1>
        <ol class="breadcrumb">
            <li>
                <a href="{{route('facility.dashboard')}}">
                    <i class="fa fa-dashboard"></i> Dashboard</a>
            </li>
            <li>
                <a href="{{route("facility.visits.list")}}">South Lake Analytics</a>
            </li>
        </ol>
        <div>Visualization of various facility data</div>
    </section>

    @medyqPermission("can-view-visit-report")
    <section class="content">
        <?php

        $pay = array();

        foreach ($scheme as $s)
            $pay[] = $s->name;
        foreach ($payment as $p)
            $pay[] = $p->name;
        ?>
        <div class="row">
            <div class="col-md-12">
                <div class="box box-success">
                    <div class="box-header with-border">
                        <div class="row">
                            <div class="col-md-7">
                                <label>Select date range</label>
                                <input type="text" name="daterange" id="daterange"
                                       class="form-control" autocomplete="off" autocapitalize="off"/>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                <div class="box box-widget">
                    <div class="box-header with-border">
                        <div class="title-block">
                            <span class="title">
                                Visits per ailment classification and payment modes
                            </span>
                            <span class="description">

                            </span>
                        </div>
                    </div>
                    <div class="box-body">
                        <div class="col-md-8">
                            <label for="scheme_filter">Select Ailment category</label>
                            <select name="scheme_filter" id="scheme_filter" class="form-control selectpicker"
                                    multiple="multiple">
                                @foreach($disease_categories as $p)
                                    <option value="{{$p->name}}">{{$p->name}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="col-md-6">
                        </div>
                        <div class="row">
                            <div id="chart-container" class="col-md-12">

                                <canvas id="visitChart"></canvas>
                            </div>
                        </div>
                        <button class="btn" onclick="saveAsPDF();">Export Chart as PDF</button>
                        <button id="btnExport" onclick="exportReportToExcel(this)" class="btn">Export Excel Report
                        </button>
                    </div>
                </div>
            </div>

            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                <div class="box box-widget">
                    <div class="box-header with-border">
                        <div class="title-block">
                            <span class="title">
                                Visits vs Illness type
                            </span>
                            <span class="description">
                                The number of visits per illness type
                            </span>
                        </div>
                    </div>
                    <div class="box-body">
                        <div class="col-md-8">
                            <label for="wr_filter">Select Scheme</label>
                            <select name="wr_filter" id="wr_filter" class="form-control selectpicker"
                                    multiple="multiple">
                                @foreach($pay as $p)
                                    <option value="{{$p}}">{{$p}}</option>
                                @endforeach

                            </select>
                        </div>
                        <div class="row">
                            <div id="chartwrcontainer" class="col-md-12">

                                <canvas id="wrChart"></canvas>
                            </div>
                        </div>
                        <button class="btn" onclick="saveAsPDF();">Export Chart as PDF</button>
                        <button id="btnExport" onclick="exportReport2ToExcel(this)" class="btn">Export Excel Report
                        </button>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <div class="box box-widget">
                    <div class="box-header with-border">
                        <div class="title-block">
                            <span class="title">
                                All Visits vs payment modes
                            </span>
                            <span class="description">
                                All Visits vs payment modes for facility
                            </span>
                        </div>
                    </div>

                    <div class="box-body">
                        <div class="col-md-5">
                            <label for="scheme_filter">Select Clinic</label>
                            <select name="scheme_filter" id="clinic_filter" class="form-control selectpicker"
                                    multiple="multiple">
                                @foreach($clinics as $p)
                                    <option value="{{$p->id}}">{{$p->name}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div id="allvisitcontainer">
                            <canvas id="allvisitChart"></canvas>
                        </div>
                    </div>
                </div>
            </div>
        </div>


        <table id="scheme-diagnosis-table" class="table table-striped" border="1" style="display: none">
            <thead>
            <tr>
                <th rowspan="2">Ailment classification</th>
                <?php echo '<th colspan="' . count($scheme) . '">Schemes</th>'; ?>
                <?php echo '<th colspan="' . count($payment) . '">Payment Modes</th>'; ?>
            </tr>
            <tr>

                <?php
                foreach ($pay as $a) {
                    echo "<th> $a</th>";
                }
                ?>
            </tr>
            </thead>

        </table>

        <table id="wrtable" class="display table table-striped" border="1" style="display: none">
            <thead style="font-weight: bold">
            <tr>
                <td rowspan="2">Ailment classification</td>
                <?php echo '<td colspan="' . count($scheme) . '">Schemes</td>'; ?>

                <?php echo '<td colspan="' . count($payment) . '">Payment Mode</td>'; ?>
            </tr>
            <tr>

                <?php
                foreach ($pay as $a)
                    echo "<td> $a</td>";

                ?>
            </tr>
            </thead>
        </table>

    </section>
    @endmedyqPermission

@endpush

@push("footer_scripts")

    @include('partials.global.table-scripts')
    <script type="text/javascript"
            src="{{asset('libs/bootstrap-select/dist/js/bootstrap-select.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('libs/moment/min/moment.min.js')}}"></script>
    <script type="text/javascript" src="{{asset("libs/bootstrap-daterangepicker/daterangepicker.js")}}"></script>
    <script type="text/javascript" src="{{asset('libs/chartjs/Chart.min.js')}}"></script>
    <script src="https://cdn.jsdelivr.net/gh/linways/table-to-excel@v1.0.4/dist/tableToExcel.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jspdf/1.3.5/jspdf.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/html2canvas/0.4.1/html2canvas.min.js"></script>

    @include('analytics::charts.visit-scheme-count')
@endpush
