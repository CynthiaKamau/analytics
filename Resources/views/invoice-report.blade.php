@extends('partials.facility.app')
@push('styles')
<link rel="stylesheet" type="text/css" href="{{asset('libs/bootstrap-select/dist/css/bootstrap-select.min.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('libs/bootstrap-daterangepicker/daterangepicker.css')}}" />

<style>
canvas {
    -moz-user-select: none;
    -webkit-user-select: none;
    -ms-user-select: none;
}
</style>

@endpush
@push("content")

<section class="content-header">
    <h1>
        Sales Reports
    </h1>
    <ol class="breadcrumb">
        <li>
            <a href="{{route('facility.dashboard')}}">
                <i class="fa fa-dashboard"></i> Dashboard</a>
        </li>
        <li>
            <a href="{{route("facility.visits.list")}}">Patient Invoices</a>
        </li>
    </ol>
    <div>Sales Reports</div>
</section>

@medyqPermission("can-view-sales-report")
<section class="content">

    <div class="box box-success">
        <div class="box box-success">
            <div class="box-header with-border">
                <div class="row">
                    <div class="col-md-3">
                        <label>Select date range</label>
                        <input type="text" name="daterange" id="daterange" class="form-control" autocomplete="off"
                            autocapitalize="off" />
                    </div>
                    <div class="col-md-6">

                    </div>
                    <div class="col-md-2">
                        <button id="btnExport" onclick="exportTableToExcel('salesTable')" class="btn btn-success">Export
                            Report </button>
                    </div>

                </div>
            </div>

            <div class="data-table-wrapper">
                <?php

                $myData = file_get_contents("http://ergast.com/api/f1/current.json");
                $myObject = json_decode($myData);
                $myObjectMap = $myObject->MRData->RaceTable->Races;
                $service_invoice = json_decode($payment_service_count);
                $bill_col = json_decode($billable_col_total);
                $bill_totals = json_decode($billable_total);
                $pay_mode = json_decode($payment);

                ?>
                <table id="salesTable" class="table table-striped " border="1" style="text-align: center">
                    <thead>
                        <tr>
                            <th>Sales</th>
                            <th>Month</th>

                            @foreach ($pay_scheme_method as $date=>$header)
                            <?php
                            $dates = \Carbon\Carbon::parse($date)->format('j-F-y');
                            ?>
                            <td> {{ $dates }} </td>
                            @endforeach
                            <th>Total</th>
                    </thead>
                    <tbody>
                        <tr>
                            <th></th>
                            <th>Payment Modes</th>
                        </tr>

                        @foreach ($pay_mode as $modes)

                        <tr>
                            <td></td>
                            <td>{{ $modes->name }}</td>

                            @foreach ($pay_scheme_method as $date=>$header)
                            @foreach ($header as $mode_headers)
                            <?php
                            $date_modes = \Carbon\Carbon::parse($date)->format('j-F-y');
                            ?>
                            @if ($mode_headers->payment_mode == $modes->name)
                            @if (isset($mode_headers->date))
                            <td>{{ $mode_headers->amount_paid }}</td>
                            @elseif ($mode_headers->payment_mode !== $modes->name))
                            <td>{{0}}</td>
                            @endif
                            @endif
                            @endforeach
                            @endforeach

                            @foreach ($amount_total as $is_scheme=>$pay_mode_scheme)
                            @foreach ($pay_mode_scheme as $pay_mode_schemes)
                            @if ($pay_mode_schemes->mode_name == $modes->name)
                            @if ($pay_mode_schemes->is_scheme == false)
                            <td>{{ $pay_mode_schemes->total_sales }}</td>
                            @else
                            <td>{{0}}</td>
                            @endif
                            @endif
                            @endforeach
                            @endforeach
                        </tr>
                        @endforeach

                        <tr>
                            <td></td>
                            <th>SubTotals</th>


                            @foreach ($mode_col_total as $mode_col_date=>$cols)
                            @foreach ($cols as $cols_mode_total)
                            @if (isset($cols_mode_total->mode_col_date))

                            <td>{{ $cols_mode_total->total_col_mode }}</td>
                            @else
                            <td>{{0}}</td>
                            @endif
                            @endforeach
                            @endforeach


                        </tr>
                        <tr>
                            <td></td>
                            <th>Payment Schemes</th>
                        </tr>

                        @foreach ($scheme as $schemes)
                        <tr>
                            <td></td>
                            <td>{{ $schemes->name }}</td>
                            @foreach ($pay_scheme_method as $date=>$header)
                            @foreach ($header as $scheme_headers)
                            @if ($scheme_headers->schemes_name == $schemes->name)
                            @if (isset($scheme_headers->date))
                            <td>{{ $scheme_headers->amount_paid }}</td>
                            @elseif (!isset($scheme_headers->date))
                            <td>{{0}}</td>
                            @endif
                            @endif
                            @endforeach
                            @endforeach

                            @foreach ($schemes_total as $is_schemes=>$pay_mode_scheme)
                            @foreach ($pay_mode_scheme as $pay_mode_schemes)
                            @if ($pay_mode_schemes->scheme_names == $schemes->name)
                            @if ($pay_mode_schemes->is_schemes == true)
                            <td>{{ $pay_mode_schemes->total_scheme }}</td>
                            @else
                            <td>{{0}}</td>
                            @endif
                            @endif
                            @endforeach
                            @endforeach

                        </tr>
                        @endforeach

                        <tr>
                            <td></td>
                            <th>Totals</th>
                            @foreach ($scheme_col_total as $scheme_col_date=>$cols)
                            <?php
                            $date_schemes = \Carbon\Carbon::parse($scheme_col_date)->format('j-F-y')
                            ?>
                            @foreach ($cols as $cols_scheme_total)
                            @if ($date_schemes == $dates)
                            <td>{{ $cols_scheme_total->total_col_scheme }}</td>
                            @else
                            <td>{{0}}</td>
                            @endif
                            @endforeach
                            @endforeach

                        </tr>
                        <tr>
                            <td></td>
                            <th>Income Break down per category</th>
                        </tr>
                        <tr>
                            <td></td>
                            <th>Description</th>
                        </tr>
                        @foreach ($billable as $bill)
                        <tr>
                            <td></td>
                            <td>{{ $bill->name }}</td>
                            @foreach ($payment_service_count as $date=>$pay_service_count)
                            @foreach ($pay_service_count as $pay_service_counts)
                            @if ($pay_service_counts->service_name == $bill->name)
                            @if (isset($dates))
                            <td>{{ $pay_service_counts->amount_paid }}</td>
                            @else
                            <td>{{0}}</td>
                            @endif
                            @endif
                            @endforeach
                            @endforeach


                            @foreach ($bill_totals as $bill_sales)
                            @if ($bill_sales->name == $bill->name)
                            <td>{{ $bill_sales->total_bill_paid }}</td>
                            @elseif ($bill_sales->name !== $bill->name)
                            <td>{{0}}</td>
                            @endif
                            @endforeach

                        </tr>
                        @endforeach

                        <tr>
                            <td></td>
                            <th>Totals</th>

                            @foreach ($bill_col as $cols)
                            <?php
                            $date_bill = \Carbon\Carbon::parse($cols->date)->format('j-F-y')
                            ?>
                            @if (isset($date_bill))
                            <td>{{ $cols->total_col_bill }}</td>
                            @elseif (!isset($date_bill))
                            <td>{{0}}</td>
                            @endif
                            @endforeach

                        </tr>
                        <tr>
                            <td></td>
                            <th>Patient Statistic</th>
                        </tr>

                        @foreach ($pay_mode as $modes)
                        <tr>
                            <td></td>
                            <td>{{ $modes->name }}</td>
                            @foreach ($pay_scheme_method as $date=>$header)
                            @foreach ($header as $mode_headers)
                            @if ($mode_headers->payment_mode == $modes->name)
                            @if (isset($dates))
                            <td>{{ $mode_headers->mode_count }}</td>
                            @else
                            <td>0</td>
                            @endif
                            @endif
                            @endforeach
                            @endforeach

                            @foreach ($amount_total as $is_scheme=>$pay_mode_scheme)
                            @foreach ($pay_mode_scheme as $pay_mode_schemes)
                            @if ($pay_mode_schemes->mode_name == $modes->name)
                            @if ($pay_mode_schemes->is_scheme == false)
                            <td>{{ $pay_mode_schemes->mode_count_total }}</td>
                            @else
                            <td>{{0}}</td>
                            @endif
                            @endif
                            @endforeach
                            @endforeach
                        </tr>
                        @endforeach
                        <tr>
                            <td></td>
                            <th>Subtotal</th>
                            @foreach ($mode_col_total as $mode_col_date=>$cols)
                            @foreach ($cols as $cols_mode_total)
                            @if (isset($dates))
                            <td>{{ $cols_mode_total->mode_count_total }}</td>
                            @else
                            <td>{{0}}</td>
                            @endif
                            @endforeach
                            @endforeach
                        </tr>

                        @foreach ($scheme as $schemes)
                        <tr>
                            <td></td>
                            <td>{{ $schemes->name }}</td>
                            @foreach ($pay_scheme_method as $date=>$header)
                            @foreach ($header as $scheme_headers)
                            @if ($scheme_headers->schemes_name == $schemes->name)
                            @if (isset($scheme_headers->date))
                            <td>{{ $scheme_headers->scheme_count }}</td>
                            @else
                            <td>{{0}}</td>
                            @endif
                            @endif
                            @endforeach
                            @endforeach

                            @foreach ($schemes_total as $is_schemes=>$pay_mode_scheme)
                            @foreach ($pay_mode_scheme as $pay_mode_schemes)
                            @if ($pay_mode_schemes->scheme_names == $schemes->name)
                            @if ($pay_mode_schemes->is_schemes == true)
                            <td>{{ $pay_mode_schemes->scheme_count_total }}</td>
                            @else
                            <td>{{0}}</td>
                            @endif
                            @endif
                            @endforeach
                            @endforeach
                        </tr>
                        @endforeach
                        <tr>
                            <td></td>
                            <th>Total</th>
                            @foreach ($scheme_col_total as $scheme_col_date=>$cols)
                            @foreach ($cols as $cols_scheme_total)
                            @if (isset($dates))
                            <td>{{ $cols_scheme_total->scheme_count_total }}</td>
                            @else
                            <td>{{0}}</td>
                            @endif
                            @endforeach
                            @endforeach
                        </tr>
                    <tbody>
                </table>
            </div>
        </div>
    </div>

</section>
@endmedyqPermission


@endpush

@push("footer_scripts")

<script type="text/javascript" src="{{asset('libs/bootstrap-select/dist/js/bootstrap-select.min.js')}}"></script>

<script type="text/javascript" src="{{asset('libs/moment/min/moment.min.js')}}"></script>
<script type="text/javascript" src="{{asset('libs/bootstrap-daterangepicker/daterangepicker.js')}}"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/gh/linways/table-to-excel@v1.0.4/dist/tableToExcel.js">
</script>

<script type="text/javascript">
function initTable() {

    $.ajax({
        type: "GET",
        url: "{{route('analyticsanalytics.reports.patient_invoices')}}",
        data: {
          //  clinics: $("#clinics").val(),
            start_date: startDate,
            end_date: endDate,
        },
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        cache: false,
        success: function(data) {

            var trHTML = '';

            $.each(data, function(i, item) {

                $.each(item, function(z, ev) {

                    trHTML += '<tr><td></td> <td></td> <td>' + @foreach ($pay_scheme_method as $date=>$header)
                            <?php
                            $dates = \Carbon\Carbon::parse($date)->format('j-F-y');
                            ?>
                            '<td>' + ev.dates + '</td>'
                            @endforeach +
                        '</td> <td></td></tr>';
                    trHTML += '<tr><td></td> <td></td> <td>' + +'</td> <td></td></tr>';
                    trHTML += '<tr><td></td> <td></td> <td>' + +'</td> <td></td></tr>';
                    trHTML += '<tr><td></td> <td></td> <td>' + +'</td> <td></td></tr>';
                    trHTML += '<tr><td></td> <td></td> <td>' + +'</td> <td></td></tr>';
                    trHTML += '<tr><td></td> <td></td> <td>' + +'</td> <td></td></tr>';
                    trHTML += '<tr><td></td> <td></td> <td>' + +'</td> <td></td></tr>';
                    trHTML += '<tr><td></td> <td></td> <td>' + +'</td> <td></td></tr>';
                    trHTML += '<tr><td></td> <td></td> <td>' + +'</td> <td></td></tr>';
                    trHTML += '<tr><td></td> <td></td> <td>' + +'</td> <td></td></tr>';

                })

            });

            $('#salesTable').append(trHTML);

            MergeGridCells();

        },

        error: function(msg) {

           // alert(msg.responseText);
        }
    });

}

function MergeGridCells() {
    var dimension_cells = new Array();
    var dimension_col = null;
    var columnCount = $("#salesTable tr:first th").length;
    for (dimension_col = 0; dimension_col < columnCount; dimension_col++) {
        var first_instance = null;
        var rowspan = 1;
        $("#salesTable").find('tr').each(function() {

            var dimension_td = $(this).find('td:nth-child(' + dimension_col + ')');

            if (first_instance == null) {
                first_instance = dimension_td;
            } else if (dimension_td.text() == first_instance.text()) {

                dimension_td.remove();
                ++rowspan;
                first_instance.attr('rowspan', rowspan);
            } else {
                first_instance = dimension_td;
                rowspan = 1;
            }
        });
    }
}

function exportTableToExcel(tableID, filename = '') {
    var downloadLink;
    var dataType = 'application/vnd.ms-excel';
    var tableSelect = document.getElementById(tableID);
    var tableHTML = tableSelect.outerHTML.replace(/ /g, '%20');

    // Specify file name
    filename = filename ? filename + '.xls' : 'sales_summary.xls';

    // Create download link element
    downloadLink = document.createElement("a");

    document.body.appendChild(downloadLink);

    if (navigator.msSaveOrOpenBlob) {
        var blob = new Blob(['\ufeff', tableHTML], {
            type: dataType
        });
        navigator.msSaveOrOpenBlob(blob, filename);
    } else {
        // Create a link to the file
        downloadLink.href = 'data:' + dataType + ', ' + tableHTML;

        // Setting the file name
        downloadLink.download = filename;

        //triggering the function
        downloadLink.click();
    }
}

let dateRange, startDate = '',
    endDate = '';

$(function() {

    initDateRangePicker();
    initTable();

});


function initDateRangePicker() {
            dateRange = $("#daterange");
            dateRange.daterangepicker({
                locale: {
                    format: '{{config('constants.js_date_format')}}'
                },
                startDate: "{{ Carbon\Carbon::now()->subMonths(1)->format("d-m-Y")}}",
                endDate: "{{ Carbon\Carbon::now()->format("d-m-Y")}}",
                maxDate: "{{ Carbon\Carbon::yesterday()->format("d-m-Y")}}",
                opens: "left",
                ranges: {
                    "{{trans('app.general.yesterday')}}": [moment().subtract(1, "days"), moment().subtract(1, "days")],
                    "{{trans('app.general.this_month')}}": [moment().startOf("month"), moment().endOf("month")],
                    "{{trans_choice('app.general.last_count_months',1)}}": [moment("{{Carbon\Carbon::now()->subMonths(1)->format("Y-m-d")}}"), moment("{{Carbon\Carbon::now()->format("Y-m-d")}}")],
                    "{{trans_choice('app.general.last_count_months',3)}}": [moment("{{Carbon\Carbon::now()->subMonths(3)->format("Y-m-d")}}"), moment("{{Carbon\Carbon::now()->format("Y-m-d")}}")],
                    "{{trans_choice('app.general.last_count_months',6)}}": [moment("{{Carbon\Carbon::now()->subMonths(6)->format("Y-m-d")}}"), moment("{{Carbon\Carbon::now()->format("Y-m-d")}}")],
                    "{{trans_choice('app.general.last_count_months',12)}}": [moment("{{Carbon\Carbon::now()->subMonths(12)->format("Y-m-d")}}"), moment("{{Carbon\Carbon::now()->format("Y-m-d")}}")],

                }
            }).on("apply.daterangepicker", function (event, picker) {
                startDate = dateRange.data("daterangepicker").startDate.format('{{config('constants.js_date_format')}}');
                endDate = dateRange.data("daterangepicker").endDate.format('{{config('constants.js_date_format')}}');
                initTable();
            });

            startDate = dateRange.data("daterangepicker").startDate.format('{{config('constants.js_date_format')}}');
            endDate = dateRange.data("daterangepicker").endDate.format('{{config('constants.js_date_format')}}');
        }
</script>



@endpush