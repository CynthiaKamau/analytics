@extends('partials.facility.app')
@push('styles')
    <link rel="stylesheet" type="text/css" href="{{asset('libs/bootstrap-select/dist/css/bootstrap-select.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset("libs/bootstrap-daterangepicker/daterangepicker.css")}}"/>
    <link rel="stylesheet" type="text/css" href="{{asset('libs/chartjs/Chart.min.css')}}">
    <style>
        canvas {
            -moz-user-select: none;
            -webkit-user-select: none;
            -ms-user-select: none;
        }
    </style>
@endpush
@push("content")

    <section class="content-header">
        <h1>
            Analytics
        </h1>
        <ol class="breadcrumb">
            <li>
                <a href="{{route('facility.dashboard')}}">
                    <i class="fa fa-dashboard"></i> Dashboard</a>
            </li>
            <li>
                <a href="{{route("facility.visits.list")}}">South Lake Analytics</a>
            </li>
        </ol>
        <div>Visualization of various facility data</div>
    </section>

    @medyqPermission("can-view-general-report")
    <section class="content">

        <div class="row">
            <div class="col-md-12">
                <div class="box box-success">
                    <div class="box-header with-border">
                        <div class="row">
                            <div class="col-md-3">
                                <label class="control-label">Clinic</label>
                                <select class="form-control selectpicker" multiple="multiple" id="clinics">
                                    @foreach($clinics as $clinic)
                                        <option value="{{$clinic->uuid}}">{{$clinic->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="col-md-2">
                                <label class="control-label">Status</label>
                                <select class="form-control selectpicker" id="status">
                                    <option value="">Any</option>
                                    <option value="active">Active</option>
                                    <option value="ended">Ended</option>
                                </select>
                            </div>
                            <div class="col-md-2">
                                <label class="control-label">&nbsp;</label>
                                <div class="checkbox">
                                    <label style="padding-left: 0;">
                                        <input type="checkbox" id="emergency"
                                               value="{{config('constants.true')}}">
                                        <span class="cr"><i class="cr-icon fa fa-check"></i></span>
                                        Emergency
                                    </label>
                                </div>
                            </div>
                            <div class="col-md-5">
                                <label for="daterange">Select date range</label>
                                <input type="text" name="daterange" id="daterange"
                                       class="form-control" autocomplete="off" autocapitalize="off"/>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">

            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

                @include('analytics::partials.visits')

            </div>

        </div>

        <div class="row">

            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">

                @include('analytics::partials.payment-modes')

            </div>

            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">

                @include('analytics::partials.sales')

            </div>

        </div>

    </section>
    @endmedyqPermission

@endpush

@push("footer_scripts")

    <script type="text/javascript"
            src="{{asset('libs/bootstrap-select/dist/js/bootstrap-select.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('libs/moment/min/moment.min.js')}}"></script>
    <script type="text/javascript" src="{{asset("libs/bootstrap-daterangepicker/daterangepicker.js")}}"></script>
    <script type="text/javascript" src="{{asset('libs/chartjs/Chart.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('libs/hammerjs/hammer.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('libs/chartjs/plugins/chartjs-plugin-zoom.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('libs/chartjs/Chart.min.js')}}"></script>
    <script type="text/javascript">

        var currencySymbol = '{{get_facility_setting('currency_symbol')}}';
        let clinics, status, isEmergency;
        let dateRange, startDate = '', endDate = '';

        function initDateRangePicker() {

            dateRange = $("#daterange");

            dateRange.daterangepicker({
                locale: {
                    format: '{{config('constants.js_date_format')}}'
                },
                startDate: "{{ Carbon\Carbon::now()->subMonths(1)->format("d-m-Y")}}",
                endDate: "{{ Carbon\Carbon::now()->format("d-m-Y")}}",
                maxDate: "{{ Carbon\Carbon::yesterday()->format("d-m-Y")}}",
                opens: "left",
                ranges: {
                    "{{trans('app.general.yesterday')}}": [moment().subtract(1, "days"), moment().subtract(1, "days")],
                    "{{trans('app.general.this_month')}}": [moment().startOf("month"), moment().endOf("month")],
                    "{{trans_choice('app.general.last_count_months',1)}}": [moment("{{Carbon\Carbon::now()->subMonths(1)->format("Y-m-d")}}"), moment("{{Carbon\Carbon::now()->format("Y-m-d")}}")],
                    "{{trans_choice('app.general.last_count_months',3)}}": [moment("{{Carbon\Carbon::now()->subMonths(3)->format("Y-m-d")}}"), moment("{{Carbon\Carbon::now()->format("Y-m-d")}}")],
                    "{{trans_choice('app.general.last_count_months',6)}}": [moment("{{Carbon\Carbon::now()->subMonths(6)->format("Y-m-d")}}"), moment("{{Carbon\Carbon::now()->format("Y-m-d")}}")],
                    "{{trans_choice('app.general.last_count_months',9)}}": [moment("{{Carbon\Carbon::now()->subMonths(9)->format("Y-m-d")}}"), moment("{{Carbon\Carbon::now()->format("Y-m-d")}}")],
                    "{{trans_choice('app.general.last_count_years',1)}}": [moment("{{Carbon\Carbon::now()->subYear()->format("Y-m-d")}}"), moment("{{Carbon\Carbon::now()->format("Y-m-d")}}")],
                    "{{trans_choice('app.general.last_count_years',2)}}": [moment("{{Carbon\Carbon::now()->subYears(2)->format("Y-m-d")}}"), moment("{{Carbon\Carbon::now()->format("Y-m-d")}}")],
                    "{{trans_choice('app.general.last_count_years',3)}}": [moment("{{Carbon\Carbon::now()->subYears(3)->format("Y-m-d")}}"), moment("{{Carbon\Carbon::now()->format("Y-m-d")}}")],

                }
            }).on("apply.daterangepicker", function (event, picker) {

                startDate = dateRange.data("daterangepicker").startDate.format('{{config('constants.js_date_format')}}');

                endDate = dateRange.data("daterangepicker").endDate.format('{{config('constants.js_date_format')}}');

                initCharts();
            });

            startDate = dateRange.data("daterangepicker").startDate.format('{{config('constants.js_date_format')}}');

            endDate = dateRange.data("daterangepicker").endDate.format('{{config('constants.js_date_format')}}');
        }

        function initCharts() {

            loadVisits(function () {

                loadPaymentModes(function () {

                    loadSales(function () {

                    })

                });

            })

        }

        $(function () {

            clinics = $("#clinics");

            clinics.change(function () {

                initCharts();

            });

            status = $("#status");
            status.change(function () {
                initCharts();
            });

            isEmergency = $("#emergency");
            isEmergency.change(function () {

                initCharts();

            });

            initDateRangePicker();

            setTimeout(function () {

                initCharts();

            }, 500);

        });

    </script>

@endpush