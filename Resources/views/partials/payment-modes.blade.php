<div class="box box-widget">
    <div class="box-header with-border">
        <div class="title-block">
                            <span class="title">
                                Payment Modes
                            </span>
            <span class="description">
                                Amounts Paid Using Different Payment Modes
                            </span>
        </div>
    </div>

    <div class="box-body">

        <div class="placeholder-item">

            <div id="paymentModesBackGround" class="animated-background"></div>

            <canvas id="paymentModesChart" class="hidden"></canvas>

        </div>

    </div>


</div>

@push("footer_end")
    <script type="text/javascript">

        let paymentModesChart;

        function loadPaymentModes(callback) {

            let config = {
                type: 'doughnut',
                data: {
                    labels: [],
                    datasets: []
                },
                options: {
                    responsive: true,
                    legend: {
                        position: 'right',
                    },
                    title: {
                        display: false,
                        text: 'Payment Modes'
                    },
                    animation: {
                        animateScale: true,
                        animateRotate: true
                    },
                    tooltips: {
                        callbacks: {
                            label: function (tooltipItem, chart) {
                                let datasetLabel = chart.labels[tooltipItem.index] || '';
                                return datasetLabel + ' : ' + currencySymbol + ' ' + number_format(chart.datasets[tooltipItem.datasetIndex].data[tooltipItem.index], 2);
                            }
                        }
                    }
                }

            };

            let paymentModesChartContext = $('#paymentModesChart');
            let paymentModesBackGround = $('#paymentModesBackGround');

            request({
                url: '{{route('analytics.dashboard.payment-modes-rest')}}',
                data: {
                    clinics: clinics.val(),
                    status: status.val(),
                    is_emergency: isEmergency.is(':checked') ? 1 : 0,
                    start_date: startDate,
                    end_date: endDate,
                },
                beforeSend: function () {

                    paymentModesChartContext.addClass('hidden');

                    paymentModesBackGround.removeClass('hidden');

                },
                success: function (response) {

                    config.data.labels = response.labels;
                    config.data.datasets = response.datasets;

                    if (paymentModesChart) {
                        paymentModesChart.destroy();
                    }

                    paymentModesChart = new Chart(paymentModesChartContext, config);

                },
                error: function (xhr) {

                    let message = "";

                    $.each(xhr.responseJSON.messages, function (index, str) {
                        message = message.concat(str).concat('\n');
                    });

                    Swal.fire({
                        title: '{{trans('app.general.request_error')}}',
                        text: message,
                        type: 'error',
                    });
                }, complete: function (xhr) {

                    paymentModesBackGround.addClass('hidden');

                    paymentModesChartContext.removeClass('hidden');

                    if (typeof callback === 'function') {
                        try {
                            callback.call();
                        } catch (e) {
                            console.log(e)
                        }
                    }

                }
            });

        }

    </script>
@endpush