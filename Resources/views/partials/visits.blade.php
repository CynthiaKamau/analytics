<div class="box box-widget">
    <div class="box-header with-border">
        <div class="title-block">
            <span class="title">
                Visit Counts
            </span>
            <span class="description">
                Number of Visits In The Facility
            </span>
        </div>
    </div>

    <div class="box-body">
        <div class="placeholder-item">
            <div id="visitsBackGround" class="animated-background"></div>
            <canvas id="visitsChart" class="hidden"></canvas>
        </div>
    </div>

</div>

@push("footer_end")
    <script type="text/javascript">

        let visitsChart;

        function loadVisits(callback) {

            let config = {
                type: 'line',
                data: {
                    labels: [],
                    datasets: []
                },
                options: {
                    responsive: true,
                    maintainAspectRatio: false,
                    elements: {
                        point: {
                            pointStyle: 'circle'
                        }
                    },
                    title: {
                        display: false,
                        text: 'Visits'
                    },
                    tooltips: {
                        mode: 'index',
                        intersect: false,
                    },
                    hover: {
                        mode: 'nearest',
                        intersect: true
                    },
                    scales: {
                        xAxes: [{
                            display: true,
                            scaleLabel: {
                                display: false,
                                labelString: 'Month'
                            }
                        }],
                        yAxes: [{
                            display: true,
                            scaleLabel: {
                                display: true,
                                labelString: 'Patient Count'
                            },
                            ticks: {
                                beginAtZero: true,
                                callback: function (value) {
                                    if (value % 1 === 0) {
                                        return value;
                                    }
                                }
                            }
                        }]
                    },
                    plugins: {
                        zoom: {
                            pan: {
                                enabled: false,
                                mode: 'xy'
                            },
                            zoom: {
                                enabled: true,
                                mode: 'x'
                            }
                        }
                    }
                }
            };

            let visitsChartContext = $('#visitsChart');
            let visitsBackGround = $('#visitsBackGround');

            function setVisitsChart(visitsChart) {
                this.visitsChart = visitsChart;
            }

            request({
                url: '{{route('analytics.dashboard.visits-rest')}}',
                data: {
                    clinics: clinics.val(),
                    status: status.val(),
                    is_emergency: isEmergency.is(':checked') ? 1 : 0,
                    start_date: startDate,
                    end_date: endDate,
                },
                beforeSend: function () {

                    if (visitsChart) {
                        visitsChart.canvas.parentNode.style.height = 'auto';
                    }

                    visitsChartContext.addClass('hidden');

                    visitsBackGround.removeClass('hidden');

                },
                success: function (response) {

                    config.data.labels = response.labels;
                    config.data.datasets = response.datasets;

                    if (visitsChart) {
                        visitsChart.destroy();
                    }

                    visitsChart = new Chart(visitsChartContext, config);
                    //chart.canvas.parentNode.style.width = '128px';
                    visitsChart.canvas.parentNode.style.height = '400px';

                    //chart.update(config);

                },
                error: function (xhr) {

                    let message = "";

                    $.each(xhr.responseJSON.messages, function (index, str) {
                        message = message.concat(str).concat('\n');
                    });

                    Swal.fire({
                        title: '{{trans('app.general.request_error')}}',
                        text: message,
                        type: 'error',
                    });
                }, complete: function (xhr) {

                    visitsBackGround.addClass('hidden');

                    visitsChartContext.removeClass('hidden');

                    if (typeof callback === 'function') {
                        try {
                            callback.call();
                        } catch (e) {
                            console.log(e)
                        }
                    }

                }
            });

        }

    </script>
@endpush