<div class="box box-widget">
    <div class="box-header with-border">
        <div class="title-block">
                            <span class="title">
                                Sales
                            </span>
            <span class="description">
                                Different types of sales according to item types
                            </span>
        </div>
    </div>

    <div class="box-body">

        <div class="placeholder-item">

            <div id="salesBackGround" class="animated-background"></div>

            <canvas id="salesChart" class="hidden"></canvas>

        </div>

    </div>


</div>

@push("footer_end")
    <script type="text/javascript">

        let salesChart;

        function loadSales(callback) {

            let config = {
                type: 'doughnut',
                data: {
                    labels: [],
                    datasets: []
                },
                options: {
                    responsive: true,
                    legend: {
                        position: 'right',
                    },
                    title: {
                        display: false,
                        text: 'Payment Modes'
                    },
                    animation: {
                        animateScale: true,
                        animateRotate: true
                    },
                    tooltips: {
                        callbacks: {
                            label: function (tooltipItem, chart) {
                                let datasetLabel = chart.labels[tooltipItem.index] || '';
                                return datasetLabel + ' : ' + currencySymbol + ' ' + number_format(chart.datasets[tooltipItem.datasetIndex].data[tooltipItem.index], 2);
                            }
                        }
                    }
                }

            };

            let salesChartContext = $('#salesChart');
            let salesBackGround = $('#salesBackGround');

            request({
                url: '{{route('analytics.dashboard.sales-rest')}}',
                data: {
                    clinics: clinics.val(),
                    status: status.val(),
                    is_emergency: isEmergency.is(':checked') ? 1 : 0,
                    start_date: startDate,
                    end_date: endDate,
                },
                beforeSend: function () {

                    salesChartContext.addClass('hidden');

                    salesBackGround.removeClass('hidden');

                },
                success: function (response) {

                    config.data.labels = response.labels;
                    config.data.datasets = response.datasets;

                    if (salesChart) {
                        salesChart.destroy();
                    }

                    salesChart = new Chart(salesChartContext, config);

                },
                error: function (xhr) {

                    let message = "";

                    $.each(xhr.responseJSON.messages, function (index, str) {
                        message = message.concat(str).concat('\n');
                    });

                    Swal.fire({
                        title: '{{trans('app.general.request_error')}}',
                        text: message,
                        type: 'error',
                    });
                }, complete: function (xhr) {

                    salesBackGround.addClass('hidden');

                    salesChartContext.removeClass('hidden');

                    if (typeof callback === 'function') {
                        try {
                            callback.call();
                        } catch (e) {
                            console.log(e)
                        }
                    }

                }
            });

        }

    </script>
@endpush