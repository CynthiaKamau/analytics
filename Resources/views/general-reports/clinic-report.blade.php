@extends('partials.facility.app')
@push('styles')

<link rel="stylesheet" type="text/css" href="{{asset('libs/bootstrap-select/dist/css/bootstrap-select.min.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset("libs/bootstrap-daterangepicker/daterangepicker.css")}}"/>
<link rel="stylesheet" type="text/css" href="{{asset('libs/chartjs/Chart.min.css')}}">

	<style>
		canvas {
			-moz-user-select: none;
			-webkit-user-select: none;
			-ms-user-select: none;
		}
	</style>
@endpush
@push("content")
	
	<section class="content-header">
		<h1>
			Analytics
        </h1>
		<ol class="breadcrumb">
			<li>
				<a href="{{route('facility.dashboard')}}">
					<i class="fa fa-dashboard"></i> Dashboard</a>
			</li>
			<li>
				<a href="{{route("facility.visits.list")}}">South Lake Analytics</a>
			</li>
		</ol>
		<div>Visualization of various clinic data</div>
	</section>
	
    @medyqPermission("can-view-clinic-report")
	<section class="content">
		
		<div class="box box-success">
			<div class="box-body">
				
				<div class="row text-center">
					{{-- <div class="col-md-2">
                        <label class="control-label">Visit Status</label>
                        <select class="form-control selectpicker" id="visit_status">
                            <option selected="selected" value="">Any</option>
                            <option value="active">Active</option>
                            <option value="ended">Ended</option>
                        </select>
					</div> --}}
					<div class="col-md-2">
						<label class="control-label">&nbsp;</label>
						<div class="checkbox">
							<label style="padding-left: 0;">
								<input type="checkbox" id="emergency"
									value="{{config('constants.true')}}">
								<span class="cr"><i class="cr-icon fa fa-check"></i></span>
								Emergency
							</label>
						</div>
					</div>
					<div class="col-md-5">
						<label>Select date range</label>
						<input type="text" name="daterange" id="daterange"
							class="form-control" autocomplete="off" autocapitalize="off"/>
					</div>
					<div class="col-md-5">
						<button id="save-clinic-chart" class="btn btn-success pull-right"> Export PDF </button>
					</div>	
				</div>
				
				<div class="row">
					<div class="col-md-12">
                        
						<canvas id="clinicChart"></canvas>
					
					</div>
					
				</div>
			
			</div>
		
		</div>
	
	</section>
	@endmedyqPermission

@endpush

@push("footer_scripts")
	
<script type="text/javascript" src="{{asset('libs/bootstrap-select/dist/js/bootstrap-select.min.js')}}"></script>
<script type="text/javascript" src="{{asset('libs/moment/min/moment.min.js')}}"></script>
<script type="text/javascript" src="{{asset("libs/bootstrap-daterangepicker/daterangepicker.js")}}"></script>
<script type="text/javascript" src="{{asset('libs/chartjs/Chart.min.js')}}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jspdf/1.3.3/jspdf.min.js"></script>


	@include('partials.global.table-scripts')

	<script type="text/javascript">

	let clinicChartContext = document.getElementById('clinicChart');

	let status, isEmergency;
	let dateRange, startDate = '', endDate = '';

	let config = {
		type: 'bar',
		data: {
			labels: [],
			datasets: []
		},
		options: {
			responsive: true,
			maintainAspectRatio: false,
			title: {
				display: false,
				text: 'Absorption'
			},
			tooltips: {
				mode: 'index',
				intersect: false,
			},
			hover: {
				mode: 'nearest',
				intersect: true
			},
			scales: {
				xAxes: [{
					display: true,
					scaleLabel: {
						display: true,
						labelString: 'Clinics'
					}
				}],
				yAxes: [{
					display: true,
					ticks: {
							beginAtZero:true
					},
					scaleLabel: {
						display: true,
						labelString: 'Visits'
					}
				}],
			},
		}
	};

	$(function () {
		status = $("#status");
		status.change(function () {
			initChart();
		});	

		isEmergency = $("#emergency");
		isEmergency.change(function () {
			initChart();
		});	

		initDateRangePicker();

		initChart();

	});

	function initChart() {
		request({
			url: '{{route('analytics.reports.clinic_rest')}}',
			data: {
				status: status.val(),
				is_emergency: isEmergency.is(':checked') ? 1 : 0,
				start_date: startDate,
				end_date: endDate,
			},
			beforeSend: function () {
				showLoadingAnimation();
			}
			, complete: function (xhr) {
				hideLoadingAnimation();
			},
			success: function (response) {

				config.data.labels = response.labels;
				config.data.datasets = response.datasets;

				let chart =  new Chart(clinicChartContext, config);

				chart.canvas.parentNode.style.height = '400px';

				Chart.plugins.register({
					afterRender: function(c) {
						var ctx = c.chart.ctx;
						ctx.save();

						ctx.globalCompositeOperation = 'destination-over';
						ctx.fillStyle = 'white';
						ctx.fillRect(0, 0, c.chart.width, c.chart.height);
						ctx.restore();
					}
				});

			},
			error: function (xhr) {

				let message = "";

				$.each(xhr.responseJSON.messages, function (index, str) {
					message = message.concat(str).concat('\n');
				});

				Swal.fire({
					title: '{{trans('app.general.request_error')}}',
					text: message,
					type: 'error',
				});
			}
		});	

	}

	function initDateRangePicker() {
		dateRange = $("#daterange");
		dateRange.daterangepicker({
			locale: {
				format: '{{config('constants.js_date_format')}}'
			},
			startDate: "{{ Carbon\Carbon::now()->subMonths(1)->format("d-m-Y")}}",
			endDate: "{{ Carbon\Carbon::now()->format("d-m-Y")}}",
			maxDate: "{{ Carbon\Carbon::yesterday()->format("d-m-Y")}}",
			opens: "left",
			ranges: {
				"{{trans('app.general.yesterday')}}": [moment().subtract(1, "days"), moment().subtract(1, "days")],
				"{{trans('app.general.this_month')}}": [moment().startOf("month"), moment().endOf("month")],
				"{{trans_choice('app.general.last_count_months',1)}}": [moment("{{Carbon\Carbon::now()->subMonths(1)->format("Y-m-d")}}"), moment("{{Carbon\Carbon::now()->format("Y-m-d")}}")],
				"{{trans_choice('app.general.last_count_months',3)}}": [moment("{{Carbon\Carbon::now()->subMonths(3)->format("Y-m-d")}}"), moment("{{Carbon\Carbon::now()->format("Y-m-d")}}")],
				"{{trans_choice('app.general.last_count_months',6)}}": [moment("{{Carbon\Carbon::now()->subMonths(6)->format("Y-m-d")}}"), moment("{{Carbon\Carbon::now()->format("Y-m-d")}}")],
				"{{trans_choice('app.general.last_count_months',12)}}": [moment("{{Carbon\Carbon::now()->subMonths(12)->format("Y-m-d")}}"), moment("{{Carbon\Carbon::now()->format("Y-m-d")}}")],

			}
		}).on("apply.daterangepicker", function (event, picker) {
			startDate = dateRange.data("daterangepicker").startDate.format('{{config('constants.js_date_format')}}');
			endDate = dateRange.data("daterangepicker").endDate.format('{{config('constants.js_date_format')}}');
			initChart();
			
		});

		startDate = dateRange.data("daterangepicker").startDate.format('{{config('constants.js_date_format')}}');
		endDate = dateRange.data("daterangepicker").endDate.format('{{config('constants.js_date_format')}}');
	}

	document.getElementById('save-clinic-chart').addEventListener("click", downloadCC);

	function downloadCC() {
		var imgData = document.getElementById("clinicChart").toDataURL("image/jpeg", 1.0);
		var pdf = new jsPDF();

		pdf.addImage(imgData, 'JPEG', 0,0);
		pdf.save("ClinicReport.pdf");
    };

	</script>

@endpush



