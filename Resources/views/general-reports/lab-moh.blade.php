@extends('partials.facility.app')
@push('styles')
    <link rel="stylesheet" type="text/css" href="{{asset('libs/bootstrap-select/dist/css/bootstrap-select.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset("libs/bootstrap-daterangepicker/daterangepicker.css")}}"/>
    <style>
        canvas {
            -moz-user-select: none;
            -webkit-user-select: none;
            -ms-user-select: none;
        }
    </style>
@endpush
@push("content")

    <section class="content-header">
        <h1>
            Analytics
        </h1>
        <ol class="breadcrumb">
            <li>
                <a href="{{route('facility.dashboard')}}">
                    <i class="fa fa-dashboard"></i> Dashboard</a>
            </li>
            <li>
                <a href="{{route("facility.visits.list")}}">MOH 706 LAB Services Summary</a>
            </li>
        </ol>
        <div>Visualization of various facility data</div>
    </section>

    @medyqPermission("can-view-sales-report")
    <section class="content">
        <div class="box box-success">
            <div class="box-header with-border">
                <div class="row">

                    <div class="col-md-3">
                        <label class="control-label">Clinic</label>
                        <select class="form-control selectpicker" multiple="multiple" id="clinics" >
                            @foreach($clinics as $clinic)
                                <option value="{{$clinic->uuid}}">{{$clinic->name}}</option>
                            @endforeach
                        </select>
                    </div>
                   
                    <div class="col-md-3">
                        <label class="control-label">&nbsp;</label>
                        <div class="checkbox">
                            <label style="padding-left: 0;">
                                <input type="checkbox" id="emergency"
                                        value="{{config('constants.true')}}">
                                <span class="cr"><i class="cr-icon fa fa-check"></i></span>
                                Emergency
                            </label>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <label for="daterange">Select date range</label>
                        <input type="text" name="daterange" id="daterange"
                                class="form-control" autocomplete="off" autocapitalize="off"/>
                    </div>
                    <div class="col-md-2">
                        <a id="downloadLink" onclick="exportF(this)" class="btn btn-success pull-right">Export to excel</a>

                </div>
            </div>    

            <div class="box-body ">
                <div class="row">
                    <div class="col-md-12">

                        <table id="results" class="table" border=2>
                          <thead></thead>
                            <thead>
                                <tr>
                                    <th> Test Item</th>
                                    <th> Total Exam</th>
                                    <th> Low</th>
                                    <th> High</th>
                                    <th> </th>
                                    <th> Positive</th>
                                    <th> Negative</th>
                                </tr>
                            </thead>
                            <tbody id="resultnody">
                            </tbody>
                            </table>

                    </div>
                </div>
            </div>

            </div>
        </div>

    </section>
    @endmedyqPermission

@endpush

@push("footer_scripts")

@include('partials.global.table-scripts')

    <script type="text/javascript" src="{{asset('libs/moment/min/moment.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('libs/datatables/DataTables-1.10.18/js/jquery.dataTables.js')}}"> </script>
     <script type="text/javascript" src="{{asset("libs/bootstrap-daterangepicker/daterangepicker.js")}}"></script>
    <script type="text/javascript"
            src="{{asset('libs/bootstrap-select/dist/js/bootstrap-select.min.js')}}"></script>

    <script src="https://cdn.jsdelivr.net/gh/linways/table-to-excel@v1.0.4/dist/tableToExcel.js"></script>

    <script type="text/javascript">

    let clinics, isEmergency;
    let dateRange, startDate = '', endDate = '';

    $(function () {
        clinics = $("#clinics");
        clinics.change(function () {
            initTable();
        });

        schemes = $("#schemes");
        schemes.change(function () {
            initTable();
        });

        isEmergency = $("#emergency");
        isEmergency.change(function () {
            initTable();
        });

        initDateRangePicker();
        initTable()

    });

    function initDateRangePicker() {
        dateRange = $("#daterange");
        dateRange.daterangepicker({
            locale: {
                format: '{{config('constants.js_date_format')}}'
            },
            startDate: "{{ Carbon\Carbon::now()->subMonths(1)->format("d-m-Y")}}",
            endDate: "{{ Carbon\Carbon::now()->format("d-m-Y")}}",
            maxDate: "{{ Carbon\Carbon::yesterday()->format("d-m-Y")}}",
            opens: "left",
            ranges: {
                "{{trans('app.general.yesterday')}}": [moment().subtract(1, "days"), moment().subtract(1, "days")],
                "{{trans('app.general.this_month')}}": [moment().startOf("month"), moment().endOf("month")],
                "{{trans_choice('app.general.last_count_months',1)}}": [moment("{{Carbon\Carbon::now()->subMonths(1)->format("Y-m-d")}}"), moment("{{Carbon\Carbon::now()->format("Y-m-d")}}")],
                "{{trans_choice('app.general.last_count_months',3)}}": [moment("{{Carbon\Carbon::now()->subMonths(3)->format("Y-m-d")}}"), moment("{{Carbon\Carbon::now()->format("Y-m-d")}}")],
                "{{trans_choice('app.general.last_count_months',6)}}": [moment("{{Carbon\Carbon::now()->subMonths(6)->format("Y-m-d")}}"), moment("{{Carbon\Carbon::now()->format("Y-m-d")}}")],
                "{{trans_choice('app.general.last_count_months',12)}}": [moment("{{Carbon\Carbon::now()->subMonths(12)->format("Y-m-d")}}"), moment("{{Carbon\Carbon::now()->format("Y-m-d")}}")],

            }
        }).on("apply.daterangepicker", function (event, picker) {
            startDate = dateRange.data("daterangepicker").startDate.format('{{config('constants.js_date_format')}}');
            endDate = dateRange.data("daterangepicker").endDate.format('{{config('constants.js_date_format')}}');
            initTable();
        });

        startDate = dateRange.data("daterangepicker").startDate.format('{{config('constants.js_date_format')}}');
        endDate = dateRange.data("daterangepicker").endDate.format('{{config('constants.js_date_format')}}');
    }

    function initTable() {
        $.ajax({
            url: "{{route('analytics.reports.urine-rest')}}",
            method: 'POST',
            data: {
                'clinics': clinics.val(),
                'is_emergency': isEmergency.is(':checked') ? 0 : 1,
                start_date: startDate,
                end_date: endDate,
            },
            success: function (response) {
                $('#resultnody').empty();
                let trHMTL = '';

                $.each(response, function(index, value) {
                    trHMTL += '<tr style="outline: thin solid">';
                   // trHMTL += '<td rowspan="' + value[0].length + '">'+index+'</td>';
                   

                    $.each(value[0], function(index1, value1) {
                        trHMTL += '<td>'+value1.blood_test+'</td>';
                        trHMTL += '<td>'+value1.blood_result_count+'</td>';                     
                        trHMTL += '<td>'+value1.blood_result_low+'</td>';
                        trHMTL += '<td>'+value1.blood_result_high+'</td>';
                        trHMTL += '<td></td>';
                        trHMTL += '<td>'+value1.blood_result_positive+'</td>';
                        trHMTL += '<td>'+value1.blood_result_negative+'</td>';
                        trHMTL += '<tr>';
                    })

                });

                $('#results').append(trHMTL);
            },

            beforeSend: function (xhr) {
                showLoadingAnimation();
            },
            complete: function (xhr) {
                hideLoadingAnimation();
            },
            error: function (xhr, error, code) {
                let message = "";
                $.each(xhr.responseJSON.messages, function (index, str) {
                    message = message.concat(str).concat("<br/>");
                });
                showErrorAlert(message);
            }
        });

    }

    function exportF(elem) {
        var table = document.getElementById("results");
        var html = table.outerHTML;
        var url = 'data:application/vnd.ms-excel,' + escape(html); // Set your html table into url
        elem.setAttribute("href", url);
        elem.setAttribute("download", "MOH-706.xls"); // Choose the file name
        return false;
    }

    </script>
@endpush
