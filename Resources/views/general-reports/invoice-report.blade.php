@extends('partials.facility.app')

@push('styles')
    <link rel="stylesheet" type="text/css" href="{{asset('libs/bootstrap-select/dist/css/bootstrap-select.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset("libs/bootstrap-daterangepicker/daterangepicker.css")}}"/>
    <link rel="stylesheet" type="text/css" href="{{asset('libs/chartjs/Chart.min.css')}}">
    <style>
        canvas {
            -moz-user-select: none;
            -webkit-user-select: none;
            -ms-user-select: none;
        }
    </style>
@endpush

@push("content")
<div class="row">
    <div class="col-md-12">
        <div class="box box-success">
            <div class="box-header with-border">
                <div class="row">
                    {{-- <div class="col-md-3">
                        <label class="control-label">Clinic</label>
                        <select class="form-control selectpicker" multiple="multiple" id="clinics">
                            @foreach($clinics as $clinic)
                                <option value="{{$clinic->uuid}}">{{$clinic->name}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="col-md-2">
                        <label class="control-label">&nbsp;</label>
                        <div class="checkbox">
                            <label style="padding-left: 0;">
                                <input type="checkbox" id="emergency"
                                       value="{{config('constants.true')}}">
                                <span class="cr"><i class="cr-icon fa fa-check"></i></span>
                                Emergency
                            </label>
                        </div>
                    </div> --}}
                    <div class="col-md-5">
                        <label>Select date range</label>
                        <input type="text" name="daterange" id="daterange"
                               class="form-control" autocomplete="off" autocapitalize="off"/>
                    </div>

                </div>
            </div>
            <div class="box-body">

                <button id="save-image" class="btn btn-success pull-right"> Save as PDF</button>

                <div class="row" style="width: 88vw; height: 40vh; position: relative">
                    <div class="col-md-12">
                        <canvas id="labChart"></canvas>

                    </div>
                </div>

            </div>

        </div>
    </div>
</div>
@endpush

@push("footer_scripts")

    <script type="text/javascript" src="{{asset('libs/bootstrap-select/dist/js/bootstrap-select.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('libs/moment/min/moment.min.js')}}"></script>
    <script type="text/javascript" src="{{asset("libs/bootstrap-daterangepicker/daterangepicker.js")}}"></script>
    <script type="text/javascript" src="{{asset('libs/chartjs/Chart.min.js')}}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jspdf/1.3.3/jspdf.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/FileSaver.js/1.3.3/FileSaver.min.js"></script>

    <script type="text/javascript">
        let labChartContext = $('#labChart');
        let clinics, status, isEmergency;
        let dateRange, startDate = '', endDate = '';
        let config = {
            type: 'bar',
            data: {
                labels: [],
                datasets: []
            },
            options: {
                responsive: true,
                maintainAspectRatio: false,
                elements: {
                    point: {
                        pointStyle: 'circle'
                    }
                },
                title: {
                    display: false,
                    text: 'Absorption'
                },
                tooltips: {
                    mode: 'index',
                    intersect: false,
                },
                hover: {
                    mode: 'nearest',
                    intersect: true
                },
                scales: {
                    xAxes: [{
                        display: true,
                        scaleLabel: {
                            display: true,
                            labelString: 'Billables'
                        }
                    }],
                    yAxes: [{
                        display: true,
                        ticks: {
                                beginAtZero:true
                        },
                        scaleLabel: {
                            display: true,
                            labelString: 'Income Generated'
                        }
                    }],
                },
            }
        };

        $(function () {
            clinics = $("#clinics");
            clinics.change(function () {
                initChart();
            });

            status = $("#status");
            status.change(function () {
                initChart();
            });

            isEmergency = $("#emergency");
            isEmergency.change(function () {

                initChart();

            });

            initDateRangePicker();

            initChart();

        });

        function initChart() {

            request({
                url: '{{route('analytics.reports.invoice_amount_rest')}}',
                data: {
                    clinics: clinics.val(),
                    status: status.val(),
                    is_emergency: isEmergency.is(':checked') ? 0 : 1,
                    start_date: startDate,
                    end_date: endDate,
                },
                beforeSend: function () {
                    showLoadingAnimation();
                }
                , complete: function (xhr) {
                    hideLoadingAnimation();
                },
                success: function (response) {

                    config.data.labels = response.labels;
                    config.data.datasets = response.datasets;

                   let chart =  new Chart(labChartContext, config);
                   
                    chart.canvas.parentNode.style.height = '400px';

                    Chart.plugins.register({
                        afterRender: function(c) {
                            var ctx = c.chart.ctx;
                            ctx.save();

                            ctx.globalCompositeOperation = 'destination-over';
                            ctx.fillStyle = 'white';
                            ctx.fillRect(0, 0, c.chart.width, c.chart.height);
                            ctx.restore();
                        }
                    });
                    //chart.canvas.parentNode.style.width = '128px';



                },
                error: function (xhr) {

                    console.log(errorMessage);

                    let message = "";

                    // $.each(xhr.responseJSON.messages, function (index, str) {
                    //     message = message.concat(str).concat('\n');
                    // });

                    Swal.fire({
                        title: '{{trans('app.general.request_error')}}',
                        text: message,
                        type: 'error',
                    });
                }
            });
        }

        function initDateRangePicker() {
            dateRange = $("#daterange");
            dateRange.daterangepicker({
                locale: {
                    format: '{{config('constants.js_date_format')}}'
                },
                startDate: "{{ Carbon\Carbon::now()->subMonths(1)->format("d-m-Y")}}",
                endDate: "{{ Carbon\Carbon::now()->format("d-m-Y")}}",
                maxDate: "{{ Carbon\Carbon::yesterday()->format("d-m-Y")}}",
                opens: "left",
                ranges: {
                    "{{trans('app.general.yesterday')}}": [moment().subtract(1, "days"), moment().subtract(1, "days")],
                    "{{trans('app.general.this_month')}}": [moment().startOf("month"), moment().endOf("month")],
                    "{{trans_choice('app.general.last_count_months',1)}}": [moment("{{Carbon\Carbon::now()->subMonths(1)->format("Y-m-d")}}"), moment("{{Carbon\Carbon::now()->format("Y-m-d")}}")],
                    "{{trans_choice('app.general.last_count_months',3)}}": [moment("{{Carbon\Carbon::now()->subMonths(3)->format("Y-m-d")}}"), moment("{{Carbon\Carbon::now()->format("Y-m-d")}}")],
                    "{{trans_choice('app.general.last_count_months',6)}}": [moment("{{Carbon\Carbon::now()->subMonths(6)->format("Y-m-d")}}"), moment("{{Carbon\Carbon::now()->format("Y-m-d")}}")],
                    "{{trans_choice('app.general.last_count_months',12)}}": [moment("{{Carbon\Carbon::now()->subMonths(12)->format("Y-m-d")}}"), moment("{{Carbon\Carbon::now()->format("Y-m-d")}}")],

                }
            }).on("apply.daterangepicker", function (event, picker) {
                startDate = dateRange.data("daterangepicker").startDate.format('{{config('constants.js_date_format')}}');
                endDate = dateRange.data("daterangepicker").endDate.format('{{config('constants.js_date_format')}}');
                initChart();
            });

            startDate = dateRange.data("daterangepicker").startDate.format('{{config('constants.js_date_format')}}');
            endDate = dateRange.data("daterangepicker").endDate.format('{{config('constants.js_date_format')}}');
        }

        $("button").on('click', function() {
            var imgData = document.getElementById("labChart").toDataURL("image/jpeg", 1.0);
            var pdf = new jsPDF();

            pdf.addImage(imgData, 'JPEG', 0,0);
            pdf.save("download.pdf");
        });

    </script>

@endpush

