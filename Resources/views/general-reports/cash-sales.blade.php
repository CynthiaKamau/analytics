@extends('partials.facility.app')
@push('styles')
    <link rel="stylesheet" type="text/css" href="{{asset('libs/bootstrap-select/dist/css/bootstrap-select.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset("libs/bootstrap-daterangepicker/daterangepicker.css")}}"/>
    @include('partials.global.table-styles')
    <style type="text/css">

        table.dataTable thead th, table.dataTable tbody tr td {
            text-align: center;
        }

        table.dataTable thead .sorting_asc:after {
            content: "";
        }

    </style>
@endpush
@push("content")

    <section class="content-header">
        <h1>
            Cash Sales
          
        </h1>
        <ol class="breadcrumb">
            <li>
                <a href="{{route('facility.dashboard')}}">
                    <i class="fa fa-dashboard"></i> Dashboard</a>
            </li>
            <li>
                <a href="{{route("facility.visits.list")}}">Cash Sales</a>
            </li>
        </ol>

    </section>

    @medyqPermission("can-view-sales-report")
    <section class="content">

        <div class="box box-success">
            <div class="box-header with-border">
                <div class="row">
                    <div class="col-md-2">
                        <label class="control-label">Clinic</label>
                        <select class="form-control selectpicker" multiple="multiple" id="clinics">
                            @foreach($clinics as $clinic)
                                <option value="{{$clinic->uuid}}">{{$clinic->name}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="col-md-2">
                        <label class="control-label">Status</label>
                        <select class="form-control selectpicker" id="status">
                            <option value="">Any</option>
                            <option value="active">Active</option>
                            <option value="ended">Ended</option>
                        </select>
                    </div>
                    <div class="col-md-2">
                        <label class="control-label">&nbsp;</label>
                        <div class="checkbox">
                            <label style="padding-left: 0;">
                                <input type="checkbox" id="emergency"
                                       value="{{config('constants.true')}}">
                                <span class="cr"><i class="cr-icon fa fa-check"></i></span>
                                Emergency
                            </label>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <label>Select date range</label>
                        <input type="text" name="daterange" id="daterange"
                               class="form-control" autocomplete="off" autocapitalize="off"/>
                    </div>
                    <div class="col-md-2">
                                <a id="downloadLink" onclick="exportF(this)" class="btn btn-success">Export to excel</a>

                    </div>

                </div>
            </div>
            <div class="box-body">

                <div class="row">
                    <div class="col-md-12">
                        <table class="table table-striped" border=2 id="cashTable">
                            <thead>
                            <tr>
                                <th class="text-center">Date</th>
                                <th class="text-center">Name</th>
                                <th class="text-center">Cash</th>      
                            </tr>
                            </thead>
                            <tbody>

                            </tbody>
                            <tfoot align="right">
                            <tr>
                                <th class="text-center" colspan="2">Total</th>  
                                <th id="total_amount" class="text-center"></th>  
                            </tr>
		
	                      </tfoot>
                        </table>
                    </div>
                </div>

            </div>

        </div>

    </section>
    @endmedyqPermission

@endpush

@push("footer_scripts")
    <script type="text/javascript" src="{{asset('libs/moment/min/moment.min.js')}}"></script>
    <script type="text/javascript" src="{{asset("libs/bootstrap-daterangepicker/daterangepicker.js")}}"></script>
    <script type="text/javascript" src="{{asset('libs/bootstrap-select/dist/js/bootstrap-select.min.js')}}"></script>
    <script src="https://cdn.jsdelivr.net/gh/linways/table-to-excel@v1.0.4/dist/tableToExcel.js"></script>
    @include('partials.global.table-scripts')
    <script type="text/javascript">
        let cashTable;
        let getVisitsUrl = "{{route('analytics.reports.cash_rest')}}";
        let clinics, status, isEmergency;
        let dateRange, startDate = '', endDate = '';

        $(function () {
            clinics = $("#clinics");
            clinics.change(function () {
                initTable();
            });

            status = $("#status");
            status.change(function () {
                initTable();
            });

            isEmergency = $("#emergency");
            isEmergency.change(function () {
                initTable();
            });

            initDateRangePicker();
            initTable();
        });

        function initDateRangePicker() {
            dateRange = $("#daterange");
            dateRange.daterangepicker({
                locale: {
                    format: '{{config('constants.js_date_format')}}'
                },
                startDate: "{{ Carbon\Carbon::now()->subMonths(1)->format("d-m-Y")}}",
                endDate: "{{ Carbon\Carbon::now()->format("d-m-Y")}}",
                maxDate: "{{ Carbon\Carbon::yesterday()->format("d-m-Y")}}",
                opens: "left",
                ranges: {
                    "{{trans('app.general.yesterday')}}": [moment().subtract(1, "days"), moment().subtract(1, "days")],
                    "{{trans('app.general.this_month')}}": [moment().startOf("month"), moment().endOf("month")],
                    "{{trans_choice('app.general.last_count_months',1)}}": [moment("{{Carbon\Carbon::now()->subMonths(1)->format("Y-m-d")}}"), moment("{{Carbon\Carbon::now()->format("Y-m-d")}}")],
                    "{{trans_choice('app.general.last_count_months',3)}}": [moment("{{Carbon\Carbon::now()->subMonths(3)->format("Y-m-d")}}"), moment("{{Carbon\Carbon::now()->format("Y-m-d")}}")],
                    "{{trans_choice('app.general.last_count_months',6)}}": [moment("{{Carbon\Carbon::now()->subMonths(6)->format("Y-m-d")}}"), moment("{{Carbon\Carbon::now()->format("Y-m-d")}}")],
                    "{{trans_choice('app.general.last_count_months',12)}}": [moment("{{Carbon\Carbon::now()->subMonths(12)->format("Y-m-d")}}"), moment("{{Carbon\Carbon::now()->format("Y-m-d")}}")],

                }
            }).on("apply.daterangepicker", function (event, picker) {
                startDate = dateRange.data("daterangepicker").startDate.format('{{config('constants.js_date_format')}}');
                endDate = dateRange.data("daterangepicker").endDate.format('{{config('constants.js_date_format')}}');
                initTable();
            });

            startDate = dateRange.data("daterangepicker").startDate.format('{{config('constants.js_date_format')}}');
            endDate = dateRange.data("daterangepicker").endDate.format('{{config('constants.js_date_format')}}');
        }

        function initTable() {

            let tableName = "#cashTable";
            if ($.fn.dataTable.isDataTable(tableName)) {
                cashTable.destroy();
            }

            cashTable = $(tableName).DataTable({
                responsive: false,
                serverSide: true,
                lengthMenu: [[100, 250, 500, 1000, 2500, 10000], [100, 250, 500, 1000, 2500, 10000]],
                cache: false,
                stateSave: false,
                columnDefs: [
                    {
                        bSortable: false,
                        class: "text-center",
                        aTargets: [0, 1, 2]
                    },
                ],
                columns: [
                    {data: "date"},
                    {data: "full_name"},
                    {data: "amount_paid"},
                ],
                language:
                dataTablesTranslations,
                ajax: {
                    url: getVisitsUrl,
                    method: 'POST',
                    data: {
                        clinics: clinics.val(),
                        status: status.val(),
                        is_emergency: isEmergency.is(':checked') ? 0 : 1,
                        start_date: startDate,
                        end_date: endDate,
                    },
                    beforeSend: function (xhr) {
                        showLoadingAnimation();
                    },
                    complete: function (xhr) {
                        hideLoadingAnimation();
                    },
                    error: function (xhr, error, code) {
                        showErrorAlert(xhr.responseJSON.messages);
                    }
                },
                createdRow: function (row, data, dataIndex) {
                    if (parseInt(data.is_emergency) ==={{config('constants.true')}}) {
                        $(row).addClass("danger");
                    }
                },
                drawCallback: function (settings) {
                    searchOnEnter(this.api());
                    initDefaultElements();
                    $('#total_amount').html(settings.json.total);
                }
            });

        }
        function exportF(elem) {
        var table = document.getElementById("cashTable");
        var html = table.outerHTML;
        var url = 'data:application/vnd.ms-excel,' + escape(html); // Set your html table into url 
        elem.setAttribute("href", url);
        elem.setAttribute("download", "cash_sales.xls"); // Choose the file name
        return false;
    }

    </script>

@endpush
