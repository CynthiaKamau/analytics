@extends('partials.facility.app')
@push('styles')
    <link rel="stylesheet" type="text/css" href="{{asset('libs/bootstrap-select/dist/css/bootstrap-select.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset("libs/bootstrap-daterangepicker/daterangepicker.css")}}"/>
    @include('partials.global.table-styles')
    <style type="text/css">

        table.dataTable thead th, table.dataTable tbody tr td {
            text-align: center;
        }

        table.dataTable thead .sorting_asc:after {
            content: "";
        }

    </style>
@endpush
@push("content")
	<section class="content-header">
		<h1>
			South Lake Naivasha Analytics
        </h1>
		<ol class="breadcrumb">
			<li>
				<a href="{{route('facility.dashboard')}}">
					<i class="fa fa-dashboard"></i> Dashboard</a>
			</li>
			<li>
				<a href="{{route("facility.visits.list")}}">South Lake Analytics</a>
			</li>
		</ol>
		<div>Lab Payments Raw Data</div>
	</section>
    
    @medyqPermission("can-view-visit-report")
	<section class="content">         
		<div class="box box-success">
            <div class="box-header with-border">
                <div class="row">

                    <div class="col-md-2">
                        <label class="control-label">Clinic</label>
                        <select class="form-control selectpicker" multiple="multiple" id="clinics">
                            @foreach($clinics as $clinic)
                                <option value="{{$clinic->uuid}}">{{$clinic->name}}</option>
                            @endforeach
                        </select>
                    </div>

                    <div class="col-md-2">
                        <label class="control-label">&nbsp;</label>
                        <div class="checkbox">
                            <label style="padding-left: 0;">
                                <input type="checkbox" id="emergency"
                                       value="{{config('constants.true')}}">
                                <span class="cr"><i class="cr-icon fa fa-check"></i></span>
                                Emergency
                            </label>
                        </div>
                    </div>

                    <div class="col-md-4 ">
                        <label>Select date range</label>
                        <input type="text" name="daterange" id="daterange"
                               class="form-control" autocomplete="off" autocapitalize="off"/>
                    </div>

                    <div class="col-md-3">
                        <a id="downloadLink" onclick="exportF(this)" class="btn btn-success pull-right">Export to excel</a>
                    </div>
                </div>
            </div>

			<div class="box-body">
				<div class="row">
                    <div class="col-md-12">

                        <table id="patientListTable" class="table table-hover"> 
                            <thead>
                                <tr>
                                    <th>Invoice No </th>
                                    <th>Patient Name </th>
                                    <th>Clinic </th>
                                    <th>Service </th>
                                    {{-- <th>Payment Mode </th> --}}
                                    <th>Total Amount </th>
                                    <th>Amount Paid </th>
                                    <th>Balance </th>
                                    <th>Date </th>

                                </tr>    
                            </thead>  
                            <tbody>
                            </tbody>    
                        </table> 
                    </div>   
				</div>
			</div>
		</div>
	
    </section>
    @endmedyqPermission

@endpush

@push('footer_scripts')

    <script type="text/javascript" src="{{asset('libs/moment/min/moment.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('libs/datatables/DataTables-1.10.18/js/jquery.dataTables.js')}}"> </script>
    <script type="text/javascript" src="{{asset("libs/bootstrap-daterangepicker/daterangepicker.js")}}"></script>
    <script src="https://cdn.jsdelivr.net/gh/linways/table-to-excel@v1.0.4/dist/tableToExcel.js"></script>

    <script type="text/javascript"
            src="{{asset('libs/bootstrap-select/dist/js/bootstrap-select.min.js')}}"></script>
    @include('partials.global.table-scripts')

    <script type="text/javascript">

        let patientListTable;
        let clinics, isEmergency;
        let dateRange, startDate = '', endDate = '';

        $(function () {
            clinics = $("#clinics");
            clinics.change(function () {
                initTable();
            });

            isEmergency = $("#emergency");
            isEmergency.change(function () {
                initTable();
            });

            initDateRangePicker();
            initTable()

        });

        function initDateRangePicker() {
            dateRange = $("#daterange");
            dateRange.daterangepicker({
                locale: {
                    format: '{{config('constants.js_date_format')}}'
                },
                startDate: "{{ Carbon\Carbon::now()->subMonths(1)->format("d-m-Y")}}",
                endDate: "{{ Carbon\Carbon::now()->format("d-m-Y")}}",
                maxDate: "{{ Carbon\Carbon::yesterday()->format("d-m-Y")}}",
                opens: "left",
                ranges: {
                    "{{trans('app.general.yesterday')}}": [moment().subtract(1, "days"), moment().subtract(1, "days")],
                    "{{trans('app.general.this_month')}}": [moment().startOf("month"), moment().endOf("month")],
                    "{{trans_choice('app.general.last_count_months',1)}}": [moment("{{Carbon\Carbon::now()->subMonths(1)->format("Y-m-d")}}"), moment("{{Carbon\Carbon::now()->format("Y-m-d")}}")],
                    "{{trans_choice('app.general.last_count_months',3)}}": [moment("{{Carbon\Carbon::now()->subMonths(3)->format("Y-m-d")}}"), moment("{{Carbon\Carbon::now()->format("Y-m-d")}}")],
                    "{{trans_choice('app.general.last_count_months',6)}}": [moment("{{Carbon\Carbon::now()->subMonths(6)->format("Y-m-d")}}"), moment("{{Carbon\Carbon::now()->format("Y-m-d")}}")],
                    "{{trans_choice('app.general.last_count_months',12)}}": [moment("{{Carbon\Carbon::now()->subMonths(12)->format("Y-m-d")}}"), moment("{{Carbon\Carbon::now()->format("Y-m-d")}}")],

                }
            }).on("apply.daterangepicker", function (event, picker) {
                startDate = dateRange.data("daterangepicker").startDate.format('{{config('constants.js_date_format')}}');
                endDate = dateRange.data("daterangepicker").endDate.format('{{config('constants.js_date_format')}}');
                initTable();
            });

            startDate = dateRange.data("daterangepicker").startDate.format('{{config('constants.js_date_format')}}');
            endDate = dateRange.data("daterangepicker").endDate.format('{{config('constants.js_date_format')}}');
        }

        function initTable() {

            let tableName = "#patientListTable";
            if ($.fn.dataTable.isDataTable(tableName)) {
                patientListTable.destroy();
            }

            patientListTable = $(tableName).DataTable({
                responsive: false,
                serverSide: true,
                lengthMenu: [[100, 250, 500, 1000, 2500, 10000], [100, 250, 500, 1000, 2500, 10000]],
                cache: false,
                stateSave: false,
                columnDefs: [
                    {
                        bSortable: false,
                        aTargets: [0, 1, 2, 3, 4, 5]
                    },
                ],
                columns: [
                    {data: 'invoice_number'},
                    {data: 'full_name'},
                    {data: 'clinic'},
                    {data: 'service'},
                    // {data: 'payment_mode'},
                    {data: 'total_amount'},
                    {data: 'amount_paid'},
                    {data: 'balance'},
                    {data: 'date_closed'}
                ],
                language: dataTablesTranslations,
                order: [[0, "asc"]],
                ajax: {
                    url: "{{route('analytics.reports.patient.rest')}}",
                    method: 'POST',
                    data: {
                        'clinics': clinics.val(),
                        'is_emergency': isEmergency.is(':checked') ? 0 : 1,
                        start_date: startDate,
                        end_date: endDate,
                    },
                    beforeSend: function (xhr) {
                        showLoadingAnimation();
                    }
                    , complete: function (xhr) {
                        hideLoadingAnimation();
                    },
                    error: function (xhr, error, code) {
                        let message = "";
                        $.each(xhr.responseJSON.messages, function (index, str) {
                            message = message.concat(str).concat("<br/>");
                        });
                        showErrorAlert(message);
                    }
                },
                createdRow: function (row, data, dataIndex) {
                    if (parseInt(data.is_emergency) === 1 ) {
                        $(row).addClass("danger");
                    }
                },
                drawCallback: function (settings) {
                    searchOnEnter(this.api());
                    initDefaultElements();
                }
            });
        }

        function exportF(elem) {
            var table = document.getElementById("patientListTable");
            var html = table.outerHTML;
            var url = 'data:application/vnd.ms-excel,' + escape(html); // Set your html table into url 
            elem.setAttribute("href", url);
            elem.setAttribute("download", "patient_sales.xls"); // Choose the file name
            return false;
        }

    </script>
@endpush    



   