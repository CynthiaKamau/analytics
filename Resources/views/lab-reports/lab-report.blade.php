@extends('partials.facility.app')

@push('styles')
    <link rel="stylesheet" type="text/css" href="{{asset('libs/bootstrap-select/dist/css/bootstrap-select.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset("libs/bootstrap-daterangepicker/daterangepicker.css")}}"/>
    <link rel="stylesheet" type="text/css" href="{{asset('libs/chartjs/Chart.min.css')}}">
    <style>
        canvas {
            -moz-user-select: none;
            -webkit-user-select: none;
            -ms-user-select: none;
        }
    </style>
@endpush

@push("content")

@medyqPermission("can-view-lab-report")
<section class="content" id="tabs" style="background-color: #F9FAFC">
	<div id="tab-container" class="tab-container">
        <h6 class="section-title h2 text-center">Lab Reports</h6>
        <a class="btn btn-success pull-right" href="{{ url('/analytics/lab-test-download')}}"> Export Raw Data </a>
        
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-11">

                    <ul class="nav nav-tabs nav-fill nav-justified" role="tablist">
                        <li class="active nav-item"> <a href="#lab-test-report" data-toggle="tab" role="tab">Lab Test Visits </a></li>
                        <li> <a href="#lab-payment-report" data-toggle="tab" role="tab">Lab Payment Visits </a></li>
                        <li> <a href="#lab-revenue-report" data-toggle="tab" role="tab">Lab Revenue Payment Methods </a></li>
                        <li> <a href="#lab-report" data-toggle="tab" role="tab">Lab Tests Revenue </a></li>

                    </ul>

                    <div class="box-header with-border">
                        <div class="row">
                            <div class="col-md-3">
                                <label class="control-label">Clinic</label>
                                <select class="form-control selectpicker" multiple="multiple" id="clinics">
                                    @foreach($clinics as $clinic)
                                        <option value="{{$clinic->uuid}}">{{$clinic->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="col-md-3">
                                <label class="control-label">Payment Methods</label>
                                <select class="form-control selectpicker" multiple="multiple" id="payment_modes">
                                    @foreach($payment_modes as $payment_mode)
                                        <option value="{{$payment_mode->uuid}}">{{$payment_mode->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="col-md-2">
                                <label class="control-label">&nbsp;</label>
                                <div class="checkbox">
                                    <label style="padding-left: 0;">
                                        <input type="checkbox" id="emergency"
                                            value="{{config('constants.true')}}">
                                        <span class="cr"><i class="cr-icon fa fa-check"></i></span>
                                        Emergency
                                    </label>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <label>Select date range</label>
                                <input type="text" name="daterange" id="daterange"
                                    class="form-control" autocomplete="off" autocapitalize="off"/>
                            </div>    

                        </div>
                    </div>

                    <div class="tab-content py-3 px-3 px-sm-0" >
                        
                        <!-- start lab test report -->
                        <div class="tab-pane active" id="lab-test-report">                            
                            <div class="pull-right">
                                {{-- <button class="btn btn-success" onclick="downloadAsExcel()"> Export Exel</button>  --}}
                                <button class="btn btn-success" id="save-lab-test-chart"> Export PDF</button>

                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <canvas id="labTestChart"></canvas>
                                </div>
                            </div>
                        </div> 

                        <!-- start lab report -->
                        <div class="tab-pane" id="lab-report">
                            <div class="pull-right">
                                {{-- <button class="btn btn-success" onclick="exportReportrToExcel(this)" > Export Exel</button>  --}}
                                <button class="btn btn-success" id="save-lab-chart"id=""> Export PDF</button>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <canvas id="labChart" style="position: relative; height:40vh, width:80vh"></canvas>
                                </div>
                            </div>
                        </div>
                        
                        <!-- start lab payment report -->
                        <div class="tab-pane" id="lab-payment-report">
                            <div class="pull-right">
                                {{-- <button class="btn btn-success" id=""> Export Exel</button>  --}}
                                <button class="btn btn-success" id="save-lab-payments"> Export PDF</button>
                            </div>

                            <div class="row">
                                <div class="col-md-12">
                                    <canvas id="labPaymentChart"></canvas>
                                </div>
                            </div>
                        </div>    

                        <!-- start lab revenue report -->
                        <div class="tab-pane" id="lab-revenue-report">
                            <div class="pull-right">
                                {{-- <button class="btn btn-success" id=""> Export Exel</button>  --}}
                                <button class="btn btn-success" id="save-lab-revenue"> Export PDF</button>
                            </div>
                            <div class="row" >
                                <div class="col-md-12">
                                    <canvas id="labRevenueChart"></canvas>
                                </div>
                            </div>
                        </div>  

                    </div>  
                
                </div> 
            </div>          
    
    </div> 
</section>
@endmedyqPermission


@endpush

@push("footer_scripts")

    <script type="text/javascript" src="{{asset('libs/bootstrap-select/dist/js/bootstrap-select.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('libs/moment/min/moment.min.js')}}"></script>
    <script type="text/javascript" src="{{asset("libs/bootstrap-daterangepicker/daterangepicker.js")}}"></script>
    <script type="text/javascript" src="{{asset('libs/chartjs/Chart.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('libs/chartjs/plugins/chartjs-plugin-zoom.min.js')}}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jspdf/1.3.3/jspdf.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/xlsx/0.16.6/xlsx.full.min.js"></script>
    <script src="https://cdn.bootcdn.net/ajax/libs/FileSaver.js/2.0.2/FileSaver.min.js"></script>

    @include('partials.global.table-scripts')

    <script type="text/javascript">

        let labChartContext = document.getElementById('labChart');
        let labTestChartContext = document.getElementById('labTestChart');
        let labPaymentsChartContext = document.getElementById('labPaymentChart');
        let labRevenueChartContext = document.getElementById('labRevenueChart');

        let clinics, payment_modes, isEmergency;
        let dateRange, startDate = '', endDate = '';

        let config = {
            type: 'bar',
            data: {
                labels: [],
                datasets: []
            },
            options: {
                responsive: true,
                maintainAspectRatio: false,
                title: {
                    display: true,
                    text: 'Total Number of Lab Tests Against Income Generated'
                },
                tooltips: {
                    mode: 'index',
                    intersect: false,
                },
                hover: {
                    mode: 'nearest',
                    intersect: true
                },
                scales: {
                    xAxes: [{
                        display: true,
                        scaleLabel: {
                            display: true,
                            labelString: 'Lab Tests'
                        }
                    }],
                    yAxes: [{
                        display: true,
                        ticks: {
                                beginAtZero:true
                        },
                        scaleLabel: {
                            display: true,
                            labelString: 'Income Generated'
                        }
                    }],
                },
            }
        };

        let config1 = {
            type: 'bar',
            data: {
                labels: [],
                datasets: []
            },
            options: {
                responsive: true,
                maintainAspectRatio: false,
                title: {
                    display: true,
                    text: 'Total Number of Lab Tests Against Lab Test Names'
                },
                tooltips: {
                    mode: 'index',
                    intersect: false,
                },
                hover: {
                    mode: 'nearest',
                    intersect: true
                },
                scales: {
                    xAxes: [{
                        display: true,
                        scaleLabel: {
                            display: true,
                            labelString: 'Lab Tests'
                        }
                    }],
                    yAxes: [{
                        display: true,
                        ticks: {
                                beginAtZero:true
                        },
                        scaleLabel: {
                            display: true,
                            labelString: 'Number of Tests'
                        }
                    }],
                },
            }
        };

        let config2 = {
            type: 'bar',
            data: {
                labels: [],
                datasets: []
            },
            options: {
                responsive: true,
                maintainAspectRatio: false,
                title: {
                    display: true,
                    text: 'Total Number of Lab Tests Against Payment Methods'
                },
                tooltips: {
                    mode: 'index',
                    intersect: false,
                },
                hover: {
                    mode: 'nearest',
                    intersect: true
                },
                scales: {
                    xAxes: [{
                        display: true,
                        scaleLabel: {
                            display: true,
                            labelString: 'Payment Modes'
                        }
                    }],
                    yAxes: [{
                        display: true,
                        ticks: {
                                beginAtZero:true
                        },
                        scaleLabel: {
                            display: true,
                            labelString: 'Number of Tests'
                        }
                    }],
                },
            }
        };

        let config3 = {
            type: 'bar',
            data: {
                labels: [],
                datasets: []
            },
            options: {
                responsive: true,
                maintainAspectRatio: false,
                title: {
                    display: true,
                    text: 'Total Income Against Payment Methods'
                },
                tooltips: {
                    mode: 'index',
                    intersect: false,
                },
                hover: {
                    mode: 'nearest',
                    intersect: true
                },
                scales: {
                    xAxes: [{
                        display: true,
                        scaleLabel: {
                            display: true,
                            labelString: 'Payment Modes'
                        }
                    }],
                    yAxes: [{
                        display: true,
                        ticks: {
                                beginAtZero:true
                        },
                        scaleLabel: {
                            display: true,
                            labelString: 'Income Generated'
                        }
                    }],
                },
            }
        };

        $(function () {
            clinics = $("#clinics");
            clinics.change(function () {
                initChart();
                initChart1();
                initChart2();
                initChart3();
            });

            payment_modes = $("#payment_modes");
            payment_modes.change(function () {
                initChart();
                initChart1();
                initChart2();
                initChart3();
            });

            isEmergency = $("#emergency");
            isEmergency.change(function () {
                initChart();
                initChart1();
                initChart2();
                initChart3();

            });

            initDateRangePicker();

            initChart();
            initChart1();
            initChart2();
            initChart3();

        });

        function initChart() {

            request({
                url: '{{route('analytics.reports.lab_rest')}}',
                data: {
                    clinics: clinics.val(),
                    payment_modes: payment_modes.val(),
                    is_emergency: isEmergency.is(':checked') ? 1 : 0,
                    start_date: startDate,
                    end_date: endDate,
                },
                beforeSend: function () {
                    showLoadingAnimation();
                }
                , complete: function (xhr) {
                    hideLoadingAnimation();
                },
                success: function (response) {

                    config.data.labels = response.labels;
                    config.data.datasets = response.datasets;

                   let chart =  new Chart(labChartContext, config);

                    chart.canvas.parentNode.style.height = '400px';
                    chart.canvas.parentNode.style.width = '850px';

                    Chart.plugins.register({
                        afterRender: function(c) {
                            var ctx = c.chart.ctx;
                            ctx.save();

                            ctx.globalCompositeOperation = 'destination-over';
                            ctx.fillStyle = 'white';
                            ctx.fillRect(0, 0, c.chart.width, c.chart.height);
                            ctx.restore();
                        }
                    });

                },
                error: function (xhr) {

                    let message = "";

                    $.each(xhr.responseJSON.messages, function (index, str) {
                        message = message.concat(str).concat('\n');
                    });

                    Swal.fire({
                        title: '{{trans('app.general.request_error')}}',
                        text: message,
                        type: 'error',
                    });
                }
            });

        }

        function initChart1() {

            request({
                url: '{{route('analytics.reports.lab_test_rest')}}',
                data: {
                    clinics: clinics.val(),
                    payment_modes: payment_modes.val(),
                    is_emergency: isEmergency.is(':checked') ? 1 :0,
                    start_date: startDate,
                    end_date: endDate,
                },
                beforeSend: function () {
                    showLoadingAnimation();
                }
                , complete: function (xhr) {
                    hideLoadingAnimation();
                },
                success: function (response) {

                    config1.data.labels = response.labels;
                    config1.data.datasets = response.datasets;

                let chart =  new Chart(labTestChartContext, config1);

                    chart.canvas.parentNode.style.height = '400px';
                    chart.canvas.parentNode.style.width = '800px';

                    Chart.plugins.register({
                        afterRender: function(c) {
                            var ctx = c.chart.ctx;
                            ctx.save();

                            ctx.globalCompositeOperation = 'destination-over';
                            ctx.fillStyle = 'white';
                            ctx.fillRect(0, 0, c.chart.width, c.chart.height);
                            ctx.restore();
                        }
                    });

                },
                error: function (xhr) {

                    let message = "";

                    $.each(xhr.responseJSON.messages, function (index, str) {
                        message = message.concat(str).concat('\n');
                    });

                    Swal.fire({
                        title: '{{trans('app.general.request_error')}}',
                        text: message,
                        type: 'error',
                    });
                }
            });

        }

        function initChart2() {

            request({
                url: '{{route('analytics.reports.lab_payments_rest')}}',
                data: {
                    clinics: clinics.val(),
                    payment_modes: payment_modes.val(),
                    is_emergency: isEmergency.is(':checked') ? 1 : 0,
                    start_date: startDate,
                    end_date: endDate,
                },
                beforeSend: function () {
                    showLoadingAnimation();
                }
                , complete: function (xhr) {
                    hideLoadingAnimation();
                },
                success: function (response) {

                    config2.data.labels = response.labels;
                    config2.data.datasets = response.datasets;

                let chart =  new Chart(labPaymentsChartContext, config2);

                    chart.canvas.parentNode.style.height = '400px';
                    chart.canvas.parentNode.style.width = '800px';

                    Chart.plugins.register({
                        afterRender: function(c) {
                            var ctx = c.chart.ctx;
                            ctx.save();

                            ctx.globalCompositeOperation = 'destination-over';
                            ctx.fillStyle = 'white';
                            ctx.fillRect(0, 0, c.chart.width, c.chart.height);
                            ctx.restore();
                        }
                    });

                },
                error: function (xhr) {

                    let message = "";

                    $.each(xhr.responseJSON.messages, function (index, str) {
                        message = message.concat(str).concat('\n');
                    });

                    Swal.fire({
                        title: '{{trans('app.general.request_error')}}',
                        text: message,
                        type: 'error',
                    });
                }
            });

        }

        function initChart3() {
            request({
                url: '{{route('analytics.reports.lab_revenue_rest')}}',
                data: {
                    clinics: clinics.val(),
                    payment_modes: payment_modes.val(),
                    is_emergency: isEmergency.is(':checked') ? 1 :0,
                    start_date: startDate,
                    end_date: endDate,
                },
                beforeSend: function () {
                    showLoadingAnimation();
                }
                , complete: function (xhr) {
                    hideLoadingAnimation();
                },
                success: function (response) {

                    config3.data.labels = response.labels;
                    config3.data.datasets = response.datasets;

                   let chart =  new Chart(labRevenueChartContext, config3);

                    chart.canvas.parentNode.style.height = '400px';
                    chart.canvas.parentNode.style.width = '800px';

                    Chart.plugins.register({
                        afterRender: function(c) {
                            var ctx = c.chart.ctx;
                            ctx.save();

                            ctx.globalCompositeOperation = 'destination-over';
                            ctx.fillStyle = 'white';
                            ctx.fillRect(0, 0, c.chart.width, c.chart.height);
                            ctx.restore();
                        }
                    });
                    

                },
                error: function (xhr) {

                    let message = "";

                    $.each(xhr.responseJSON.messages, function (index, str) {
                        message = message.concat(str).concat('\n');
                    });

                    Swal.fire({
                        title: '{{trans('app.general.request_error')}}',
                        text: message,
                        type: 'error',
                    });
                }
            });

        }

        function initDateRangePicker() {
            dateRange = $("#daterange");
            dateRange.daterangepicker({
                locale: {
                    format: '{{config('constants.js_date_format')}}'
                },
                startDate: "{{ Carbon\Carbon::now()->subMonths(1)->format("d-m-Y")}}",
                endDate: "{{ Carbon\Carbon::now()->format("d-m-Y")}}",
                maxDate: "{{ Carbon\Carbon::yesterday()->format("d-m-Y")}}",
                opens: "left",
                ranges: {
                    "{{trans('app.general.yesterday')}}": [moment().subtract(1, "days"), moment().subtract(1, "days")],
                    "{{trans('app.general.this_month')}}": [moment().startOf("month"), moment().endOf("month")],
                    "{{trans_choice('app.general.last_count_months',1)}}": [moment("{{Carbon\Carbon::now()->subMonths(1)->format("Y-m-d")}}"), moment("{{Carbon\Carbon::now()->format("Y-m-d")}}")],
                    "{{trans_choice('app.general.last_count_months',3)}}": [moment("{{Carbon\Carbon::now()->subMonths(3)->format("Y-m-d")}}"), moment("{{Carbon\Carbon::now()->format("Y-m-d")}}")],
                    "{{trans_choice('app.general.last_count_months',6)}}": [moment("{{Carbon\Carbon::now()->subMonths(6)->format("Y-m-d")}}"), moment("{{Carbon\Carbon::now()->format("Y-m-d")}}")],
                    "{{trans_choice('app.general.last_count_months',12)}}": [moment("{{Carbon\Carbon::now()->subMonths(12)->format("Y-m-d")}}"), moment("{{Carbon\Carbon::now()->format("Y-m-d")}}")],

                }
            }).on("apply.daterangepicker", function (event, picker) {
                startDate = dateRange.data("daterangepicker").startDate.format('{{config('constants.js_date_format')}}');
                endDate = dateRange.data("daterangepicker").endDate.format('{{config('constants.js_date_format')}}');
                initChart();
                initChart1();
                initChart2();
                initChart3();
            });

            startDate = dateRange.data("daterangepicker").startDate.format('{{config('constants.js_date_format')}}');
            endDate = dateRange.data("daterangepicker").endDate.format('{{config('constants.js_date_format')}}');
        }

        document.getElementById('save-lab-test-chart').addEventListener("click", downloadLTC);
        document.getElementById('save-lab-chart').addEventListener("click", downloadLC);
        document.getElementById('save-lab-payments').addEventListener("click", downloadLPC);
        document.getElementById('save-lab-revenue').addEventListener("click", downloadLRC);
        
        function downloadLTC() {
            var imgData = document.getElementById("labTestChart").toDataURL("image/jpeg", 1.0);
            var pdf = new jsPDF();

            pdf.addImage(imgData, 'JPEG', 0,0);
            pdf.save("LabTestRevenue.pdf");
        };

        function downloadLC() {
            var imgData1 = document.getElementById("labChart").toDataURL("image/jpeg", 1.0);

            var pdf = new jsPDF();

            pdf.addImage(imgData1, 'JPEG', 0,0);
            pdf.save("LabVisits.pdf");
        };

        function downloadLPC() {
            var imgData2 = document.getElementById("labPaymentChart").toDataURL("image/jpeg", 1.0);
            var pdf = new jsPDF();

            pdf.addImage(imgData2, 'JPEG', 0,0);
            pdf.save("LabPayments.pdf");
        };

        function downloadLRC() {
            var imgData3 = document.getElementById("labRevenueChart").toDataURL("image/jpeg", 1.0);
            var pdf = new jsPDF();

            pdf.addImage(imgData3, 'JPEG', 0,0);
            pdf.save("LabRevenue.pdf");
        };

    </script>

@endpush
