<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.0.0-alpha1/jquery.min.js"> </script>

<script>

	var url = "{{url('/analytics/clinic-rest') }}";
    var Clinics = new Array();

    $(document).ready(function() {
        $.get(url, function(response) {

            console.log(response);

            const xlabels = Object.keys(response)
			const ylabels = Object.values(response)

            const ctx = document.getElementById("clinicChart").getContext('2d');
            const myChart = new Chart(ctx, {
                type: 'bar',
                data: {
					labels: xlabels,
					    datasets: [{
                            backgroundColor: '#79D1CF',
                            borderColor: "##79D1CF",
							label: 'Patient Visits',
							data: ylabels,
							borderWidth: 1
						}]
                },
                options: {
                    scales: {
                        yAxes: [{
                            ticks: {
                                beginAtZero:true
                            }
                        }],

                    }
                }
            })
        })
    })
</script>

