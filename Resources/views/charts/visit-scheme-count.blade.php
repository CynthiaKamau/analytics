<script type="text/javascript">

    let wrChartContext = $('#wrChart');
    let visitChartContext = $('#allvisitChart');
    let dateRange, startDate = '', endDate = '';
    let ailClass;
    let config = {
        type: 'bar',
        data: {
            labels: [],
            datasets: []
        },
        options: {
            responsive: true,
            maintainAspectRatio: false,

            tooltips: {
                mode: 'index',
                intersect: false,
            },
            hover: {
                mode: 'nearest',
                intersect: true
            },
            scales: {
                xAxes: [{
                    display: true,
                    scaleLabel: {
                        display: true,
                        labelString: 'Scheme'
                    }
                }],
                yAxes: [{
                    display: true,
                    scaleLabel: {
                        display: true,
                        labelString: 'Patient Count'
                    },
                    ticks: {
                        beginAtZero: true
                    }
                }]
            },
        }
    };
    let configvis = {
        type: 'bar',
        data: {
            labels: [],
            datasets: []
        },
        options: {
            responsive: true,
            maintainAspectRatio: false,

            tooltips: {
                mode: 'index',
                intersect: false,
            },
            hover: {
                mode: 'nearest',
                intersect: true
            },
            scales: {
                xAxes: [{
                    display: true,
                    scaleLabel: {
                        display: true,
                        labelString: 'Schemes'
                    }
                }],
                yAxes: [{
                    display: true,
                    scaleLabel: {
                        display: true,
                        labelString: 'Patient Count'
                    },
                    ticks: {
                        beginAtZero: true
                    }
                }]
            },
        }
    };

    let visitsChartContext = $('#visitChart');
    let schemes;
    let clinic;
    let config2 = {
        type: 'bar',
        data: {
            labels: [],
            datasets: []
        },
        options: {
            responsive: true,
            maintainAspectRatio: false,

            tooltips: {
                mode: 'index',
                intersect: false,
            },
            hover: {
                mode: 'nearest',
                intersect: true
            },
            scales: {
                xAxes: [{
                    display: true,
                    scaleLabel: {
                        display: true,
                        labelString: 'Illness type'
                    }
                }],
                yAxes: [{
                    display: true,
                    scaleLabel: {
                        display: true,
                        labelString: 'Patient Count'
                    },
                    ticks: {
                        beginAtZero: true
                    }
                }]
            },
        }
    };

    $(function () {
        setTimeout(function () {
            ailClass = $("#scheme_filter");
            ailClass.change(function () {
                initChart();
            });
            initChart();
        }, 500);

        setTimeout(function () {
            schemes = $("#wr_filter");
            schemes.change(function () {
                initChartWr();
            });
            initChartWr();
        }, 500);

        setTimeout(function () {
            clinic = $("#clinic_filter");
            clinic.change(function () {
                initChartVisit();
            });
            initChartVisit();
        }, 500);

        initDateRangePicker();

    });

    function initChart() {

        request({
            url: '{{route('analytics.reports.visits_report')}}',
            data: {
                scheme: ailClass.val(),
                start_date: startDate,
                end_date: endDate,
            },
            beforeSend: function () {
                showLoadingAnimation();
            }
            , complete: function (xhr) {
                hideLoadingAnimation();
            },
            success: function (response) {

                config.data.labels = response[0].labels;
                config.data.datasets = response[0].datasets;

                let chart = new Chart(visitsChartContext, config);

                chart.canvas.parentNode.style.height = '400px';

            },
            error: function (xhr, status, errorMessage) {

                let message = "";

                $.each(xhr.responseJSON.messages, function (index, str) {
                    message = message.concat(str).concat('\n');
                });

                Swal.fire({
                    title: '{{trans('app.general.request_error')}}',
                    text: message,
                    type: 'error',
                });
            }
        });
    }

    function initChartWr() {

        request({
            url: '{{route('analytics.reports.work-related-illness-rest')}}',
            data: {
                scheme: schemes.val(),
                start_date: startDate,
                end_date: endDate,
            },
            beforeSend: function () {
                showLoadingAnimation();
            }
            , complete: function (xhr) {
                hideLoadingAnimation();
            },
            success: function (response) {

                config2.data.labels = response.labels;
                config2.data.datasets = response.datasets;

                let chart = new Chart(wrChartContext, config2);

                chart.canvas.parentNode.style.height = '400px';
                //chart.canvas.parentNode.style.width = '128px';

            },
            error: function (xhr, status, errorMessage) {

                let message = "";

                $.each(xhr.responseJSON.messages, function (index, str) {
                    message = message.concat(str).concat('\n');
                });

                Swal.fire({
                    title: '{{trans('app.general.request_error')}}',
                    text: message,
                    type: 'error',
                });
            }
        });
    }

    function initChartVisit() {

        request({
            url: '{{route('analytics.reports.all-visits-rest')}}',
            data: {
                clinic: clinic.val(),
                start_date: startDate,
                end_date: endDate,
            },
            beforeSend: function () {
                showLoadingAnimation();
            }
            , complete: function (xhr) {
                hideLoadingAnimation();
            },
            success: function (response) {
                configvis.data.labels = response.labels;
                configvis.data.datasets = response.datasets;

                let chart = new Chart(visitChartContext, configvis);

                chart.canvas.parentNode.style.height = '400px';
                //chart.canvas.parentNode.style.width = '128px';

            },
            error: function (xhr, status, errorMessage) {

                let message = "";

                $.each(xhr.responseJSON.messages, function (index, str) {
                    message = message.concat(str).concat('\n');
                });

                Swal.fire({
                    title: '{{trans('app.general.request_error')}}',
                    text: message,
                    type: 'error',
                });
            }
        });
    }


    function initDateRangePicker() {
        dateRange = $("#daterange");
        dateRange.daterangepicker({
            locale: {
                format: '{{config('constants.js_date_format')}}'
            },
            startDate: "{{ Carbon\Carbon::now()->subMonths(1)->format("d-m-Y")}}",
            endDate: "{{ Carbon\Carbon::now()->format("d-m-Y")}}",
            maxDate: "{{ Carbon\Carbon::now()->format("d-m-Y")}}",
            opens: "left",
            ranges: {
                "{{trans('app.general.yesterday')}}": [moment().subtract(1, "days"), moment().subtract(1, "days")],
                "{{trans('app.general.this_month')}}": [moment().startOf("month"), moment().endOf("month")],
                "{{trans_choice('app.general.last_count_months',1)}}": [moment("{{Carbon\Carbon::now()->subMonths(1)->format("Y-m-d")}}"), moment("{{Carbon\Carbon::now()->format("Y-m-d")}}")],
                "{{trans_choice('app.general.last_count_months',3)}}": [moment("{{Carbon\Carbon::now()->subMonths(3)->format("Y-m-d")}}"), moment("{{Carbon\Carbon::now()->format("Y-m-d")}}")],
                "{{trans_choice('app.general.last_count_months',6)}}": [moment("{{Carbon\Carbon::now()->subMonths(6)->format("Y-m-d")}}"), moment("{{Carbon\Carbon::now()->format("Y-m-d")}}")],
                "{{trans_choice('app.general.last_count_months',12)}}": [moment("{{Carbon\Carbon::now()->subMonths(12)->format("Y-m-d")}}"), moment("{{Carbon\Carbon::now()->format("Y-m-d")}}")],

            }
        }).on("apply.daterangepicker", function (event, picker) {
            startDate = dateRange.data("daterangepicker").startDate.format('{{config('constants.js_date_format')}}');
            endDate = dateRange.data("daterangepicker").endDate.format('{{config('constants.js_date_format')}}');
            initChart();
            initChartVisit();
            initChartWr();
        });

        startDate = dateRange.data("daterangepicker").startDate.format('{{config('constants.js_date_format')}}');
        endDate = dateRange.data("daterangepicker").endDate.format('{{config('constants.js_date_format')}}');
    }

    function exportReportToExcel() {
        table = $('#scheme-diagnosis-table').DataTable({
            "ordering": false,
            "ajax": {
                url: "{{route('analytics.reports.ail-category-rest')}}",
                data: {
                    scheme: ailClass.val(),
                    start_date: startDate,
                    end_date: endDate,
                },
            }
        });

        setTimeout(function () {
            let table = document.getElementById("scheme-diagnosis-table");
            TableToExcel.convert(table, {
                name: `export ${new Date().getFullYear()}-${(new Date().getMonth() + 1)}-${new Date().getDate()}.xlsx`,
                sheet: {
                    name: 'Sheet 1'
                }
            });
        }, 1000);
        table.destroy();
    }

    function exportReport2ToExcel() {
        table = $('#example').DataTable({
            "ordering": false,
            "ajax": {
                url: "{{route('analytics.reports.work-related-illness')}}",
                data: {
                    scheme: schemes.val(),
                    start_date: startDate,
                    end_date: endDate,
                }
            }
        });

        setTimeout(function () {
            let table = document.getElementById("wrtable");
            TableToExcel.convert(table, {
                name: `export ${new Date().getFullYear()}-${(new Date().getMonth() + 1)}-${new Date().getDate()}.xlsx`,
                sheet: {
                    name: 'Sheet 1'
                }
            });
        }, 1000);
        table.destroy();

    }

    function saveAsPDF() {
        html2canvas(document.getElementById("chart-container"), {
            onrendered: function (canvas) {
                var img = canvas.toDataURL();
                var doc = new jsPDF();
                doc.addImage(img, 10, 10);
                doc.save(`chart ${new Date().getFullYear()}-${(new Date().getMonth() + 1)}-${new Date().getDate()}.pdf`);
            }
        });
    }

    function saveAsPDFwr() {
        html2canvas(document.getElementById("chartwrcontainer"), {
            onrendered: function (canvas) {
                var img = canvas.toDataURL();
                var doc = new jsPDF();
                doc.addImage(img, 10, 10);
                doc.save(`chart ${new Date().getFullYear()}-${(new Date().getMonth() + 1)}-${new Date().getDate()}.pdf`);
            }
        });
    }
</script>

