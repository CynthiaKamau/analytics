@extends('partials.facility.app')
@push('styles')
    <link rel="stylesheet" type="text/css" href="{{asset('libs/bootstrap-select/dist/css/bootstrap-select.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset("libs/bootstrap-daterangepicker/daterangepicker.css")}}"/>
    <style>
        canvas {
            -moz-user-select: none;
            -webkit-user-select: none;
            -ms-user-select: none;
        }
    </style>
@endpush
@push("content")

    <section class="content-header">
        <h1>
            MOH 705B
        </h1>
        <ol class="breadcrumb">
            <li>
                <a href="{{route('facility.dashboard')}}">
                    <i class="fa fa-dashboard"></i> Dashboard</a>
            </li>
            <li>
                <a href="{{route("facility.visits.list")}}">Analytics</a>
            </li>
        </ol>
        <div></div>
    </section>

    @medyqPermission("can-view-visit-report")
    <section class="content">
        <?php

        $pay = array();

        ?>
        <div class="row">
            <div class="col-md-12">
                <div class="box box-widget">
                    <div class="box-header with-border">
                        <div class="title-block">
                            <span class="title">
                                MOH 705B
                            </span>
                            <span class="description">
                            </span>
                        </div>
                    </div>

                    <div class="box-body">
                        <br>
                        <table id="scheme-diagnosis-table" class="table table-striped" border="1">
                            <thead>
                            <tr>
                                <th rowspan="2" colspan="2" style="text-align: center">DISEASES (New cases only)</th>
                                <th colspan="32" style="text-align: center">Day of Month</th>
                                <th></th>
                            </tr>
                            <tr>
                                <?php
                                $numbers = range(1, 31);
                                foreach ($numbers as $num) {
                                    echo "<th>$num</th>";
                                }
                                ?>
                                <th>TOTALS</th>
                                <th>#</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <td>1</td>
                                <td>Diarrhoea</td>
                                <?php
                                $numbers = range(1, 32);
                                foreach ($numbers as $num) {
                                    echo "<td>0</td>";
                                }
                                ?>
                                <th>1</th>
                            </tr>
                            <tr>
                                <td>2</td>
                                <td>Tuberculosis</td>
                                <?php
                                $numbers = range(1, 32);
                                foreach ($numbers as $num) {
                                    if (isset($Tuberculosis[$num])) {
                                        echo "<td>$Tuberculosis[$num]</td>";
                                    } elseif ($num == 32) {
                                        $sum = 0;
                                        foreach ($Tuberculosis as $receipt) {
                                            $sum += $receipt;
                                        }
                                        echo "<td>$sum</td>";
                                    } else {
                                        echo "<td>0</td>";
                                    }
                                }
                                ?>

                                <th>2</th>
                            </tr>
                            <tr>
                                <td>3</td>
                                <td>Dysentery</td>
                                <?php
                                $numbers = range(1, 32);
                                foreach ($numbers as $num) {
                                    if (isset($Dysentery[$num])) {
                                        echo "<td>$Dysentery[$num]</td>";
                                    } elseif ($num == 32) {
                                        $sum = 0;
                                        foreach ($Dysentery as $receipt) {
                                            $sum += $receipt;
                                        }
                                        echo "<td>$sum</td>";
                                    } else {
                                        echo "<td>0</td>";
                                    }
                                }
                                ?>

                                <th>3</th>
                            </tr>
                            <tr>
                                <td>4</td>
                                <td>Cholera</td>
                                <?php
                                $numbers = range(1, 32);
                                foreach ($numbers as $num) {
                                    if (isset($cholera[$num])) {
                                        echo "<td>$cholera[$num]</td>";
                                    } elseif ($num == 32) {
                                        $sum = 0;
                                        foreach ($cholera as $receipt) {
                                            $sum += $receipt;
                                        }
                                        echo "<td>$sum</td>";
                                    } else {
                                        echo "<td>0</td>";
                                    }
                                }
                                ?>

                                <th>4</th>
                            </tr>
                            <tr>
                                <td>5</td>
                                <td>Meningococcal meningitis</td>
                                <?php
                                $numbers = range(1, 32);
                                foreach ($numbers as $num) {
                                    if (isset($meningitis[$num])) {
                                        echo "<td>$meningitis[$num]</td>";
                                    } elseif ($num == 32) {
                                        $sum = 0;
                                        foreach ($meningitis as $receipt) {
                                            $sum += $receipt;
                                        }
                                        echo "<td>$sum</td>";
                                    } else {
                                        echo "<td>0</td>";
                                    }
                                }
                                ?>

                                <th>5</th>
                            </tr>
                            <tr>
                                <td>6</td>
                                <td>Tetanus</td>
                                <?php
                                $numbers = range(1, 32);
                                foreach ($numbers as $num) {
                                    if (isset($Tetanus[$num])) {
                                        echo "<td>$Tetanus[$num]</td>";
                                    } elseif ($num == 32) {
                                        $sum = 0;
                                        foreach ($Tetanus as $receipt) {
                                            $sum += $receipt;
                                        }
                                        echo "<td>$sum</td>";
                                    } else {
                                        echo "<td>0</td>";
                                    }
                                }
                                ?>

                                <th>6</th>
                            </tr>
                            <tr>
                                <td>7</td>
                                <td>Poliomyelitis</td>
                                <?php
                                $numbers = range(1, 32);
                                foreach ($numbers as $num) {
                                    if (isset($Polio[$num])) {
                                        echo "<td>$Polio[$num]</td>";
                                    } elseif ($num == 32) {
                                        $sum = 0;
                                        foreach ($Polio as $receipt) {
                                            $sum += $receipt;
                                        }
                                        echo "<td>$sum</td>";
                                    } else {
                                        echo "<td>0</td>";
                                    }
                                }
                                ?>

                                <th>7</th>
                            </tr>
                            <tr>
                                <td>8</td>
                                <td>Chicken pox</td>
                                <?php
                                $numbers = range(1, 32);
                                foreach ($numbers as $num) {
                                    if (isset($Chic[$num])) {
                                        echo "<td>$Chic[$num]</td>";
                                    } elseif ($num == 32) {
                                        $sum = 0;
                                        foreach ($Chic as $receipt) {
                                            $sum += $receipt;
                                        }
                                        echo "<td>$sum</td>";
                                    } else {
                                        echo "<td>0</td>";
                                    }
                                }
                                ?>
                                <th>8</th>
                            </tr>
                            <tr>
                                <td>9</td>
                                <td>Measles</td>
                                <?php
                                $numbers = range(1, 32);
                                foreach ($numbers as $num) {
                                    if (isset($Measles[$num])) {
                                        echo "<td>$Measles[$num]</td>";
                                    } elseif ($num == 32) {
                                        $sum = 0;
                                        foreach ($Measles as $receipt) {
                                            $sum += $receipt;
                                        }
                                        echo "<td>$sum</td>";
                                    } else {
                                        echo "<td>0</td>";
                                    }
                                }
                                ?>

                                <th>9</th>
                            </tr>
                            <tr>
                                <td>10</td>
                                <td>Infectious Hepatitis</td>
                                <?php
                                $numbers = range(1, 32);
                                foreach ($numbers as $num) {
                                    if (isset($Hepatitis[$num])) {
                                        echo "<td>$Hepatitis[$num]</td>";
                                    } elseif ($num == 32) {
                                        $sum = 0;
                                        foreach ($Hepatitis as $receipt) {
                                            $sum += $receipt;
                                        }
                                        echo "<td>$sum</td>";
                                    } else {
                                        echo "<td>0</td>";
                                    }
                                }
                                ?>

                                <th>10</th>
                            </tr>
                            <tr>
                                <td>11</td>
                                <td>Mumps</td>
                                <?php
                                $numbers = range(1, 32);
                                foreach ($numbers as $num) {
                                    if (isset($Mumps[$num])) {
                                        echo "<td>$Mumps[$num]</td>";
                                    } elseif ($num == 32) {
                                        $sum = 0;
                                        foreach ($Mumps as $receipt) {
                                            $sum += $receipt;
                                        }
                                        echo "<td>$sum</td>";
                                    } else {
                                        echo "<td>0</td>";
                                    }
                                }
                                ?>

                                <th>11</th>
                            </tr>
                            <tr>
                                <td>12</td>
                                <td>Clinical Malaria</td>
                                <?php
                                $numbers = range(1, 32);
                                foreach ($numbers as $num) {
                                    if (isset($CMalaria[$num])) {
                                        echo "<td>$CMalaria[$num]</td>";
                                    } elseif ($num == 32) {
                                        $sum = 0;
                                        foreach ($CMalaria as $receipt) {
                                            $sum += $receipt;
                                        }
                                        echo "<td>$sum</td>";
                                    } else {
                                        echo "<td>0</td>";
                                    }
                                }
                                ?>

                                <th>12</th>
                            </tr>
                            <tr>
                                <td>13</td>
                                <td>Confirmed Malaria</td>
                                <?php
                                $numbers = range(1, 32);
                                foreach ($numbers as $num) {
                                    if (isset($FMalaria[$num])) {
                                        echo "<td>$FMalaria[$num]</td>";
                                    } elseif ($num == 32) {
                                        $sum = 0;
                                        foreach ($FMalaria as $receipt) {
                                            $sum += $receipt;
                                        }
                                        echo "<td>$sum</td>";
                                    } else {
                                        echo "<td>0</td>";
                                    }
                                }
                                ?>

                                <th>13</th>
                            </tr>
                            <tr>
                                <td>14</td>
                                <td>Urinary Tract Infection</td>
                                <?php
                                $numbers = range(1, 32);
                                foreach ($numbers as $num) {
                                    if (isset($Typhoid[$num])) {
                                        echo "<td>$Typhoid[$num]</td>";
                                    } elseif ($num == 32) {
                                        $sum = 0;
                                        foreach ($Typhoid as $receipt) {
                                            $sum += $receipt;
                                        }
                                        echo "<td>$sum</td>";
                                    } else {
                                        echo "<td>0</td>";
                                    }
                                }
                                ?>

                                <th>14</th>
                            </tr>
                            <tr>
                                <td>15</td>
                                <td>Typhoid fever</td>
                                <?php
                                $numbers = range(1, 32);
                                foreach ($numbers as $num) {
                                    echo "<td>0</td>";
                                }
                                ?>

                                <th>15</th>
                            </tr>
                            <tr>
                                <td>16</td>
                                <td>Bilharzia</td>
                                <?php
                                $numbers = range(1, 32);
                                foreach ($numbers as $num) {
                                    if (isset($Bilharzia[$num])) {
                                        echo "<td>$Bilharzia[$num]</td>";
                                    } elseif ($num == 32) {
                                        $sum = 0;
                                        foreach ($Bilharzia as $receipt) {
                                            $sum += $receipt;
                                        }
                                        echo "<td>$sum</td>";
                                    } else {
                                        echo "<td>0</td>";
                                    }
                                }
                                ?>

                                <th>16</th>
                            </tr>
                            <tr>
                                <td>17</td>
                                <td>Intestinal Worms</td>
                                <?php
                                $numbers = range(1, 32);
                                foreach ($numbers as $num) {
                                    if (isset($Intestinal[$num])) {
                                        echo "<td>$Intestinal[$num]</td>";
                                    } elseif ($num == 32) {
                                        $sum = 0;
                                        foreach ($Intestinal as $receipt) {
                                            $sum += $receipt;
                                        }
                                        echo "<td>$sum</td>";
                                    } else {
                                        echo "<td>0</td>";
                                    }
                                }
                                ?>
                                <th>17</th>
                            </tr>
                            <tr>
                                <td>18</td>
                                <td>Malnutrition</td>
                                <?php
                                $numbers = range(1, 32);
                                foreach ($numbers as $num) {
                                    if (isset($Malnutrition[$num])) {
                                        echo "<td>$Malnutrition[$num]</td>";
                                    } elseif ($num == 32) {
                                        $sum = 0;
                                        foreach ($Malnutrition as $receipt) {
                                            $sum += $receipt;
                                        }
                                        echo "<td>$sum</td>";
                                    } else {
                                        echo "<td>0</td>";
                                    }
                                }
                                ?>

                                <th>18</th>
                            </tr>
                            <tr>
                                <td>19</td>
                                <td>Anemia</td>
                                <?php
                                $numbers = range(1, 32);
                                foreach ($numbers as $num) {
                                    if (isset($Anemia[$num])) {
                                        echo "<td>$Anemia[$num]</td>";
                                    } elseif ($num == 32) {
                                        $sum = 0;
                                        foreach ($Anemia as $receipt) {
                                            $sum += $receipt;
                                        }
                                        echo "<td>$sum</td>";
                                    } else {
                                        echo "<td>0</td>";
                                    }
                                }
                                ?>

                                <th>19</th>
                            </tr>
                            <tr>
                                <td>20</td>
                                <td>Eye Infections</td>
                                <?php
                                $numbers = range(1, 32);
                                foreach ($numbers as $num) {
                                    if (isset($eye[$num])) {
                                        echo "<td>$eye[$num]</td>";
                                    } elseif ($num == 32) {
                                        $sum = 0;
                                        foreach ($eye as $receipt) {
                                            $sum += $receipt;
                                        }
                                        echo "<td>$sum</td>";
                                    } else {
                                        echo "<td>0</td>";
                                    }
                                }
                                ?>

                                <th>20</th>
                            </tr>
                            <tr>
                                <td>21</td>
                                <td>Ear Infections</td>
                                <?php
                                $numbers = range(1, 32);
                                foreach ($numbers as $num) {
                                    if (isset($ear[$num])) {
                                        echo "<td>$ear[$num]</td>";
                                    } elseif ($num == 32) {
                                        $sum = 0;
                                        foreach ($ear as $receipt) {
                                            $sum += $receipt;
                                        }
                                        echo "<td>$sum</td>";
                                    } else {
                                        echo "<td>0</td>";
                                    }
                                }
                                ?>

                                <th>21</th>
                            </tr>
                            <tr>
                                <td>22</td>
                                <td>Other Dis. of Respiratory System</td>
                                <?php
                                $numbers = range(1, 32);
                                foreach ($numbers as $num) {
                                    echo "<td>0</td>";
                                }
                                ?>

                                <th>22</th>
                            </tr>
                            <tr>
                                <td>23</td>
                                <td>Pneumonia</td>
                                <?php
                                $numbers = range(1, 32);
                                foreach ($numbers as $num) {
                                    if (isset($Pneumonia[$num])) {
                                        echo "<td>$Pneumonia[$num]</td>";
                                    } elseif ($num == 32) {
                                        $sum = 0;
                                        foreach ($Pneumonia as $receipt) {
                                            $sum += $receipt;
                                        }
                                        echo "<td>$sum</td>";
                                    } else {
                                        echo "<td>0</td>";
                                    }
                                }
                                ?>

                                <th>23</th>
                            </tr>
                            <tr>
                                <td>24</td>
                                <td>Abortion</td>
                                <?php
                                $numbers = range(1, 32);
                                foreach ($numbers as $num) {
                                    echo "<td>0</td>";
                                }
                                ?>

                                <th>24</th>
                            </tr>
                            <tr>
                                <td>25</td>
                                <td>Dis. of Puerperium & Childbirth</td>
                                <?php
                                $numbers = range(1, 32);
                                foreach ($numbers as $num) {
                                    echo "<td>0</td>";
                                }
                                ?>

                                <th>25</th>
                            </tr>
                            <tr>
                                <td>26</td>
                                <td>Hypertension</td>
                                <?php
                                $numbers = range(1, 32);
                                foreach ($numbers as $num) {
                                    if (isset($Hypertension[$num])) {
                                        echo "<td>$Hypertension[$num]</td>";
                                    } elseif ($num == 32) {
                                        $sum = 0;
                                        foreach ($Hypertension as $receipt) {
                                            $sum += $receipt;
                                        }
                                        echo "<td>$sum</td>";
                                    } else {
                                        echo "<td>0</td>";
                                    }
                                }
                                ?>

                                <th>26</th>
                            </tr>
                            <tr>
                                <td>27</td>
                                <td>Mental Disorders</td>
                                <?php
                                $numbers = range(1, 32);
                                foreach ($numbers as $num) {
                                    echo "<td>0</td>";
                                }
                                ?>

                                <th>27</th>
                            </tr>
                            <tr>
                                <td>28</td>
                                <td>Dental Disorders</td>
                                <?php
                                $numbers = range(1, 32);
                                foreach ($numbers as $num) {
                                    echo "<td>0</td>";
                                }
                                ?>

                                <th>28</th>
                            </tr>
                            <tr>
                                <td>29</td>
                                <td>Dis. of Skin (incl. wounds)</td>
                                <?php
                                $numbers = range(1, 32);
                                foreach ($numbers as $num) {
                                    echo "<td>0</td>";
                                }
                                ?>

                                <th>29</th>
                            </tr>
                            <tr>
                                <td>30</td>
                                <td>Rheumatism, Joint pains etc</td>
                                <?php
                                $numbers = range(1, 32);
                                foreach ($numbers as $num) {
                                    echo "<td>0</td>";
                                }
                                ?>

                                <th>30</th>
                            </tr>
                            <tr>
                                <td>31</td>
                                <td>Poisoning</td>
                                <?php
                                $numbers = range(1, 32);
                                foreach ($numbers as $num) {
                                    echo "<td>0</td>";
                                }
                                ?>

                                <th>31</th>
                            </tr>
                            <tr>
                                <td>32</td>
                                <td>Accidents - Fractures, Injuries, etc</td>
                                <?php
                                $numbers = range(1, 32);
                                foreach ($numbers as $num) {
                                    echo "<td>0</td>";
                                }
                                ?>

                                <th>32</th>
                            </tr>
                            <tr>
                                <td>33</td>
                                <td>Sexual Assault</td>
                                <?php
                                $numbers = range(1, 32);
                                foreach ($numbers as $num) {
                                    echo "<td>0</td>";
                                }
                                ?>

                                <th>33</th>
                            </tr>
                            <tr>
                                <td>34</td>
                                <td>Burns</td>
                                <?php
                                $numbers = range(1, 32);
                                foreach ($numbers as $num) {
                                    echo "<td>0</td>";
                                }
                                ?>

                                <th>34</th>
                            </tr>
                            <tr>
                                <td>35</td>
                                <td>Bites - Animals, Snakes</td>
                                <?php
                                $numbers = range(1, 32);
                                foreach ($numbers as $num) {
                                    echo "<td>0</td>";
                                }
                                ?>

                                <th>35</th>
                            </tr>
                            <tr>
                                <td>36</td>
                                <td>Diabetes</td>
                                <?php
                                $numbers = range(1, 32);
                                foreach ($numbers as $num) {
                                    if (isset($Diabetes[$num])) {
                                        echo "<td>$Diabetes[$num]</td>";
                                    } elseif ($num == 32) {
                                        $sum = 0;
                                        foreach ($Diabetes as $receipt) {
                                            $sum += $receipt;
                                        }
                                        echo "<td>$sum</td>";
                                    } else {
                                        echo "<td>0</td>";
                                    }
                                }
                                ?>

                                <th>36</th>
                            </tr>
                            <tr>
                                <td>37</td>
                                <td>Epilepsy</td>
                                <?php
                                $numbers = range(1, 32);
                                foreach ($numbers as $num) {
                                    if (isset($Epilepsy[$num])) {
                                        echo "<td>$Epilepsy[$num]</td>";
                                    } elseif ($num == 32) {
                                        $sum = 0;
                                        foreach ($Epilepsy as $receipt) {
                                            $sum += $receipt;
                                        }
                                        echo "<td>$sum</td>";
                                    } else {
                                        echo "<td>0</td>";
                                    }
                                }
                                ?>

                                <th>37</th>
                            </tr>
                            <tr>
                                <td>38</td>
                                <td>Dracunculiasis</td>
                                <?php
                                $numbers = range(1, 32);
                                foreach ($numbers as $num) {
                                    if (isset($Dracunculosis[$num])) {
                                        echo "<td>$Dracunculosis[$num]</td>";
                                    } elseif ($num == 32) {
                                        $sum = 0;
                                        foreach ($Dracunculosis as $receipt) {
                                            $sum += $receipt;
                                        }
                                        echo "<td>$sum</td>";
                                    } else {
                                        echo "<td>0</td>";
                                    }
                                }
                                ?>

                                <th>38</th>
                            </tr>
                            <tr>
                                <td>39</td>
                                <td>Yellow Fever</td>
                                <?php
                                $numbers = range(1, 32);
                                foreach ($numbers as $num) {
                                    if (isset($yell[$num])) {
                                        echo "<td>$yell[$num]</td>";
                                    } elseif ($num == 32) {
                                        $sum = 0;
                                        foreach ($yell as $receipt) {
                                            $sum += $receipt;
                                        }
                                        echo "<td>$sum</td>";
                                    } else {
                                        echo "<td>0</td>";
                                    }
                                }
                                ?>
                                <th>39</th>
                            </tr>
                            <tr>
                                <td>40</td>
                                <td>Viral Hemorrhagic Fever</td>

                                <?php
                                $numbers = range(1, 32);
                                foreach ($numbers as $num) {
                                    echo "<td>0</td>";
                                }
                                ?>
                                <th>40</th>
                            </tr>
                            <tr>
                                <td>41</td>
                                <td>New AIDS Cases</td>

                                <?php
                                $numbers = range(1, 32);
                                foreach ($numbers as $num) {
                                    echo "<td>0</td>";
                                }
                                ?>
                                <th>41</th>
                            </tr>
                            <tr>
                                <td>42</td>
                                <td>Plague</td>
                                <?php
                                $numbers = range(1, 32);
                                foreach ($numbers as $num) {
                                    if (isset($Plague[$num])) {
                                        echo "<td>$Plague[$num]</td>";
                                    } elseif ($num == 32) {
                                        $sum = 0;
                                        foreach ($Plague as $receipt) {
                                            $sum += $receipt;
                                        }
                                        echo "<td>$sum</td>";
                                    } else {
                                        echo "<td>0</td>";
                                    }
                                }
                                ?>
                                <th>42</th>
                            </tr>
                            <tr>
                                <td>43</td>
                                <td>Brucellosis</td>
                                <?php
                                $numbers = range(1, 32);
                                foreach ($numbers as $num) {
                                    if (isset($Brucellosis[$num])) {
                                        echo "<td>$Brucellosis[$num]</td>";
                                    } elseif ($num == 32) {
                                        $sum = 0;
                                        foreach ($Brucellosis as $receipt) {
                                            $sum += $receipt;
                                        }
                                        echo "<td>$sum</td>";
                                    } else {
                                        echo "<td>0</td>";
                                    }
                                }
                                ?>
                                <th>43</th>
                            </tr>
                            <tr>
                                <td>44</td>
                                <td></td>

                                <?php
                                $numbers = range(1, 32);
                                foreach ($numbers as $num) {
                                    echo "<td></td>";
                                }
                                ?>
                                <th>44</th>
                            </tr>
                            <tr>
                                <td>45</td>
                                <td></td>

                                <?php
                                $numbers = range(1, 32);
                                foreach ($numbers as $num) {
                                    echo "<td></td>";
                                }
                                ?>
                                <th>45</th>
                            </tr>
                            <tr>
                                <td>46</td>
                                <td>ALL OTHER DISEASES</td>

                                <?php
                                $numbers = range(1, 32);
                                foreach ($numbers as $num) {
                                    echo "<td></td>";
                                }
                                ?>
                                <th>46</th>
                            </tr>
                            <tr>
                                <td>47</td>
                                <td>TOTAL NEW CASES</td>

                                <?php
                                $numbers = range(1, 32);
                                foreach ($numbers as $num) {
                                    echo "<td>0</td>";
                                }
                                ?>
                                <th>47</th>
                            </tr>
                            <tr>
                                <td>48</td>
                                <td>NO. OF FIRST ATTENDANCES</td>

                                <?php
                                $numbers = range(1, 32);
                                foreach ($numbers as $num) {
                                    echo "<td></td>";
                                }
                                ?>
                                <th>48</th>
                            </tr>
                            <tr>
                                <td>49</td>
                                <td>RE-ATTENDANCES</td>

                                <?php
                                $numbers = range(1, 32);
                                foreach ($numbers as $num) {
                                    echo "<td></td>";
                                }
                                ?>
                                <th>49</th>
                            </tr>
                            <tr>
                                <td>50</td>
                                <td>REFERRALS IN</td>

                                <?php
                                $numbers = range(1, 32);
                                foreach ($numbers as $num) {
                                    echo "<td></td>";
                                }
                                ?>
                                <th>50</th>
                            </tr>
                            <tr>
                                <td>51</td>
                                <td>REFERRALS OUT</td>

                                <?php
                                $numbers = range(1, 32);
                                foreach ($numbers as $num) {
                                    echo "<td></td>";
                                }
                                ?>
                                <th>51</th>
                            </tr>
                            </tbody>

                        </table>
                        <br>
                        <p>&emsp;&ensp;&ensp;Completed By (Name):
                            ................................................................................ &emsp;&ensp;&ensp;
                            Designation: ........................................ &emsp;&ensp;&ensp; Sign:
                            ........................................ &emsp;&ensp;&ensp;Date: ....................</p>
                    </div>
                </div>
            </div>
        </div>

    </section>
    @endmedyqPermission

@endpush

@push("footer_scripts")

    @include('partials.global.table-scripts')
    <script type="text/javascript"
            src="{{asset('libs/bootstrap-select/dist/js/bootstrap-select.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('libs/moment/min/moment.min.js')}}"></script>
    <script type="text/javascript" src="{{asset("libs/bootstrap-daterangepicker/daterangepicker.js")}}"></script>
    <script type="text/javascript" src="{{asset('libs/chartjs/Chart.min.js')}}"></script>
    <script src="https://cdn.jsdelivr.net/gh/linways/table-to-excel@v1.0.4/dist/tableToExcel.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jspdf/1.3.5/jspdf.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/html2canvas/0.4.1/html2canvas.min.js"></script>

    {{--    @include('analytics::charts.visit-scheme-count')--}}
@endpush
