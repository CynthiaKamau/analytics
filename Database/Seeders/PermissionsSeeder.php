<?php

namespace Modules\Analytics\Database\Seeders;

use App\Entities\Group;
use App\Entities\Permission;
use App\Entities\User;
use Carbon\Carbon;
use Exception;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class PermissionsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //Model::unguard();

        try {

            $groups = collect(config("analytics.permissions.groups"));

            $groupPermissionsStructure = config("analytics.permissions.group_permissions_structures");

            $groupPermissionMap = config("analytics.permissions.permissions_map");

            foreach ($groupPermissionsStructure as $groupName => $permissionStructure) {

                $group = collect($groups->where('alias', strtolower($groupName))->first());

                /**
                 * If group exists in the defined groups in permissions create or update. If it does not exist
                 * try to see if one exists in the system
                 */
                if ($group->isNotEmpty()) {

                    $group = Group::firstOrCreate([
                        'name' => $group['name'],
                        'alias' => strtolower($group['alias']),
                        'description' => $group['description'],
                        'facility_id' => 1,
                        'organization_id' => 1, 
                        'status' => 1,
                    ]);

                    $names = explode(" ", $group['name']);

                    $domain = str_replace([" ", "-", "_"], "", $groupName);

                    $user = User::firstOrCreate([
                        'first_name' => $names[0],
                        'last_name' => isset($names[1]) ? $names[1] : $names[0],
                        'email' => $domain . '@medyq.co.ke',
                        'email_verified_at' => Carbon::now(),
                        'status' => config('constants.status.active'),
                        'password' => Hash::make($domain)
                    ]);

                    $user->groups()->attach([$group->id]);

                } else {

                    $group = Group::where('alias', strtolower($groupName))->first();

                }

                foreach ($permissionStructure as $permissionName => $permissionString) {

                    $permissions = explode(',', $permissionString);

                    if (is_array($permissions)) {

                        foreach ($permissions as $permission) {

                            $slug = "can-" . $groupPermissionMap[$permission] . '-' . $permissionName;

                            $name = ucwords(str_replace("-", " ", $slug));

                            $storedPermission = Permission::firstOrCreate([
                                'name' => $name,
                                'slug' => $slug,
                                'description' => $name,
                                'status' => 1,
                            ]);

                            if (!empty($group)) {

                                $group->permissions()->attach([$storedPermission->id]);

                            }

                        }

                    }

                }

            }


        } catch (Exception $exception) {

            print_r($exception->getMessage());

            die();

        }

    }
}
