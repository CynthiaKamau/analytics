<?php

return [
    'permissions' => [
        'groups' => [
            [
                'name' => 'Clinician',
                'alias' => 'clinician',
                'description' => 'A person having direct contact with patients rather than being involved with theoretical or laboratory services.',
                'status' => config('constants.status.active'),
            ]
        ],
        'group_permissions_structures' => [
            'super_administrator' => [
                'general-report' => 'r',
                'visit-report' => 'c,r,u,d,re,p',
                'sales-report' => 'c,r,u,d,re,p',
                'clinic-report' => 'c,r,u,d,re,p',
                'lab-report' => 'c,r,u,d,re,p',
                'invoice-list' => 'c,r,u,d,re,p',
            ],
            'administrator' => [
                'general-report' => 'r',
                'visit-report' => 'c,r,u,d,re,p',
                'sales-report' => 'c,r,u,d,re,p',
                'clinic-report' => 'c,r,u,d,re,p',
                'lab-report' => 'c,r,u,d,re,p',
                'invoice-list' => 'c,r,u,d,re,p',
            ],
            'organization_administrator' => [
                'general-report' => 'r',
                'visit-report' => 'c,r,u,d,re,p',
                'sales-report' => 'c,r,u,d,re,p',
                'clinic-report' => 'c,r,u,d,re,p',
                'lab-report' => 'c,r,u,d,re,p',
                'invoice-list' => 'c,r,u,d,re,p',
            ],
            'facility_administrator' => [
                'general-report' => 'r',
                'visit-report' => 'c,r,u,d,re,p',
                'sales-report' => 'c,r,u,d,re,p',
                'clinic-report' => 'c,r,u,d,re,p',
                'lab-report' => 'c,r,u,d,re,p',
                'invoice-list' => 'c,r,u,d,re,p',
            ],
        ],
        'permissions_map' => [
            'c' => 'create',
            'r' => 'view',
            'u' => 'update',
            'd' => 'delete',
            're' => 'restore',
            'p' => 'purge',
            'a' => 'use',
        ]
    ],
    'event_status' => [
        'active' => 1,
        'cancelled' => 0,
    ]
];
